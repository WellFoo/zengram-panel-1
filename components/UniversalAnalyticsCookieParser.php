<?php
namespace app\components;
class UniversalAnalyticsCookieParser
{
	static function parseCookie()
	{
		if (isset($_COOKIE['_ga'])) {
			list($version, $domainDepth, $cid1, $cid2) = preg_split('[\.]', $_COOKIE['_ga'], 4);
		}else{
			$version = '';
			$domainDepth ='';
			$cid1 = '';
			$cid2 = '';
		}
		return ['version' => $version, 'domainDepth' => $domainDepth, 'cid' => $cid1 . '.' . $cid2];
	}
	static function getCid()
	{
		$contents = self::parseCookie();
		return $contents['cid'];
	}
}