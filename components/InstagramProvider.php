<?php

namespace app\components;

use app\models\Account;
use BadMethodCallException;
use Core;
use core\instagram\Api;
use yii\base\Model;
use yii\web\NotFoundHttpException;

class InstagramProvider extends Model
{
	/** @var Account $_acc */
	private $_acc;

	/**
	 * @param mixed $account
	 * @throws NotFoundHttpException
	 * @throws BadMethodCallException
	 */
	public function setAccount($account)
	{
		if (is_numeric($account)) {
			/** @var Account $acc */
			$account = Account::findOne($account);
			if (is_null($account)) {
				throw new NotFoundHttpException('Account not found');
			}
		}

		if (!($account instanceof Account)) {
			throw new BadMethodCallException('$account must be instance of app\models\Account or account_id');
		}

		$this->initApi($account);
	}

	private function initApi($account)
	{
		$this->_acc = $account;

		Core::init([
			'app' => [
				'work_dir' => $this->getWorkDir(),
				'api' => [],
			],
			'env' => 'release'
		]);

		if (!Core::$app->api->loadFromFile('api.json')) {
			Core::$app->api->load([
				'proxy'  => $this->_acc->proxy->proxy,
				'ip'     => $this->_acc->ip,
				'device' => [
					'os' => [
						'api_level' => $this->_acc->device->android_api,
						'version'   => $this->_acc->device->android_version,
						'language'  => 'en_US',
					],
					'screen' => [
						'width'  => $this->_acc->device->screen_x,
						'height' => $this->_acc->device->screen_y,
						'dpi'    => $this->_acc->device->dpi,
					],
					'vendor'   => $this->_acc->device->vendor,
					'model'    => $this->_acc->device->model,
					'platform' => $this->_acc->device->platform,
					'cpu'      => $this->_acc->device->cpu,
				],
			]);
		}

		Core::$app->api->login(
			$this->_acc->login,
			UHelper::unpack($this->_acc->password)
		);
	}

	private function getWorkDir()
	{
		$path = \Yii::getAlias('@app/runtime/instagram/'.$this->_acc->login);
		if (!is_dir($path)) {
			mkdir($path, 0700, true);
		}
		return $path;
	}

	public function __destruct()
	{
		if (!is_null($this->_acc)) {
			Core::$app->api->saveToFile('api.json');
		}
	}

	public function findByLogin($login)
	{
		if (is_null($this->_acc)) {
			throw new BadMethodCallException('You must call setAccount() first');
		}

		return Core::$app->api->findByLogin($login);
	}

	public function getUserFeed($insta_id)
	{
		return Core::$app->api->getUserFeed($insta_id);
	}
}