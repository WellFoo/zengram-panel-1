<?php namespace app\components;

use yii\db\Command;

class BatchIgnoreInsertCommand extends Command
{
	public function batchInsertIgnore($table, $columns, $rows)
	{
		$object = parent::batchInsert($table, $columns, $rows);
		$sql = $object->getSql();

		//var_dump($sql); exit;
		$sql .= ' ON CONFLICT DO NOTHING;';
		//$sql = str_replace('INSERT', 'INSERT IGNORE', $sql);
		return $object->setSql($sql);
	}
}