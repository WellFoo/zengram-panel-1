<?php namespace app\components;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use Yii;

class AMQPChannelSingleton
{
	/** @var AMQPStreamConnection */
	private static $connection;

	/**
	 * @return \PhpAmqpLib\Channel\AMQPChannel
	 */
	public static function instance()
	{
		if (is_null(self::$connection)) {
			self::$connection = new AMQPStreamConnection(
				Yii::$app->params['rabbit_server']['host'],
				Yii::$app->params['rabbit_server']['port'],
				Yii::$app->params['rabbit_server']['username'],
				Yii::$app->params['rabbit_server']['password']
			);
		}
		return self::$connection->channel();
	}
}