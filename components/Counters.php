<?php

namespace app\components;

use app\models\Metrika;
use app\models\Invoice;
use UnitedPrototype\GoogleAnalytics\Event;
use UnitedPrototype\GoogleAnalytics\Page;
use UnitedPrototype\GoogleAnalytics\Session;
use UnitedPrototype\GoogleAnalytics\Tracker;
use UnitedPrototype\GoogleAnalytics\Visitor;
use yii\helpers\Console;
use yii\helpers\Html;
use Yii;
use yii\web\JsExpression;
use yii\web\Cookie;

class Counters
{
	public static $userAgent = 'THE ICONIC GA Measurement Protocol PHP Client (https://github.com/theiconic/php-ga-measurement-protocol)';

	public static function render()
	{
		$result = Html::beginTag('script', ['type' => 'text/javascript']);
		// Подставляем id-шник пользователя в ga
		if (isset(Yii::$app->user->id)) {
			$user_id = Yii::$app->user->id;
			$userIdGa = $user_id;

			$gaUsersParmams = '';

			// Передача пользовательского показателя - количество аккаунтов
			$gaUsersParmams .= "ga('set', 'metric1'," . Yii::$app->user->identity->accountsCount . ");\r\n";

			// Передача пользовательского показателя - количество дней после регистрации
			$timestamp = strtotime(Yii::$app->user->identity->regdate);

			// Передача пользовательского параметра - дней после регистрации
			if ($timestamp > 0) {
				$daysAfterRegistration = round((time() - $timestamp) / 24 / 60 / 60) + 1;
				$gaUsersParmams .= "ga('set', 'metric2', $daysAfterRegistration);\r\n";
			}

			// Передача пользовательского параметра - пол
			if (Yii::$app->user->identity->gender != 'Не определен') {
				$gaUsersParmams .= "ga('set', 'dimension1', '" . Yii::$app->user->identity->gender . "');\r\n";
			}

			// Передача пользовательского параметра - лицо
			if (Yii::$app->user->identity->person != 'Не определен') {
				$gaUsersParmams .= "ga('set', 'dimension2', '" . Yii::$app->user->identity->person . "');\r\n";
			}

			$customDimension = "ga('set', 'userId', $userIdGa);\r\n";
			$customDimension .= $gaUsersParmams;

			Yii::$app->params['counters']['script'][0] = str_replace('//{CustomDimension}', $customDimension, Yii::$app->params['counters']['script'][0]);

		}
		foreach (Yii::$app->params['counters']['script'] as $script) {
			$result .= $script . PHP_EOL;
		}
		$result .= Html::endTag('script');

		$result .= Html::beginTag('noscript');
		foreach (Yii::$app->params['counters']['noscript'] as $noscript) {
			$result .= $noscript . PHP_EOL;
		}
		$result .= Html::endTag('script');

		return $result;
	}

	public static function renderAction($action)
	{
		return new JsExpression(
			'try {' . PHP_EOL .
			implode(PHP_EOL, Yii::$app->params['counters']['js_actions'][$action]) . PHP_EOL .
			implode(PHP_EOL, Yii::$app->params['counters']['js_actions']['summary']) . PHP_EOL .
			'} catch (e) { console.log(e); }'
		);
	}

	public static function trigger($action, $refferer = false)
	{
		try {
			$counter = new Metrika(Yii::$app->params['counters']['yandex']['id']);
			$counter->hit(Yii::$app->params['counters']['php_actions'][$action], null, $refferer ? $refferer : '/');
			$counter->reachGoal(Yii::$app->params['counters']['yandex']['summary']);

			$tracker = new Tracker(Yii::$app->params['counters']['google']['id'], 'auto');
			$page = new Page(Yii::$app->params['counters']['php_actions'][$action]);
			$session = new Session();
			$user = new Visitor();
			$tracker->trackPageview($page, $session, $user);
			$evt = new Event(
				Yii::$app->params['counters']['google']['summary'][0],
				Yii::$app->params['counters']['google']['summary'][1]
			);
			$tracker->trackEvent($evt, $session, $user);
		} catch (\Exception $e) {
			@Console::stdout('Error trigger counter: ' . $e->getMessage());
			@Yii::info('Error trigger counter: ' . $e->getMessage(), 'Trigger');
		}
	}

	public static function triggerMeasurement($action, $user_id)
	{
		/** @var \baibaratsky\yii\google\analytics\MeasurementProtocol $ga */
		$ga = \Yii::$app->ga;
		$request = $ga->request();
		$request->setUserId($user_id);
		$request->setClientId('');

		$request->setEventCategory('действие')
			->setEventAction($action)
			->sendEvent();

	}

	public static function gaTransaction($user_id, $transaction_id, $price, $days, $method = 'none', $ip = null, $ga_json)
	{

		if ($ga_json != '') {
			$gaData = json_decode($ga_json, true);
		}
		$user_agent = isset($gaData['user_agent']) ? $gaData['user_agent'] : '';
		$client_id = isset($gaData['client_id']) ? $gaData['client_id'] : '';
		$user_lang = isset($gaData['user_lang']) ? $gaData['user_lang'] : '';

		// Пишем информацию о переменных в логи
		$file = fopen('transaction_log.txt', 'a');
		$log = date("Y-m-d H:i:s") .
			";$method;$user_id;$transaction_id;$price;$days;$ip;$client_id;$user_agent;$user_lang\r\n";
		fwrite($file, $log);
		fclose($file);

		// Передача информации о транзакции в GA

		/** @var \baibaratsky\yii\google\analytics\MeasurementProtocol $ga */
		$ga = \Yii::$app->ga;
		$request = $ga->request();


		$request->setUserId($user_id);
		if (!is_null($ip)) {
			$request->setIpOverride($ip);
		}
		if ($user_agent != '') {
			self::$userAgent = $user_agent;
		}

		if ($user_lang != '') {
			$request->setUserLanguage($user_lang);
		}

		$request->setClientId($client_id);


		$request->setTransactionId($transaction_id)
			->setAffiliation('zengram.ru')
			->setRevenue($price);

		$productData = [
			'name' => 'Количество дней: ' . $days,
			'price' => $price,
			'quantity' => 1,
			'sku' => $days . '-' . $price
		];
		$request->addProduct($productData);
		$request->setProductActionToPurchase();

		$request->setEventCategory('действие')
			->setEventAction('оплачено')
			->sendEvent();
	}

	public static function gaFirstSource()
	{
		$FirstUtm['source'] = Yii::$app->request->get('utm_source', null);
		$FirstUtm['medium'] = Yii::$app->request->get('utm_medium', null);
		$FirstUtm['campaign'] = Yii::$app->request->get('utm_campaign', null);

		if (is_null($FirstUtm['source'])) {
			if (Yii::$app->request->getReferrer() == '') {
				$FirstUtm['source'] = 'direct';
			} else {
				$url = parse_url(Yii::$app->request->getReferrer());
				$FirstUtm['source'] = $url['host'];
			}
		}

		// Сохраняем данные о первоисточнике в куки

		// В контроллере ставим куки и записываем базу статиситку заходов по utm-меткам
		if (!isset(Yii::$app->request->cookies['utm_source'])) {
			Yii::$app->response->cookies->add(new Cookie([
				'name' => 'utm_source',
				'value' => $FirstUtm['source']
			]));
		}
		if (!isset(Yii::$app->request->cookies['utm_medium'])) {
			Yii::$app->response->cookies->add(new Cookie([
				'name' => 'utm_medium',
				'value' => $FirstUtm['medium']
			]));
		}
		if (!isset(Yii::$app->request->cookies['utm_campaign'])) {
			Yii::$app->response->cookies->add(new Cookie([
				'name' => 'utm_campaign',
				'value' => $FirstUtm['campaign']
			]));
		}
	}

	public static function gaRegistration()
	{
		// Получаем GA client_id из кук
		$client_id = UniversalAnalyticsCookieParser::getCid();
		// Отправляем данные об исходном канале в GA
		/** @var \baibaratsky\yii\google\analytics\MeasurementProtocol $ga */
		$ga = \Yii::$app->ga;
		$request = $ga->request();
		$request->setUserId(Yii::$app->user->id);
		$request->setClientId($client_id);
		$request->setUserLanguage(mb_strtolower(mb_substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5)));
		self::$userAgent = $_SERVER['HTTP_USER_AGENT'];

		if (isset(Yii::$app->request->cookies['utm_source'])) {
			$request->setCustomDimension(Yii::$app->request->cookies->getValue('utm_source'), 3);
		}
		if (isset(Yii::$app->request->cookies['utm_medium'])) {
			$request->setCustomDimension(Yii::$app->request->cookies->getValue('utm_medium'), 4);
		}
		if (isset(Yii::$app->request->cookies['utm_campaign'])) {
			$request->setCustomDimension(Yii::$app->request->cookies->getValue('utm_campaign'), 5);
		}

		$request->setEventCategory('действие')
			->setEventAction('регистрация')
			->sendEvent();
	}

	public static function roiStatsSendEvent($event = null, $visit_id = null)
	{
		if (LANGUAGE != 'ru') {
			return;
		}
		$roiProjectId = Yii::$app->params['counters']['roi_stats']['project_id'];
		$eventUrl = 'https://cloud.roistat.com/api/site/1.0/' . $roiProjectId . '/event/register';


		$roistatCookie = null;
		if (isset($_COOKIE['roistat_visit'])){
			$roistatCookie = $_COOKIE['roistat_visit'];

		}
		if (!is_null($visit_id)){
			$roistatCookie = $visit_id;
		}

		if (!is_null($event)) {
			$data['event'] = $event;
			$data['visit'] = $roistatCookie;
			$curl = clone Yii::$app->curl;
			$curl->options['timeout'] = 600;
			$result = $curl->get($eventUrl, $data);
			Yii::info($result);
		}
	}


	public static function roiStatsRegister(){
		if (LANGUAGE != 'ru') {
			return;
		}

		// Созданием фейковый платёж для создания карточки клиента
		$fakeInvoice = new Invoice();
		$fakeInvoice->load(
			[
				'user_id'     => Yii::$app->user->id,
				'price_id'    => 8,
				'sum'         => 0,
				'agent'         => 'none',
				'ga_data' => ''
			], '');
		$fakeInvoice->save();
		$fakeTransactionId = $fakeInvoice->id;
		$fakeInvoice->delete();

		$roistatCookie = self::roiCookieVisit();

		self::roiClient();

		self::roiAddFakeOrder($roistatCookie, $fakeTransactionId);
		// Отправляем событие в Roistats
		self::roiStatsSendEvent('register');

		// Пишем информацию о переменных в логи
		$file = fopen('roi_log.txt', 'w');
		$log = date("Y-m-d H:i:s") . ';'
			. Yii::$app->user->id .';'
			. Yii::$app->user->identity->mail .';'
			. $roistatCookie . ';'
			. 'register'
			. "\r\n";
		fwrite($file, $log);
		fclose($file);
	}

	public static function roiAddFakeOrder($roistatCookie, $fakeTransactionId){
		$data = [
			0 => [
				'id' => $fakeTransactionId,
				'name' => 'Тестовый период',
				'date_create' => time(),
				'status' => 1,
				'roistat' => $roistatCookie,
				'client_id' => Yii::$app->user->id
			]
		];


		$json = json_encode($data);
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://cloud.roistat.com/api/v1/project/add-orders?key=da4e292ccd624c3cc25cd5d1a36682aa&project=25038",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $json,
		));
		curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);

		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);

		Yii::info('Добавили фейковую транзакции ' . $json);
		if ($err) {
			Yii::error('cURL Error #:' . $err);
		} else {
			Yii::info('Добавили фейковую транзакции ' . $response);
		}

	}



	public static function roiInvoice($invoice = [], $visit_id = null){
		if (LANGUAGE != 'ru') {
			return;
		}

		if (empty($invoice)){
			return;
		}

		$roistatCookie = null;
		if (isset($_COOKIE['roistat_visit'])){
			$roistatCookie = $_COOKIE['roistat_visit'];
		}
		if (!is_null($visit_id)){
			$roistatCookie = $visit_id;
		}


		$data = [
			0 => [
				'id' => $invoice['id'],
				'name' => $invoice['name'],
				'date_create' => time(),
				'status' => $invoice['status'],
				'roistat' => $roistatCookie,
				'price' => $invoice['price'],
				'cost' => $invoice['price'],
				'client_id' => $invoice['client_id']
			]
		];


		$json = json_encode($data);
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://cloud.roistat.com/api/v1/project/add-orders?key=da4e292ccd624c3cc25cd5d1a36682aa&project=25038",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $json,
		));
		curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);

		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);

		Yii::info('Создали платеж ' . $json);
		if ($err) {
			Yii::error('cURL Error #:' . $err);
		} else {
			Yii::info('Платеж успешно создан ' . $response);
		}
		//if($data[0]['status']) {
		//	Counters::roiStatsSendEvent('success_payment');
		//}else{
		//	Counters::roiStatsSendEvent('create_invoice');
		//}
	}
	public static function roiClientOld()
	{
		// Добавляем клиента
		if (LANGUAGE != 'ru') {
			return;
		}
		/*
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://cloud.roistat.com/api/v1/project/visits/1001?key=da4e292ccd624c3cc25cd5d1a36682aa&project=25038",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_POSTFIELDS => "{}",
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
		*/

//		$roiProjectId = Yii::$app->params['counters']['roi_stats']['project_id'];
//		$apiKey = Yii::$app->params['counters']['roi_stats']['api_key'];
//		$eventUrl = 'https://cloud.roistat.com/api/v1/project/clients/import?'.
//			http_build_query(['project' => $roiProjectId, 'key' => $apiKey]);
//
//
//		$data = [
//			0 => [
//				'id' => 2414,
//				'name' => 'Сергей',
//				'phone' => '+79001122333',
//				'email'  => 'sergeivl39@gmail.com',
//			]
//		];
//		echo $eventUrl;
//		$curl = clone Yii::$app->curl;
//		$curl->options['timeout'] = 600;
//		$result = $curl->post($eventUrl, $data);
//		print_r($result);

		$data = [
			0 => [
				'id' => 2414,
				'name' => 'Сергей',
				'phone' => '+79001122333',
				'email' => 'sergeivl39@gmail.com',

			],
			1 => [
				'id' => 2415,
				'name' => 'Тест',
				'phone' => '+79001122334',
				'email' => 'sergeivl391@gmail.com',
			]
		];
		$json = json_encode($data);
		echo $json ;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://cloud.roistat.com/api/v1/project/clients/import?key=da4e292ccd624c3cc25cd5d1a36682aa&project=25038",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $json,
			//CURLOPT_POSTFIELDS => $data,
		));
		curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
		//curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

		$response = curl_exec($curl);



		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
			Yii::error("cURL Error #:" . $err);
		} else {
			Yii::info($response);
		}

	}
	public static function roiSetStatuses(){
		if (LANGUAGE != 'ru') {
			return;
		}
		$roiProjectId = Yii::$app->params['counters']['roi_stats']['project_id'];
		$key = Yii::$app->params['counters']['roi_stats']['api_key'];
		$data = [
			0 => [
				'id' => 1,
				'name' => 'Cчёт cформирован',
				'status' => 'progress',
			],
			1 => [
				'id' => 2,
				'name' => 'Счёт оплачен',
				'status' => 'paid',
			],
			2 => [
				'id' => 3,
				'name' => 'Тестовый период',
				'status' => 'progress',
			]
		];
		$json = json_encode($data);
		// echo $json;
		//die();
		$curl = curl_init();
		curl_setopt_array($curl, [
			CURLOPT_URL => 'https://cloud.roistat.com/api/v1/project/set-statuses?project="' . $roiProjectId .'&key='. $key,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $json,
		]);
		curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);

		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
			Yii::error("cURL Error #:" . $err);
		} else {
			Yii::info($response);
		}
	}

	public static function roiAddOrder($roistatCookie){
		if (LANGUAGE != 'ru') {
			return;
		}
		$data = [
			0 => [
				'id' => 1,
				'name' => 'Тестовый период',
				'date_create' => 1475155759,
				'status' => 1,
				'roistat' => self::roiCookieVisit(),
				'price' => ''
			]
		];
		$json = json_encode($data);

		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://cloud.roistat.com/api/v1/project/add-orders?key=da4e292ccd624c3cc25cd5d1a36682aa&project=25038",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $json,
		));
		curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);

		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
			Yii::info('cURL Error #:' . $err);
		} else {
			Yii::error('Добавили фейковую транзакции ' . $response);
		}


	}

	public static function roiClient(){
		if (LANGUAGE != 'ru') {
			return;
		}
		// Извлекаем имя из мыла
		$mailParts = explode('@', Yii::$app->user->identity->mail);
		$data = [
			0 => [
				'id' => Yii::$app->user->id,
				'name' => $mailParts[0],
				'phone' => null,
				'email' => Yii::$app->user->identity->mail,
			],
		];
		$json = json_encode($data);
		//echo $json ;
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://cloud.roistat.com/api/v1/project/clients/import?key=da4e292ccd624c3cc25cd5d1a36682aa&project=25038",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $json,
		));
		curl_setopt($curl, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);

		$response = curl_exec($curl);

		$err = curl_error($curl);
		curl_close($curl);

		if ($err) {
			Yii::error('cURL Error #:' . $err);
		} else {
			Yii::info('Добавили клиента' . $response);
		}
	}
	public static function roiCookieVisit(){
		if (isset($_COOKIE['roistat_visit'])){
			$roistatCookie = $_COOKIE['roistat_visit'];

		} else {
			$roistatCookie = null;
		}
		return $roistatCookie;
	}

}