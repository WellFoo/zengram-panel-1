<?php
namespace app\components;

use yii\image\drivers\Kohana_Image_Imagick;

Class YiiImagick extends Kohana_Image_Imagick {

	public function __construct($file)
	{
		parent::__construct($file);
	}

	public function placeRandomWatermark(){
		$draw  = new \ImagickDraw();
		$draw->setFillColor(new \ImagickPixel(sprintf('rgba(%d, %d, %d, %s)', mt_rand(0,255), mt_rand(0,255), mt_rand(0,255), mt_rand(1, 15)/100)));
		$draw->point(mt_rand(1, $this->im->getImageWidth()), mt_rand(1, $this->im->getImageHeight()));
		$this->im->drawImage($draw);
	}

	public function render($type = null, $quality = 100)
	{
		// Get the image format and type
		list($format, $type) = $this->_get_imagetype($type);

		// Set the output image type
		$this->im->setFormat($format);

		// Set the output quality
		$this->im->setImageCompressionQuality($quality);

		// Reset the image type and mime type
		$this->type = $type;
		$this->mime = image_type_to_mime_type($type);

		$this->im->stripImage();
		return (string) $this->im;
	}

	public function getImageWidth(){
		return $this->im->getImageWidth();
	}

	public function getImageHeight(){
		return $this->im->getImageHeight();

	}
}