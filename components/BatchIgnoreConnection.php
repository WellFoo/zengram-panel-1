<?php

namespace app\components;
use yii\db\Connection;
use yii\db\Exception;

class BatchIgnoreConnection extends Connection
{
	public $local_dsn;
	/**
	 * @param null|string $sql
	 * @param array $params
	 * @return BatchIgnoreConnection $this
	 */
	public function createCommand($sql = null, $params = [])
	{
		$command = new BatchIgnoreInsertCommand([
			'db' => $this,
			'sql' => $sql,
		]);

		return $command->bindValues($params);
	}

	public function open()
	{
		if ($this->pdo !== null) {
			return;
		}
		try {
			parent::open();
			define('NO_DB_CONNECTION', false);
		} catch (Exception $e) {
			define('NO_DB_CONNECTION', true);
			$this->dsn = $this->local_dsn;
			parent::open();
		}
	}
}