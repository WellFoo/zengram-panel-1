<?php namespace app\components;

/**
 * Created by BetsuNo
 */
class StringCallback
{
	private $callback;

	/**
	 * @param $callable
	 */
	public function __construct($callable)
	{
		$this->callback = $callable;
	}

	/**
	 * The __toString method allows a class to decide how it will react when it is converted to a string.
	 *
	 * @return string
	 * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.tostring
	 */
	function __toString()
	{
		if (is_callable($this->callback)) {
			return call_user_func($this->callback);
		}
		return '';
	}

}