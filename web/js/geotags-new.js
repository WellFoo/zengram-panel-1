'use strict';

function geotagsManger($modal, placeItem)
{
	var map, cluster, circleEdit;

	var circle = new google.maps.Circle({
		editable:     false,
		draggable:    true,
		cursor:       'move',
		fillColor:    '#5F84D0',
		fillOpacity:  .5,
		strokeColor:  '#DB1D1D',
		strokeOpacity: .7,
		strokeWeight:  2
	});
	circle.addListener('dragstart', circleDragStart);
	circle.addListener('drag', circleDrag);
	circle.addListener('dragend', circleDragEnd);

	var markerActiveIcon = $.extend(google.maps.Icon, {
		url:    '/img/map_marker_icon.png',
		size:   new google.maps.Size(24, 32),
		origin: new google.maps.Point(24, 0),
		anchor: new google.maps.Point(12, 32)
	});
	var markerDisabledIcon = $.extend(google.maps.Icon, {
		url:    '/img/map_marker_icon.png',
		size:   new google.maps.Size(24, 32),
		origin: new google.maps.Point(0, 0),
		anchor: new google.maps.Point(12, 32)
	});
	var markerShape = $.extend(google.maps.MarkerShape, {
		type: 'poly',
		coords: [11, 32, 2, 17, 0, 10, 2, 4, 5, 2, 8, 0, 16, 0, 19, 2, 22, 4, 24, 19, 22, 17, 12, 32]
	});

	var GeotagItem = can.Map.extend({
		id:         undefined,
		coords:     can.compute(giCoordsCompute),
		title:      undefined,
		address:    undefined,
		additional: undefined,
		marker:     undefined,
		enabled:    can.compute(giEnabledCompute),
		visible:    undefined
	});

	function giCoordsCompute(value)
	{
		if (!arguments.length) {
			return this.coords;
		}
		if (value instanceof google.maps.LatLng) {
			this.coords = value;
		}
		this.coords = new google.maps.LatLng(value.lat, value.lng);
	}

	function giEnabledCompute(value)
	{
		if (!arguments.length) {
			return this.enabled;
		}
		value = Boolean(value);
		if (value) {
			this.marker.setOptions({opacity: 1});
			this.marker.setIcon(markerActiveIcon);
		} else {
			this.marker.setOptions({opacity: .5});
			this.marker.setIcon(markerDisabledIcon);
		}
		this.enabled = value;
	}

	var GeotagsControl = can.Control.extend({
		init: initView,
		'.balloon click': balloonClick,
		'.geotags-circle-filter click': circleFilterSwitch,
		'.save-geotags click': saveTags,
		'.geotags-additional-search submit': additionalSearch,
		'.geotags-additional-search input keyup': additionalSearchKeyUp,
		'.geotags-additional-search input focus': additionalSearchFocus,
		'.geotags-additional-search input blur': additionalSearchBlur,
		'.geotags-additional-tags input change': useAdditionalChanged,
		'.locate click': locateClick
	});

	function initView ($el, options)
	{
		$el.html(can.view('geotags-list-tpl', options));
		$el.find('.geotag-search input').val(placeItem.name);

		map = new google.maps.Map($el.find('.geotags-map')[0], {
			center: new google.maps.LatLng(placeItem.center.lat, placeItem.center.lng),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			streetViewControl: false,
			zoom: 8
		});

		$modal.off('shown.bs.modal');
		$modal.on('shown.bs.modal', function () {
			google.maps.event.trigger(map, 'resize');
			map.fitBounds(new google.maps.LatLngBounds(
				new google.maps.LatLng(placeItem.sw.lat, placeItem.sw.lng),
				new google.maps.LatLng(placeItem.ne.lat, placeItem.ne.lng)
			));
		});
		$modal.modal();

		cluster = new MarkerClusterer(map, [], {
			imagePath: '/img/markerclusterer/m',
			maxZoom: 15,
			minimumClusterSize: 5
		});

		circle.setCenter(new google.maps.LatLng(placeItem.circle.center.lat, placeItem.circle.center.lng));
		circle.setRadius(placeItem.circle.radius);
		circle.setMap(map);
		circle.setVisible(placeItem.circle.enabled);
		drawCircleEdit(map, new google.maps.LatLng(
			placeItem.circle.center.lat,
			circle.getBounds().getSouthWest().lng()
		), placeItem.circle.enabled);
		options.circleFilter(placeItem.circle.enabled);

		options.pending(true);

		// ������� ����������
		var gmap = getGmap();

		$.getJSON($('#geotags-list-tpl').data('url'), {
			place_id: placeItem.id,
			// ����������
			gmap: JSON.stringify(gmap)
			//gmap_lat,
			//gmap_lng
		}, function (data) {
			drawMarkers(data);
			options.useAdditional(placeItem.use_additional_tags);
		}).always(function () {
			options.pending(false);
		});
	}

	function balloonClick($el)
	{
		var item = geotagsData.attr($el.data('idx'));
		item.attr('enabled', !item.attr('enabled'));
	}

	function circleFilterSwitch()
	{
		geotagsControl.options.circleFilter(!geotagsControl.options.circleFilter());
		circle.setVisible(geotagsControl.options.circleFilter());
		circleEdit.setVisible(geotagsControl.options.circleFilter());
		circleDragEnd();
	}

	var saving = false;
	function saveTags()
	{
		if (saving) {
			return;
		}
		saving = true;
		var data = {
			use_additional_tags: geotagsControl.options.useAdditional() ? 1 : 0,
			circle: {
				enabled: geotagsControl.options.circleFilter() ? 1 : 0,
				center:  {
					lat: circle.getCenter().lat(),
					lng: circle.getCenter().lng()
				},
				radius:  circle.getRadius()
			},
			tags: geotagsDataToObject()
		};
		$.post($('#geotags-list-tpl').data('url') + '?place_id=' + placeItem.id, {
				action: 'save',
				data:   data
			}, function () {
				placeItem.circle.enabled = geotagsControl.options.circleFilter();
				placeItem.circle.radius  = circle.getRadius();
				placeItem.circle.center.lat = circle.getCenter().lat();
				placeItem.circle.center.lng = circle.getCenter().lng();
				placeItem.use_additional_tags = geotagsControl.options.useAdditional();
				$modal.modal('hide');
			}
		).always(function() {
				saving = false;
			});
	}

	function additionalSearch($el)
	{
		var $input = $el.find('input');
		if (!$input.val().length) {
			return;
		}
		geotagsControl.options.pending(true);
		$.post($('#geotags-list-tpl').data('url') + '?place_id=' + placeItem.id, {
			action: 'search',
			query:  $input.val()
		}, function (data) {
			addMarkers(data);
			$input.val('');
		}).always(function () {
			geotagsControl.options.pending(false);
		});
	}

	var searchAutocompleteTimeout;
	function additionalSearchKeyUp($el)
	{
		if ($el.val().length < 3) {
			return;
		}
		clearTimeout(searchAutocompleteTimeout);
		searchAutocompleteTimeout = setTimeout(searchAutocomplete.bind($el), 200);
	}

	function additionalSearchFocus()
	{
		geotagsControl.options.autocompleteShow(autocompleteData.length > 0);
	}

	function additionalSearchBlur()
	{
		geotagsControl.options.autocompleteShow(false);
		if (!$el.val().length) {
			autocompleteData.splice(0);
		}
	}

	function searchAutocomplete()
	{
		$.post($('#geotags-list-tpl').data('url') + '?place_id=' + placeItem.id, {
			action: 'autocomplete',
			query:  this.val()
		}, function (data) {
			autocompleteData.splice(0);
			if (!data.status) {
				for (var key in data) {
					if (!data.hasOwnProperty(key)) {
						continue;
					}
					autocompleteData.push(data[key]);
				}
			}
			geotagsControl.options.autocompleteShow(autocompleteData.length > 0);
		}).always(function () {
			geotagsControl.options.pending(false);
		});
	}

	function useAdditionalChanged($el)
	{
		geotagsControl.options.useAdditional($el.is(':checked'));
	}

	function locateClick($el, evt)
	{
		evt.stopPropagation();
		var marker = geotagsData.attr($el.parents('.balloon').data('idx')).attr('marker');
		map.setCenter(marker.position);
		map.setZoom(17);
		marker.setAnimation(google.maps.Animation.BOUNCE);
		setTimeout(function () {
			marker.setAnimation(null);
		}, 2000);
	}

	function geotagsDataToObject()
	{
		var result = [];
		geotagsData.each(function (item) {
			result.push({
				id:      item.attr('id'),
				enabled: item.attr('enabled') ? 1 : 0
			});
		});
		return result;
	}

	var geotagsData = new can.List([]);
	var autocompleteData = new can.List([]);

	var geotagsControl = new GeotagsControl('#geotag-manager', {
		placeName:    placeItem.name,
		items:        geotagsData,
		pending:      can.compute(false),
		circleFilter: can.compute(true),
		useAdditional: can.compute(gcUseAdditionalCompute),
		autocompleteShow: can.compute(false),
		autocompleteItems: autocompleteData
	});
	$modal.on('hidden.bs.modal', function () {
		geotagsControl.destroy();
	});

	function gcUseAdditionalCompute(value)
	{
		if (!arguments.length) {
			return this.useAdditional;
		}
		value = Boolean(value);
		this.useAdditional = value;
		geotagsControl.options.items.each(function (item) {
			var visibility = !item.attr('additional') || (item.attr('additional') && value);
			item.attr('visible', visibility);
			item.attr('marker').setVisible(visibility);
		});
	}

	function getGeotagItem(data)
	{
		var marker = drawMarker(data);
		var item = new GeotagItem({
			id:         data.id,
			coords:     data.coords,
			title:      data.title,
			address:    data.address,
			marker:     marker,
			enabled:    data.enabled,
			additional: data.additional,
			visible:    !data.additional || (data.additional && geotagsControl.options.useAdditional())
		});
		marker.geotag = item;
		cluster.addMarker(marker);
		return item;
	}

	function drawMarkers(data)
	{
		for (var key in data) {
			if (!data.hasOwnProperty(key)) {
				continue;
			}
			var item = getGeotagItem(data[key]);
			if (item.attr('additional')) {
				geotagsData.unshift(item);
			} else {
				geotagsData.push(item);
			}
		}
	}

	function addMarkers(data)
	{
		for (var key in data) {
			if (!data.hasOwnProperty(key)) {
				continue;
			}
			if (tagExists(data[key].id)) {
				continue;
			}
			geotagsData.unshift(getGeotagItem(data[key]));
		}
	}

	function tagExists(id)
	{
		var result = false;
		geotagsData.each(function (item) {
			if (item.attr('id') === id) {
				result = true;
			}
		});
		return result;
	}

	function drawMarker(data)
	{
		var coords = data.coords;
		coords.lat += Math.random() * 0.0001 - 0.0001;
		coords.lng += Math.random() * 0.0001 - 0.0001;
		var marker = new google.maps.Marker({
			position: coords,
			title:    data.title + (data.address ? "\n" + data.address : ''),
			shape:    markerShape,
			visible:  !data.additional || (data.additional && geotagsControl.options.useAdditional())
		});
		marker.addListener('click', markerClick);
		return marker;
	}

	function markerClick()
	{
		this.geotag.attr('enabled', !this.geotag.attr('enabled'));
	}

	var circleDragLatLng;
	var circleEditDragLatLng;
	function circleDragStart(evt)
	{
		circleDragLatLng = evt.latLng;
		circleEditDragLatLng = circleEdit.getPosition();
	}

	function circleDrag(evt)
	{
		window.requestAnimationFrame(function () {
			circleEdit.setPosition(new google.maps.LatLng(
				circleEditDragLatLng.lat() + evt.latLng.lat() - circleDragLatLng.lat(),
				circleEditDragLatLng.lng() + evt.latLng.lng() - circleDragLatLng.lng()
			));
		});
	}

	function circleDragEnd()
	{
		geotagsData.each(function (item) {
			var enabled;
			if (geotagsControl.options.circleFilter()) {
				enabled = circle.contains(item.attr('coords'));
			} else {
				enabled = true;
			}
			item.attr('enabled', enabled);
		});
	}

	function drawCircleEdit(mapItem, coords, visible)
	{
		circleEdit = new google.maps.Marker({
			map:       mapItem,
			position:  coords,
			title:     'drag to resize circle',
			draggable: true,
			visible:   visible,
			zIndex:    10000,
			icon: {
				url: '/img/map_circle_editor.png',
				size: new google.maps.Size(20, 20),
				anchor: new google.maps.Point(10, 10)
			},
			shape: {
				coords: [0, 0, 20],
				type: 'circle'
			}
		});
		circleEdit.addListener('drag', circleEditChanged);
		circleEdit.addListener('dragend', circleDragEnd);
	}

	function circleEditChanged()
	{
		window.requestAnimationFrame(function () {
			circle.setRadius(google.maps.geometry.spherical.computeDistanceBetween(
				circle.getCenter(),
				circleEdit.getPosition()
			));
		})
	}
}
function locateClient(callback, apikey)
{
	$.ajax({
		url: 'https://www.googleapis.com/geolocation/v1/geolocate?key=' + apikey,
		type: 'post',
		async: false,
		contentType : 'application/json',
		data: JSON.stringify({
			'macAddress':         '00:00:00:00:00:00',
			'signalToNoiseRatio': 0,
			'signalStrength':     0,
			'channel':            0,
			'age':                0
		}),
		dataType: 'json',
		success: function (data) {
			callback(true, data);
		},
		error: function () {
			callback(false, null);
		}
	});
}

function getGmapData(){
	return locateClient(function(success, data){
		if (success){
			console.log('��������� ����������');
			//console.log(data);
			console.log('������ ' +  data.location.lat);
			console.log('������� ' +  data.location.lng);
			//Math.floor(Date.now() / 1000);
			var gmap = {
				lat: data.location.lat,
				lng: data.location.lng,
				timestamp: Math.round(Date.now()/1000)
			};

			localStorage.setItem('gmap', JSON.stringify(gmap));

			//localStorage.setItem('gmap_lng', data.location.lng)
		}else{
			console.log('������ ��������� ���������');
		}
	}, apikey);
}

function getGmap() {
	var gmap = {};
	if (localStorage.getItem('gmap') === null) {
		getGmapData();
		gmap = JSON.parse(localStorage.getItem('gmap'));

	} else {
		gmap = JSON.parse(localStorage.getItem('gmap'));
		if (Math.round(Date.now() / 1000) - gmap.timestamp > SECOND_PER_DAY) {
			getGmapData();
		}
	}
	return gmap;
}

google.maps.Circle.prototype.contains = function(latLng) {
	if (!this.getBounds().contains(latLng)) {
		return false;
	}
	var distance = google.maps.geometry.spherical.computeDistanceBetween(
		this.getCenter(),
		latLng
	);
	return this.getRadius() >= distance;
};



google.maps.Marker.prototype.geotag = undefined;