<?php

namespace app\commands;

use app\components\TaskController;
use app\models\SnifferMonitoringParameters;
use app\models\SnifferServers;
use app\models\SnifferUsers;
use Yii;
use yii\base\ErrorException;

class SnifferController extends TaskController
{

	const SCAN_TRY_COUNT = 3;
	const SCAN_TRY_PAUSE = 20;
	const NOTIFY_USERS = true;

	public function actionScan($scan = true)
	{
		self::log('Запуск сканирования состояния серверов');

		if ($scan) {
			self::log('Сканирование с фиксированием состояния');
		}

		$servers = SnifferServers::find();

		if (!empty($this->argv[3])) {
			$servers->andWhere(['id' => $this->argv[3]]);
		}

		/** @var SnifferServers $server */
		foreach ($servers->each() as $server) {

			self::log('Обработка сервера ' . $server->id . ' - ' .
				long2ip($server->ip) . ' - ' .
				$server->name . ' (' .
				$server->group->name . ')');

			if ($scan) {
				$server->log();
			}

			$try_count = self::SCAN_TRY_COUNT;

			do {
				$ping = $this->pingDomain(long2ip($server->ip));

				if ($ping === -1 && $scan) {
					$server->status(SnifferServers::STATUS_OFF);
					$server->ping = 0;
					$server->message = 'Сервер не отвечает';
					$server->save();

					self::log('Ошибка обновления данных');
					continue 2;
				}

				$server->ping = $ping;

				$data = $server->sniff();

				if ($data === false) {
					self::log('Ошибка обновления данных');
					continue 2;
				}

				try {
					$server->storeData($data);
					$server->status(SnifferServers::STATUS_ON);
					$server->message = '';
					self::log('Данные обновлены');

				} catch (ErrorException $e) {

					$server->status(SnifferServers::STATUS_UNEXPECTED);
					$server->message = 'С сервера получены некорректные данные';
					self::log('Полученные данные невалидны');
				}

				$data_correct = $this->check($server, --$try_count);

				if ($data_correct === false && $try_count > 0) {
					self::log('Показатели невалидны. Пауза перед последующим запросом: ' . self::SCAN_TRY_PAUSE . ' сек');
					sleep(self::SCAN_TRY_PAUSE);
				}

			} while ($data_correct === false && $try_count > 0);

			if ($scan) {
				$server->save();
			}
		}

		self::log('Сканирование завершено');
		return true;
	}

	public function actionCheckServers()
	{
		self::log('Проверка серверов');

		$servers = SnifferServers::find();

		if (!empty($this->argv[3])) {
			$servers->andWhere(['id' => $this->argv[3]]);
		}

		/** @var SnifferServers $server */
		foreach ($servers->each() as $server) {

			self::log('Обработка сервера ' . $server->id . ' - ' .
				long2ip($server->ip) . ' - ' .
				$server->name . ' (' .
				$server->group->name . ')');

			$this->check($server, 0);
		}

		self::log('Проверка завершена');
		return true;
	}

	/**
	 * @param $server SnifferServers
	 * @param $try_count integer
	 * @return bool|void
	 */
	private function check($server, $try_count)
	{
		$parameters = SnifferMonitoringParameters::findAll([
			'server_id' => $server->id
		]);

		$notifications = SnifferMonitoringParameters::getNotificationsList();

		$users = [];

		foreach ($parameters as $parameter) {

			$name = $parameter->parameter;
			$value = $server->$name;
			$previous_value = $server->getLastParameter($name);

			if (!$parameter->compare($value)) {
				continue;
			}

			if (!is_null($previous_value) && $parameter->compare($previous_value)) {
				continue;
			}

			if ($try_count > 0) {
				return false;
			}

			if (empty($users[$parameter->user_id])) {
				$users[$parameter->user_id] = [];
			}

			foreach ($notifications as $notification => $description) {

				if (!$parameter->$notification) {
					continue;
				}

				if (empty($users[$parameter->user_id][$notification])) {
					$users[$parameter->user_id][$notification] = [];
				}

				$users[$parameter->user_id][$notification][] =
					$parameter->getParameterDescription() . ' ' .
					$parameter->getOperatorDescription() . ' ' .
					$parameter->value . ' (' . $value . ')';
			}
		}

		if (count($users) > 0) {
			return $this->sendNotification($users, $server);
		}

		return true;
	}

	private function sendNotification($users, $server)
	{
		foreach ($users as $user_id => $data) {

			$user = SnifferUsers::findOne($user_id);

			if (is_null($user)) {
				continue;
			}

			if (self::NOTIFY_USERS) {
				$user->notify($data, $server);
			}
		}

		return count($users);
	}

	public function pingDomain($domain){
		$starttime = microtime(true);

		try {
			$file = fsockopen ($domain, 80, $errno, $errstr, 10);
		} catch (ErrorException $e) {
			return -1;
		}

		$stoptime  = microtime(true);

		if (!$file) $status = -1;  // Site is down
		else {
			fclose($file);
			$status = ($stoptime - $starttime) * 1000;
			$status = ceil($status);
		}
		return $status;
	}

	public function actionCheck()
	{
		$this->actionScan(false);
	}

	public function actionUpdate()
	{
		self::log('Запуск обновления файлов на серверах');

		$servers = SnifferServers::find();

		if (!empty($this->argv[3])) {
			$servers->andWhere(['id' => $this->argv[3]]);
		}

		/** @var SnifferServers $server */
		foreach ($servers->each() as $server) {

			self::log('Обработка сервера ' . $server->id . ' - ' .
				long2ip($server->ip) . ' - ' .
				$server->name . ' (' .
				$server->group->name . ')');

			$directory = Yii::getAlias('@app/components/Sniffer');

			$files = array_slice(scandir($directory), 2);

			$upload = [];

			foreach ($files as $file) {
				$upload[$file] = new \CURLFile($directory . DIRECTORY_SEPARATOR . $file);
			}

			self::log('Загружается файлов: ' . count($upload));

			$data = $server->upload($upload);

			if (empty($data)) {
				self::log('Файлы не обновлены');
				continue;
			}

			foreach ($data as $file => $information) {

				self::log(
					' - Файл: ' . $file .
					'; Удаление: ' . ($information['delete'] ? 'ok' : 'fail') .
					'; Обновление: ' . ($information['upload'] ? 'ok' : 'fail')
				);

			}

		}

		self::log('Обновление завершено');
		return true;
	}
}