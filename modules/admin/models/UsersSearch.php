<?php namespace app\modules\admin\models;

use app\models\Account;
use app\models\Invoice;
use app\models\Users;
use app\models\UsersLog;
use Faker\Provider\lv_LV\Payment;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UsersSearch extends Model
{
	public $id;
	public $mail;
	public $accounts;

	public function rules()
	{
		return [
			[['mail', 'accounts', 'id'], 'safe'],
			['id', 'integer'],
		];
	}

	public function search($params = null)
	{

		$subQuery = Account::find()->select('user_id, COUNT(id) AS accounts_count')->groupBy('user_id');

		$query = Users::find()->leftJoin(['a' => $subQuery], 'a.user_id = ' . Users::tableName() . '.id');

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['id' => SORT_DESC],
				'attributes' => [
					'id',
					'mail',
					'balance',
					'accounts' => [
						'asc' => ['a.accounts_count' => SORT_ASC],
						'desc' => ['a.accounts_count' => SORT_DESC]
					],
					'demo'
				]
			],
			'pagination' => [
				'pageSize' => 50
			],
		]);


		if ($params === null) {
			$params = \Yii::$app->request->queryParams;
		}


		if (!$this->load($params) || !$this->validate()) {
			return $dataProvider;
		}

		if ($this->mail) {
			$query->andWhere(['ILIKE', 'mail', trim($this->mail)]);
		}

		if ($this->id) {
			$query->andWhere([Users::tableName() . '.id' => trim($this->id)]);
		}

		if ($this->accounts) {
			$query->innerJoin(Account::tableName() . ' AS acc', 'acc.user_id = ' . Users::tableName() . '.id');
			$query->andWhere(['a.accounts_count' => intval($this->accounts)]);
		}

		if (!empty(\Yii::$app->request->queryParams['num_users'])) {
			$dataProvider->pagination->pageSize = \Yii::$app->request->queryParams['num_users'];
		}

		return $dataProvider;
	}
}