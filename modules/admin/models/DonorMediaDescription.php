<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "donor_media_descriptions".
 *
 * @property integer $id
 * @property integer $donor_media_id
 * @property string $language
 * @property string $text
 *
 * @property DonorMedia $donorMedia
 */
class DonorMediaDescription extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'donor_media_descriptions';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['donor_media_id', 'language', 'text'], 'required'],
			[['donor_media_id'], 'integer'],
			[['language', 'text'], 'string']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'donor_media_id' => 'Donor Media ID',
			'language' => 'Language',
			'text' => 'Text',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDonorMedia()
	{
		return $this->hasOne(DonorMedia::className(), ['id' => 'donor_media_id']);
	}
}
