<?php

namespace app\modules\admin\models;

use app\models\MonitoringLog;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Monitoring;
use yii\db\Expression;

/**
 * MonitoringSearch represents the model behind the search form about `app\models\Monitoring`.
 */
class MonitoringSearch extends Monitoring
{
	public $categoriesCount;
	
	public $date;
	public $dateFrom;
	public $dateTo;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'value', 'period', 'comparation', 'sms'], 'integer'],
			[['title', 'email'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Monitoring::find()->orderBy('category');

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => false
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		$this->dateTo = strtotime(date('Y-m-d'));
		$this->date = Yii::$app->request->get('date', 'day');

		switch ($this->date) {
			case 'day':
			default:
				$this->dateFrom = $this->dateTo - 86400;
				break;

			case 'week':
				$this->dateFrom = $this->dateTo - 86400 * 7;
				break;

			case 'month':
				$this->dateFrom = $this->dateTo - 86400 * 30;
				break;

			case 'range':
				$this->dateFrom = strtotime(Yii::$app->request->get('from'));
				$this->dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$query->groupBy([Monitoring::tableName().'.id']);

		$query->andFilterWhere([
			'id'     => $this->id,
			'value'  => $this->value,
			'period' => $this->period,
			'sms'    => $this->sms,
			'comparation' => $this->comparation,
		]);
		
		$query->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'email', $this->email]);

		return $dataProvider;
	}
}
