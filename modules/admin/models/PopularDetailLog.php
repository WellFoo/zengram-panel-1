<?php

namespace app\modules\admin\models;

use app\models\Account;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "popular_detail_log".
 *
 * @property integer $insta_id
 * @property integer $account_id
 * @property string $date
 * @property string $media
 * @property string $text
 *
 * @property PopularAccounts $popular
 * @property Account $commenter
 */
class PopularDetailLog extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'popular_detail_log';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['insta_id', 'account_id', 'media', 'text'], 'required'],
			[['insta_id', 'account_id'], 'integer'],
			[['date'], 'safe'],
			[['text'], 'string'],
			[['media'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'insta_id'   => 'Instagram ID',
			'account_id' => 'Commenter ID',
			'date'       => 'Дата',
			'media'      => 'Медиа',
			'text'       => 'Текст',
		];
	}

	public function getPopular()
	{
		return self::hasOne(PopularAccounts::className(), ['instagram_id' => 'insta_id']);
	}

	public function getCommenter()
	{
		return self::hasOne(Account::className(), ['id' => 'account_id']);
	}
}
