<?php

namespace app\modules\admin\models;

use Yii;
use app\models\Users;
use app\models\Account;

/**
 * This is the model class for table "antispam_log".
 *
 * @property integer $id
 * @property string $media_id
 * @property string $comment_id
 * @property string $text
 * @property string $time_deleted
 * @property integer $user_id
 * @property integer $account_id
 * @property integer $commentator_id
 * @property string $commentator_login
 * @property string $media_code
 */
class AntispamLog extends \yii\db\ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'antispam_log';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['media_id', 'comment_id', 'text', 'user_id', 'account_id'], 'required'],
			[['text'], 'string'],
			[['time_deleted'], 'safe'],
			[['user_id', 'account_id', 'commentator_id'], 'integer'],
			[['media_id', 'comment_id', 'commentator_login', 'media_code'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'media_id' => 'Media ID',
			'comment_id' => 'Comment ID',
			'text' => 'Text',
			'time_deleted' => 'Time Deleted',
			'user_id' => 'User ID',
			'account_id' => 'Account ID',
			'commentator_id' => 'Commentator ID',
			'commentator_login' => 'Commentator Login',
			'media_code' => 'Media Code',
		];
	}

	public function getUsers()
	{
		return self::hasOne(Users::className(), ['id' => 'user_id']);
	}

	public function getAccount()
	{
		return self::hasOne(Account::className(), ['instagram_id' => 'account_id']);
	}
}
