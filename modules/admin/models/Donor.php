<?php

namespace app\modules\admin\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "donors".
 *
 * @property integer $id
 * @property string  $login
 * @property string  $insta_id
 * @property string  $avatar
 * @property string  $sex
 * @property mixed   $languages
 * @property integer $random_posting
 * @property string  $create_date
 * @property integer $completed
 *
 * @property DonorDescription[] $descriptions
 * @property DonorLink[]        $links
 * @property DonorMedia[]       $medias
 */
class Donor extends ActiveRecord
{
	const LANG_EN = 'en';
	const LANG_ES = 'es';
	const LANG_RU = 'ru';

	const MALE   = 'male';
	const FEMALE = 'female';

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'donors';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['login', 'insta_id', 'languages', 'sex'], 'required'],
			['languages', 'each', 'rule' => ['in', 'range' => [self::LANG_EN, self::LANG_ES, self::LANG_RU]]],
			['sex', 'in', 'range' => [self::MALE, self::FEMALE]],
			[['random_posting', 'completed'], 'boolean'],
			[['create_date'], 'safe'],
			[['login', 'avatar'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'login'          => 'Логин',
			'avatar'         => 'Аватар',
			'languages'      => 'Языки',
			'random_posting' => 'Рандомный постинг',
			'create_date'    => 'Дата добавления',
			'completed'      => 'Завершен',
		];
	}

	public static function sexesLabels()
	{
		return [
			self::MALE   => 'муж.',
			self::FEMALE => 'жен.'
		];
	}

	public static function languagesLabels()
	{
		return [
			self::LANG_EN => 'Английский',
			self::LANG_ES => 'Испанский',
			self::LANG_RU => 'Русский',
		];
	}
	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getDescriptions()
	{
		return $this->hasMany(DonorDescription::className(), ['donor_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLinks()
	{
		return $this->hasMany(DonorLink::className(), ['donor_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMedias()
	{
		return $this->hasMany(DonorMedia::className(), ['donor_id' => 'id']);
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if (is_array($this->languages)) {
				$this->languages = implode(',', $this->languages);
			}
			return true;
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function afterFind()
	{
		parent::afterFind();

		$this->languages = explode(',', $this->languages);
	}
}
