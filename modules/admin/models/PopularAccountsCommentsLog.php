<?php

namespace app\modules\admin\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "popular_accounts_comments_log".
 *
 * @property integer $id
 * @property integer $code
 * @property integer $date
 * @property string  $description
 * @property integer $instagram_id
 * @property integer $project_id
 *
 */
class PopularAccountsCommentsLog extends ActiveRecord
{

	const CODE_TRUNCATE        = 20;
	const CODE_ADDED_ROWS      = 30;
	const CODE_STATISTIC       = 40;
	const CODE_COMMENTED       = 50;
	const CODE_DELETED         = 55;
	const CODE_ERROR           = 60;
	const CODE_COMMENT_FAILED  = 70;
	const CODE_COMMENT_SUCCESS = 75;
	const CODE_EMPTY_COMMENT   = 80;
	const CODE_DELETE_FAILED   = 90;
	const CODE_DELETE_SUCCESS  = 95;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'popular_accounts_comments_log';
	}

	public function attributeLabels()
	{
		return [
			'instagram_id' => 'Instagram ID',
			'date'         => 'Дата',
			'code'         => 'Код действия',
		];
	}

	public static function codeDescriptions()
	{

		return [
			self::CODE_TRUNCATE      => 'Таблица комментирования популярных аккаунтов очищена.',
			self::CODE_EMPTY_COMMENT => 'В выбранном аккаунте не найдены комментарии.',

			self::CODE_STATISTIC => 'Не используется.',
			self::CODE_ERROR     => 'Ошибка при выполнении запроса',
			self::CODE_COMMENTED => 'Не используется.',
			self::CODE_DELETED => 'Не используется.',

			self::CODE_COMMENT_FAILED => 'Комментирование: ошибка.',
			self::CODE_COMMENT_SUCCESS => 'Комментирование: успешно.',
			self::CODE_DELETE_FAILED => 'Удаление: ошибка.',
			self::CODE_DELETE_SUCCESS => 'Удаление: успешно.',
			
			self::CODE_ADDED_ROWS => 'Выбраны аккаунты для работы.',
		];

	}

	public static function getCodeDescription($code)
	{
		return self::codeDescriptions()[$code];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['code'], 'required'],
			[['description'], 'safe'],
			[['id', 'date', 'instagram_id', 'project_id'], 'integer'],
			['code', 'in', 'range' => [
				self::CODE_TRUNCATE,
				self::CODE_ADDED_ROWS,
				self::CODE_STATISTIC,
				self::CODE_ERROR,
				self::CODE_COMMENTED,
				self::CODE_DELETED,
				self::CODE_EMPTY_COMMENT,
				self::CODE_COMMENT_FAILED,
				self::CODE_COMMENT_SUCCESS,
				self::CODE_DELETE_FAILED,
				self::CODE_DELETE_SUCCESS,
			]],
		];
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			if (!$this->description) {
				$this->description = self::getCodeDescription($this->code);
			}

			if (is_array($this->description)) {
				//$this->description = implode("\r\n", $this->description);
				$this->description = json_encode($this->description);
			}

			if (!$this->date) {
				$this->date = time();
			}

			return true;
		}

		return false;

	}

	public static function log($data){

		$log = new PopularAccountsCommentsLog();
		$log->load([$log->formName() => $data]);
		$log->save();

	}

	public static function getDescriptionData($log, $field, $empty = '-')
	{

		/** @var PopularAccountsCommentsLog $item */
		$description = json_decode($log->description, true, 512, JSON_BIGINT_AS_STRING);

		if (is_null($description)) {
			return $empty;
		}

		if (!empty($description[$field])) {
			return $description[$field];
		}

		return $empty;


	}
}
