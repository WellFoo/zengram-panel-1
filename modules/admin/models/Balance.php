<?php

namespace app\modules\admin\models;

use DateInterval;

Class Balance
{

	public static function formatBalance($seconds, $format = '%r%Dд, %Hч, %Iм, %Sc')
	{
		$interval = new DateInterval('PT' . abs($seconds) . 'S');
		if ($seconds < 0) {
			$interval->invert = 1;
		}
		$interval = self::recalculateDateInterval($interval);
		return $interval->format($format);
	}

	/**
	 * @param $object DateInterval
	 * @return DateInterval
	 */
	private static function recalculateDateInterval($object)
	{
		$seconds = self::to_seconds($object);
		//мы отображаем максимально дни, поэтому вычислять годы и месяцы нам не нужно
//		$object->y = floor($seconds / 60 / 60 / 24 / 365);
//		$seconds -= $object->y * 31536000;
//		$object->m = floor($seconds / 60 / 60 / 24 / 30);
//		$seconds -= $object->m * 2592000;
		$object->d = floor($seconds / 60 / 60 / 24);
		$seconds -= $object->d * 86400;
		$object->h = floor($seconds / 60 / 60);
		$seconds -= $object->h * 3600;
		$object->i = floor($seconds / 60);
		$seconds -= $object->i * 60;
		$object->s = $seconds;
		return $object;
	}

	private static function to_seconds($object)
	{
		return ($object->y * 365 * 24 * 60 * 60) +
		($object->m * 30 * 24 * 60 * 60) +
		($object->d * 24 * 60 * 60) +
		($object->h * 60 * 60) +
		($object->i * 60) +
		$object->s;
	}
}