<?php

namespace app\modules\admin\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "hashtags".
 *
 * @property integer $id
 * @property string $name
 * @property integer $place_id
 * @property integer $counter
 */
class Hashtags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hashtags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['counter'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'place_id' => 'Place ID',
            'counter' => 'Counter',
        ];
    }
    public function getPlaces(){
        return $this->hasMany(HashtagsPlaces::className(), ['hashtags_id' => 'id']);
    }
    public function setPlace($place_id){
        $place = new HashtagsPlaces();
        $place->place_id = $place_id;
        $place->hashtags_id = $this->id;
        $place->save();
    }
}
