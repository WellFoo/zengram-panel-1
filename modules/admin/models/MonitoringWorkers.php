<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "monitoring_workers".
 *
 * @property integer $id
 * @property string $starttime
 * @property string $type_event
 * @property string $worker
 * @property string $description
 * @property integer $monitoring_id
 */
class MonitoringWorkers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    const MEMORY_OVERLOAD = 'memory_overload';
    const PING_LOSS = 'ping_loss';
    const PING_ALIVE = 'ping_alive';

    public static function tableName()
    {
        return 'monitoring_workers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'monitoring_id'], 'integer'],
            [['starttime'], 'safe'],
            [['type_event'], 'string'],
            ['type_event', 'in', 'range' => [self::MEMORY_OVERLOAD, self::PING_LOSS, self::PING_ALIVE]],
            [['worker'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'starttime' => 'Время',
            'type_event' => 'Событие',
            'worker' => 'Имя сервера',
            'description' => 'Описание',
            'monitoring_id' => 'Monitoring ID',
        ];
    }
}
