<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Proxy;
use app\models\ProxyCat;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProxyCatController extends Controller
{
	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public function actionCreate()
	{
		$model = new ProxyCat();
		$model->load(Yii::$app->request->post());

		if ($model->validate() && $model->save()) {
			return Yii::$app->response->redirect(['/admin/proxy', 'cat' => $model->id]);
		} else {
			return Yii::$app->response->redirect(['/admin/proxy']);
		}
	}

	public function actionValidate()
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException('AJAX only');
		}
		Yii::$app->response->format = Response::FORMAT_JSON;

		$model = new ProxyCat();
		$model->load(Yii::$app->request->post());

		return ActiveForm::validate($model);
	}

	public function actionSetDefault($id)
	{
		/* @var $model ProxyCat */
		$model = ProxyCat::findOne($id);
		if ($model === null) {
			throw new BadRequestHttpException('Item with ID #'.$id.' is not exists');
		}
		$model->default = true;
		$model->save();

		return Yii::$app->response->redirect(['/admin/proxy', 'cat' => $model->id]);
	}

	public function actionRemove($id)
	{
		/* @var $model ProxyCat */
		$model = ProxyCat::findOne($id);
		if ($model === null) {
			throw new BadRequestHttpException('Item with ID #'.$id.' is not exists');
		}
		Proxy::deleteAll(['cat_id' => $model->id]);
		$model->delete();

		return Yii::$app->response->redirect(['/admin/proxy']);
	}

}