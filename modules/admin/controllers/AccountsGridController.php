<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Account;
use app\models\Users;
use app\modules\admin\components\Settings;
use app\modules\admin\models\AccountsGrid;
use app\modules\admin\models\AccountsGridForm;
use app\modules\admin\models\CommentingProject;
use app\modules\admin\models\PopularAccounts;
use app\modules\admin\models\PopularAccountsCommentsLog;
use app\modules\admin\models\PopularAccountsCommentsLogSearch;
use app\modules\admin\models\PopularDetailLog;
use app\modules\admin\models\PopularLog;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class AccountsGridController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'except' => [
					'popular-log',
					'popular-accounts'
				],
				'rules' => [
					[
						'allow' => true,
						'matchCallback' => function ($rule, $action) {
							return Yii::$app->user->identity->isAdmin;
						}
					]
				]
			]
		];
	}

	public function actionIndex()
	{
		$dataProvider = new ActiveDataProvider([
			'query' => AccountsGrid::find()->with(['user', 'account']),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider
		]);
	}

	public function actionImport()
	{
		Yii::$app->db->createCommand(
			'INSERT INTO accounts_grid (user_id, account_id, create_date)
			SELECT user_id, id, extract(epoch FROM NOW()) FROM accounts
			WHERE user_id IN (SELECT id FROM users WHERE is_service = 1)
			AND id NOT IN (SELECT account_id FROM accounts_grid)
			ORDER BY user_id, id;'
		)->execute();
		return $this->redirect(['/admin/accounts-grid']);
	}

	public function actionCreate()
	{
		return $this->actionEdit();
	}

	public function actionEdit($id = null)
	{
		$model = new AccountsGridForm();
		$model->scenario = 'create';

		if ($id !== null) {
			/** @var $grid AccountsGrid */
			$grid = AccountsGrid::findOne($id);
			if ($grid === null) {
				throw new NotFoundHttpException('Записи с таким ID не найдено');
			}
			$model->load(['AccountsGridForm' => $grid->getAttributes()]);
			$model->grid = $grid;
			$model->account_login = $grid->account->login;
			$model->account_password = $grid->account->password;
			$model->email_password = $grid->email_pwd;
			$model->scenario = 'update';
		}

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['/admin/accounts-grid']);
		}

		return $this->render('edit', [
			'model' => $model
		]);
	}

	public function actionPopularAccounts()
	{
		$query = PopularAccounts::find();

		$count_all = $query->count();
		$count_query = (clone $query);
		$count_30k = $count_query
			->where(['>=', 'follower_count', 30000])
			->andWhere(['<', 'follower_count', 100000])
			->count();
		$count_100k = $count_query
			->where(['>=', 'follower_count', 100000])
			->andWhere(['<', 'follower_count', 500000])
			->count();
		$count_500k = $count_query
			->where(['>=', 'follower_count', 500000])
			->andWhere(['<', 'follower_count', 1000000])
			->count();
		$count_1m = $count_query
			->where(['>=', 'follower_count', 1000000])
			->count();

		$type = Yii::$app->request->get('type', '30K');
		switch ($type) {
			case '1M':
				$query->where(['>=', 'follower_count', 1000000]);
				break;
			case '500K':
				$query->where(['>=', 'follower_count', 500000])
					->andWhere(['<', 'follower_count', 1000000]);
				break;
			case '100K':
				$query->where(['>=', 'follower_count', 100000])
					->andWhere(['<', 'follower_count', 500000]);
				break;
			case '30K':
			default:
				$query->where(['>=', 'follower_count', 30000])
					->andWhere(['<', 'follower_count', 100000]);
				break;
		}

		$data_provider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['follower_count' => SORT_DESC]
			]
		]);

		return $this->render('popular-accounts', [
			'data_provider' => $data_provider,
			'type'          => $type,
			'count_all'     => $count_all,
			'count_30k'    => $count_30k,
			'count_100k'    => $count_100k,
			'count_500k'    => $count_500k,
			'count_1m'      => $count_1m,
		]);
	}

	public function actionPopularLog()
	{
		$insta_id = Yii::$app->request->get('insta_id', null);
		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');
		$user = Yii::$app->request->get('user', 0);

		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$query = PopularLog::find()
			->with(['account', 'user'])
			->andWhere(['>=', 'date', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'date', date('Y-m-d', $dateTo)]);

		if ($user == -1) {
			$query->andWhere('user_id IS NULL');
		} else if ($user == -2) {
			$query->andWhere('user_id IS NOT NULL');
		} else if ($user > 0) {
			$query->andWhere([
				'project_id' => $user
			]);
		}

		if (!empty($insta_id)) {
			$query->andWhere(['instagram_id' => $insta_id]);
		}

		$data_provider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['id' => SORT_DESC]
			]
		]);

		// Коэффициент комментатора
		$commentator_query = PopularAccountsCommentsLog::find()
			->andWhere(['>=', 'date', $dateFrom])
			->andWhere(['<', 'date', $dateTo])
			->andWhere(['code' => PopularAccountsCommentsLog::CODE_COMMENT_SUCCESS]);

		if ($user > 0) {
			$commentator_query->andWhere([
				'project_id' => $user
			]);
		}

		$data = [];

		/* @var $log PopularAccountsCommentsLog */
		foreach ($commentator_query->each() as $log) {

			if (empty($data[$log->project_id])) {
				$data[$log->project_id] = [];
			}

			$commentator = PopularAccountsCommentsLog::getDescriptionData($log, 'user_id', false);

			if ($commentator === false) {
				continue;
			}

			if (empty($data[$log->project_id][$commentator])) {
				$data[$log->project_id][$commentator] = 1;
			} else {
				$data[$log->project_id][$commentator]++;
			}

		}

		$coefficient_data = [];

		foreach ($data as $project => $project_data) {

			$commentators = 0;
			$comments     = 0;
			$max_comments = 0;

			foreach ($project_data as $commentator => $count) {

				$commentators++;
				$comments += $count;

				$max_comments = $max_comments > $count ? $max_comments : $count;

			}

			$coefficient_data[$project] = [
				'project' => CommentingProject::findOne($project)->name,
				'count' => $comments,
				'max' => $max_comments,
				'coeff' => round($comments / $commentators, 4),
			];

		}

		$coefficient_data_provider = new ArrayDataProvider([
			'allModels' => $coefficient_data,
		]);

		return $this->render('popular-log', [
			'data_provider' => $data_provider,
			'coefficient'   => $coefficient_data_provider,
			'date'          => $date,
			'dateFrom'      => $dateFrom,
			'dateTo'        => $dateTo,
			'insta_id'      => $insta_id,
			'user'          => $user,
			'sum'           => (int) $query->sum(PopularLog::tableName().'.comments'),
		]);
	}

	public function actionPopularDetailLog($insta_id)
	{
		$popular = PopularAccounts::findOne(['instagram_id' => $insta_id]);
		if (is_null($popular)) {
			throw new NotFoundHttpException('Аккаунт ' . $insta_id . ' не найден');
		}

		$dateTo = strtotime(date('Y-m-d'));
		$date = Yii::$app->request->get('date', 'day');

		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 7;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 30;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from'));
				$dateTo = strtotime(Yii::$app->request->get('to'));
				break;
		}

		$query = PopularDetailLog::find()
			->with(['commenter'])
			->where(['insta_id' => $insta_id])
			->andWhere(['>=', 'date', date('Y-m-d', $dateFrom)])
			->andWhere(['<', 'date', date('Y-m-d', $dateTo)]);

		$data_provider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['date' => SORT_DESC]
			]
		]);

		return $this->render('popular-details-log', [
			'data_provider' => $data_provider,
			'date'          => $date,
			'dateFrom'      => $dateFrom,
			'dateTo'        => $dateTo,
			'popular'       => $popular
		]);
	}

	public function actionCommentDetailLog($insta_id = null)
	{

		//$dateTo = time();
		$dateTo = strtotime(date('Y-m-d 23:59'));

		$date = Yii::$app->request->get('date', 'day');

		switch ($date) {
			case 'day':
			default:
				$dateFrom = $dateTo - 86400 * 2;
				break;

			case 'week':
				$dateFrom = $dateTo - 86400 * 8;
				break;

			case 'month':
				$dateFrom = $dateTo - 86400 * 31;
				break;

			case 'range':
				$dateFrom = strtotime(Yii::$app->request->get('from') . ' 00:00');
				$dateTo = strtotime(Yii::$app->request->get('to') . ' 23:59');
				break;
		}

		$query = PopularAccountsCommentsLog::find()
			->where(['>=', 'date', $dateFrom])
			->andWhere(['<=', 'date', $dateTo]);

		$user = Yii::$app->request->get('user', 0);

		if ($user == -1) {
			$query->andWhere('user_id IS NULL');
		} else if ($user == -2) {
			$query->andWhere('user_id IS NOT NULL');
		} else if ($user > 0) {
			$query->andWhere([
				'project_id' => $user
			]);
		}

		if ($insta_id !== null) {
			$query->andWhere(['instagram_id' => $insta_id]);
		}

		$text_filter = Yii::$app->request->get('text', '');

		if ($text_filter !== '') {
			$query->andWhere([
				'like', 'description', str_replace('"', '', json_encode($text_filter))
			]);
		}

		$type = Yii::$app->request->get('type', 0);

		$summary = [];
		
		switch ($type) {
			case 0:
			default:
				$data_provider = new PopularAccountsCommentsLogSearch();
				$data_provider->query = $query;

				$search_model = $data_provider;
				$data_provider = $data_provider->search(Yii::$app->request->queryParams);
				break;
			case 1:

				$log_data = [];
				
				// Жонглирование костылями
				$search_model = [];
				$key = 0;

				$query->andWhere(['or',
					['code' => PopularAccountsCommentsLog::CODE_COMMENT_FAILED],
					['code' => PopularAccountsCommentsLog::CODE_COMMENT_SUCCESS],
				]);

				/* @var $log PopularAccountsCommentsLog */
				foreach ($query->each() as $log) {

					$text = PopularAccountsCommentsLog::getDescriptionData($log, 'text');

					if (empty($log_data[$text])) {

						$log_data[$text] = [
							'text' => $text,
							'count' => 1
						];

					} else {
						$log_data[$text]['count']++;
					}

					if (empty($summary['count'])) {
						$summary['count'] = 1;
					} else {
						$summary['count']++;
					}

					if ($log->code == PopularAccountsCommentsLog::CODE_COMMENT_SUCCESS) {

						if (empty($log_data[$text]['success'])) {
							$log_data[$text]['success'] = 1;
						} else {
							$log_data[$text]['success']++;
						}

						if (empty($summary['success'])) {
							$summary['success'] = 1;
						} else {
							$summary['success']++;
						}

					} else {

						$error = PopularAccountsCommentsLog::getDescriptionData($log, 'error');

						if (is_array($error)) {
							$error = 'NULL';
						}

						if (!in_array($error, $search_model)) {
							$search_model[$key++] = $error;
						}


						$error_key = array_search($error, $search_model);

						if (empty($log_data[$text][$error_key])) {
							$log_data[$text][$error_key] = 1;
						} else {
							$log_data[$text][$error_key]++;
						}

						if (empty($summary[$error_key])) {
							$summary[$error_key] = 1;
						} else {
							$summary[$error_key]++;
						}
					}

				}

				$sort = [
					'text', 'count', 'success'
				];

				foreach ($search_model as $key => $value) {
					$sort[] = $key;
				}

				$data_provider = new ArrayDataProvider([
					'allModels' => $log_data,
					'sort' => [
						'attributes' => $sort,
						'defaultOrder' => 'DESC',
					],
				]);



				break;
		}
		
		return $this->render('comments-log', [
			'data_provider' => $data_provider,
			'searchModel'   => $search_model,
			'date'          => $date,
			'dateFrom'      => $dateFrom,
			'dateTo'        => $dateTo,
			'user'          => $user,
			'insta_id'      => $insta_id,
			'type'          => $type,
			'text'          => $text_filter,
			'summary'       => $summary,
		]);
	}
}