<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Post;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;

class BlogController extends Controller
{
	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public function actionIndex()
	{
		$posts = Post::find()->all();

		return $this->render('index', [
			'posts' => $posts,
		]);
	}

	public function actionCreate()
	{
		return $this->actionView();
	}

	public function actionView($id = null)
	{
		if ($id == null) {
			$model = new Post();
		} else {
			$model = Post::findOne($id);
		}

		if ($model == null) {
			throw new HttpException(404, 'Model not found.');
		}

		if (isset($_POST['Post'])) {
			$model->load($_POST);
			if ($model->save()) {
				Yii::$app->session->setFlash('success', 'Post has been saved');
				Yii::$app->response->redirect(['/admin/blog']);
			} else {
				Yii::$app->session->setFlash('error', 'Post could not be saved');
			}
		}

		return $this->render('edit', [
			'model' => $model,
		]);
	}

	public function actionDelete($id)
	{
		if (!$id) {
			Yii::$app->session->setFlash('Post Deleted Error');
			Yii::$app->getResponse()->redirect(['admin/posts']);
			return;
		}

		/* @type Post $post */
		$post = Post::findOne(['id' => $id]);

		if ($post == null || !$post->id) {
			throw new ForbiddenHttpException(Yii::t('app', 'You cannot view that resource'));
		}
		$post->delete();

		Yii::$app->session->setFlash('PostDeleted');
		Yii::$app->response->redirect(['/admin/blog']);
	}
}