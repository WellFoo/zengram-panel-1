<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Page;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use Yii;

class PagesController extends Controller
{
	/**
	 * Initializes the object.
	 * This method is invoked at the end of the constructor after the object is initialized with the
	 * given configuration.
	 */
	public function init()
	{
		parent::init();

		if (!Yii::$app->user->identity->isAdmin) {
			throw new ForbiddenHttpException('У вас нет прав для просмотра этой страницы.');
		}
	}

	public function actionIndex()
	{
		$pages = Page::find()->asArray()->all();

		return $this->render('pages',
			[
				'pages' => $pages,
			]);
	}

	public function actionEdit($id = null)
	{
		if ($id == null) {
			$model = new Page();
		} else {
			$model = Page::findOne($id);
		}

		if ($model == null) {
			throw new HttpException(404, 'Model not found.');
		}

		if (isset($_POST['Page'])) {
			$model->load($_POST);
			if ($model->save()) {
				Yii::$app->session->setFlash('success', 'Page has been saved');
				Yii::$app->response->redirect(['/admin/pages']);
			} else {
				Yii::$app->session->setFlash('error', 'Page could not be saved');
			}
		}

		return $this->render('page_edit',
			[
				'model' => $model,
			]);
	}

	public function actionDelete($id)
	{
		if (!$id) {
			Yii::$app->session->setFlash('Page Deleted Error');
			Yii::$app->getResponse()->redirect(['admin/pages']);
			return;
		}

		/* @type Page $page */
		$page = Page::findOne(['id' => $id]);

		if ($page == null || !$page->id) {
			throw new ForbiddenHttpException(Yii::t('app', 'You cannot view that resource'));
		}
		$page->delete();

		Yii::$app->session->setFlash('PapgeDeleted');
		Yii::$app->getResponse()->redirect(['admin/pages']);
	}

}