<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Invoice;
use app\modules\admin\models\PayedUsersSearch;
use Yii;
use yii\web\Response;

class PayedUsersController extends Controller
{

	public function actionIndex()
	{
		$searchModel = new PayedUsersSearch();
		$dataProvider = $searchModel->search();

		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider
		]);
	}

	public function actionPaymentLog($id)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		/** @var Invoice[] $payments */
		$payments = Invoice::find()->where(['user_id' => $id, 'status' => Invoice::STATUS_SUCCESS])
				->orderBy(['date' => SORT_DESC])->joinWith('price')->all();
		$data = ['<div class="row"><div class="col-md-4 col-sm-4 col-xs-4">Дата</div>
				<div class="col-md-3 col-sm-3 col-xs-3">Тип</div>
				<div class="col-md-2 col-sm-2 col-xs-2">Кол-во дней</div>
				<div class="col-md-3 col-sm-3 col-xs-3">Цена</div></div>'];
		foreach ($payments as $payment)
		{
			$data[] = '<div class="row"><div class="col-md-4 col-sm-4 col-xs-4">'. $payment->date. ' МСК</div>
			<div class="col-md-3 col-sm-3 col-xs-3">'.$payment->agent.'</div>
			<div class="col-md-2 col-sm-2 col-xs-2">'.$payment->price->value.'</div>
			<div class="col-md-3 col-sm-3 col-xs-3">'.$payment->price->price.' <span class="fa fa-rub"></span></div></div>';
		}
		return ['data' => $data, 'status' => 'ok'];
	}

}