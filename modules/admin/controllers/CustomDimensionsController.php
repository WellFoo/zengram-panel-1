<?php

namespace app\modules\admin\controllers;

use app\components\Controller;
use app\models\Users;
use app\modules\admin\models\PayedUsersSearch;
use Yii;
class CustomDimensionsController extends Controller
{
	public function actionIndex()
	{
		$searchModel = new PayedUsersSearch();
		$dataProvider = $searchModel->searchNoDimensions();

		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider
		]);
	}
	public function actionChange(){
		/* @var Users $user */
		$user = Users::findOne(Yii::$app->request->get('id'));
		$user->gender = Yii::$app->request->get('gender');
		$user->person = Yii::$app->request->get('person');
		$user->save();
		return $this->redirect('/admin/custom-dimensions/');
	}

}