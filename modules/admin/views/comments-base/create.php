<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CommentsBase */

$this->title = Yii::t('app', 'Create Commentsbase');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Commentsbases'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="commentsbase-create site-admin">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_form', [
	'model' => $model,
	]) ?>

</div>
