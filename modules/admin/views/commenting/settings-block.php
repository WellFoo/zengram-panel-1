<?php

/**
 * @var $this yii\web\View
 * @var $id integer
 */

use app\modules\admin\components\Settings;
use app\modules\admin\models\CommentingProject;
use yii\helpers\Html;

/* @var $settings CommentingProject */
$settings = CommentingProject::findOne($id);

if (is_null($settings)) {
	return '';
}

?>

<div class="comment-settings col-sm-6 col-lg-4" data-id="<?= $settings->id ?>">

	<?php

	$form = \yii\widgets\ActiveForm::begin();

	echo $form->field($settings, 'name');

	echo $form->field($settings, 'popular_category')->dropDownList([
		CommentingProject::CATEGORY_30   => '30K+',
		CommentingProject::CATEGORY_100  => '100K+',
		CommentingProject::CATEGORY_500  => '500K+',
		CommentingProject::CATEGORY_1000 => '1M+',
	], ['multiple' => 'true']);
	echo $form->field($settings, 'post_count');
	echo $form->field($settings, 'expired_time');

	echo $form->field($settings, 'count_per_iteration');
	echo $form->field($settings, 'time_from');
	echo $form->field($settings, 'time_until');
	echo $form->field($settings, 'comment_account');
	echo $form->field($settings, 'max_random_image');
	echo $form->field($settings, 'obfuscation')->checkbox();

	?>

	<div class="row">

		<div class="col-sm-2">
			<?= Html::a('Стоп', ['#'], [
				'class' => (($settings->cron_status == Settings::STATUS_STOP) ? 'btn btn-danger' : 'btn btn-default').' ',
				'data' => [
					'action' => 'stop'
				]
			]) ?>
		</div>

		<div class="col-sm-3">

			<?= Html::a('Пауза', ['#'], [
				'class' => (($settings->cron_status == Settings::STATUS_PAUSE) ? 'btn btn-warning' : 'btn btn-default').' ',
				'data' => [
					'action' => 'pause'
				]
			]) ?>
		</div>

		<div class="col-sm-3">
			<?= Html::a('Старт', ['#'], [
				'class' => (($settings->cron_status == Settings::STATUS_ACTIVE) ? 'btn btn-success' : 'btn btn-default').' col-sm-12',
				'data' => [
					'action' => 'start'
				]
			]) ?>
		</div>

		<div class="col-sm-4">
			<a href="#" class="btn btn-default col-sm-12" data-action="delete">Удалить</a>
		</div>

	</div>


	<?php
		$form::end();
	?>

</div>

