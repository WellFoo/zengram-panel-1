<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\web\View;
use dosamigos\tinymce\TinyMce;

$this->title = 'Пост';
//$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('@web/js/translit.js', ['position' => View::POS_BEGIN]);
?>
<div class="site-admin">

	<?php

	$form = ActiveForm::begin([
		'options'     => ['class' => 'form-horizontal', 'role' => 'form'],
		//'action' => '/admin/setting/'.$model->id,
		'fieldConfig' => [
			'template'     => "{label}\n<div class=\"col-sm-11\">{input}</div>\n<div class=\"col-sm-1\"></div><div class=\"col-sm-11\">{error}</div>",
			'labelOptions' => ['class' => 'col-sm-1 control-label'],
		],
	]);

	echo $form->field($model, 'title')->textInput(['id' => 'title-input', 'onkeyup' => 'fillAlias()']);
	echo $form->field($model, 'alias')->textInput(['id' => 'alias-input']);
	echo $form->field($model, 'date')->input('text', ['class' => 'form-control date-picker', 'data-date-format' => 'yyyy-mm-dd']);
	echo $form->field($model, 'image');
	echo $form->field($model, 'text_short')->textarea();
	echo $form->field($model, 'text')->widget(TinyMce::className(), [
		'options' => ['rows' => 12],
		'language' => 'ru',
		'clientOptions' => [
			'plugins' => [
				"advlist autolink lists link charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media image table contextmenu paste"
			],
			'toolbar' => "undo redo | styleselect | bold italic | valigntop valignmiddle valignbottom | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
		]
	]);
	echo $form->field($model, 'meta_description')->textarea();

	echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);

	ActiveForm::end();
	?>
	<h2>Инструкция по загрузке</h2>
	<blockquote>
		<ul>
			<li>Введите тему поста блога в поле "Заголовок"</li>
			<li>Выберите дату, к которой привязан пост блога</li>
			<li>Выберите язык сайта, к которому привязан данный пост</li>
			<li>Введите прямую ссылку на изображение, оно автоматически загрузится на сервер. Пропорции желательно
				выбирать максимально близко к 3.5:1(в идеале 950x300). Максимальная высота автоматически урезается до
				300px</li>
			<li>Введите текст поста. Т.к. там используется html редактор, можно применять форматирование к тексту
			(например, текст выделить курсивом или вставить ссылку)</li>
			<li>Нажмите "Сохранить"</li>
		</ul>
	</blockquote>
	<script>
		function fillAlias() {
			$('#alias-input').val(ru2en.translit($('#title-input').val()));
		}
	</script>
</div>