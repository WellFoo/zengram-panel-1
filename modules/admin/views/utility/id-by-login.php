<?php

/* @var $this yii\web\View
 */

$this->title = 'Получение ID по логину';
?>

<style>
	.form {
		width: 500px;
		margin: 0 auto;
		text-align: center;
	}

	.loading {
		background: url(/images/main-square.gif) no-repeat right 5px center;
		background-size: 20px 20px;
	}
</style>

<div class="form">
	<h2>Логин:</h2>
	<input type="text" id="login" class="form-control">


	<h2>ID:</h2>
	<div id="result">

	</div>
</div>

<script>

	var $login = $('#login');

	$login.change(function(){

		$login.addClass('loading');

		$.post(
			'/admin/utility/id-by-login-action/',
			{login: $(this).val()},
			function (responce) {
				$('#result').html(responce.data);
			},
			'json'
		).always(function(){
			$login.removeClass('loading');
		});

	});


</script>