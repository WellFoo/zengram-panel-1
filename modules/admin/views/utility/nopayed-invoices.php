<form method = "GET">
    <input name = "date" type = "date" />
    <input type = "submit" value = "Перейти" />
</form> <br /><br />

<?php
use yii\grid\GridView;
use yii\widgets\Pjax;



Pjax::begin();
echo GridView::widget([
    'id'             => 'users-list',
    'dataProvider'   => $dataProvider,
    'filterSelector' => '[name="num_users"]',
    'pager'          => [
        'firstPageLabel' => 'Перв',
        'lastPageLabel'  => 'Посл'
    ],
    'columns'        => [
        [
            'label'         => 'id',
            'attribute'     => 'id',
            'headerOptions' => ['style' => 'width: 7%;'],
            'format'        => 'raw',
            'value'         => function ($item) {
                return $item->id;
            }
        ], [
            'label'     => 'Пользователь',
            'attribute' => 'user_id',
            'format'    => 'raw',
            'value'     => function ($item) {
                return $item->user->mail;
            }
        ], [
            'label'     => 'Дата',
            'attribute' => 'date',
            'format'    => 'raw',
            'value'     => function ($item) {
                return $item->date;
            }
        ], [
            'label'     => 'Сумма платежа',
            'attribute' => 'sum',
            'format'    => 'raw',
            'value'     => function ($item) {
                return $item->price->price;
            }
        ]
    ]
]);
?>
<?php Pjax::end(); ?>