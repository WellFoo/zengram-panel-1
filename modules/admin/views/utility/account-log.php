<?php

/* @var $this yii\web\View
 * @var $account \app\models\Account
 * @var $dateTo
 * @var $dateFrom
 * @var $hoursFrom
 * @var $hoursTo
 * @var $date
 * @var $worker
 * @var $group
 * @var $envinroment
 * @var $text_search
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use core\logger\Logger;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;

$this->title = 'Лог аккаунта';


$request = '/admin/utility/account-log-update?';
$params = '';
foreach (Yii::$app->request->get() as $param => $value) {
	$params .= ($params == '' ? '' : '&') . $param . '=' . $value;
}
$request = $request . $params;

ActiveForm::begin([
	'method' => 'get',
	'action' => Url::to(['/admin/utility/account-log/'.$account->instagram_id]),
	'options' => [
		'class' => 'form-horizontal',
		'style' => 'margin: 20px 0;'
	]
]);

$hours = [];

for ($i = 0; $i < 24; $i++) {
	$hours[$i] = $i;
}

?>


<?= 'Аккаунт '.Html::a(
		'@'.$account->login,
		'https://instagram.com/'.$account->login.'/',
		[
			'target' => '_blank',
		]
	);
?>

<br><br>

<div class="row">
	<div class="btn-group" role="group" aria-label="...">
		<button value="day"
		        class="day-button btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
		</button>
		<button value="week"
		        class="day-button btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
		</button>
		<button value="month"
		        class="day-button btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
		</button>
	</div>
	<input name="date" type="hidden" value="<?= $date ?>">
	<div class="col-xs-4">
			<span class="input-group-btn">
					<?=Html::dropDownList(
						'worker',
						$worker,
						array_merge(['Воркер...'], Yii::$app->params['workers']), [
						'class' => 'form-control trigger-filter'
					])
					?>
				</span>
		<span class="input-group-btn">
					<?=Html::dropDownList(
						'envinroment',
						$envinroment,
						([-1 => 'Функционал'] + Logger::getEnvironmentDescriptions()), [
						'class' => 'form-control trigger-filter'
					])
					?>
				</span>
		<span class="input-group-btn">
					<?php
					$code_croups = [];

					if ($envinroment !== -1) {
						$code_croups = Logger::getGroupBranch($envinroment);
					}

					echo Html::dropDownList(
						'group',
						$group,
						([-1 => 'Группа кодов'] + $code_croups), [
						'class' => 'form-control trigger-filter'
					]) ?>
				</span>
	</div>

	<div class="col-xs-5">
		<div class="input-group">
			<label for="from" class="input-group-addon">с</label>
			<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
			       class="form-control date-picker" data-date-format="yyyy-mm-dd">
			<label for="to" class="input-group-addon">по</label>
			<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
			       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
		</div>
	</div>

</div>

<br>

<div class="col-xs-8">
	<div class="input-group">
		<label for="from" class="input-group-addon">Часы, с</label>
		<?=Html::dropDownList(
			'hoursFrom',
			$hoursFrom,
			$hours, [
			'class' => 'form-control trigger-filter'
		])
		?>
		<label for="to" class="input-group-addon">по</label>
		<?=Html::dropDownList(
			'hoursTo',
			$hoursTo,
			$hours, [
			'class' => 'form-control trigger-filter'
		])
		?>

		<label class="input-group-addon">Поиск</label>
		<?=Html::input(
			'text',
			'text-search',
			$text_search, [
			'class' => 'form-control trigger-filter'
		])
		?>
	</div>
</div>

<br><br>

<?php ActiveForm::end(); ?>


<div id="data-table">
	<?= $this->render('account-log-table', [
		'dataProvider' => $dataProvider
	]) ?>
</div>


<style>
	.date {
		width: 118px;
	}

	table {
		font-size: .9em;
	}

	table td {
		position: relative;
	}

	.level-1 { /* info */

	}

	.level-2 { /* warning */
		background-color: #fcf8e3 !important;
	}

	.level-3 { /* error */
		background-color: #f2dede !important;
	}

	.level-4 { /* success */
		background-color: #dff0d8 !important;
	}

	.extender-group {
		background-color: rgba(255, 165, 0, 0.21) !important;
	}

	.no-code:after {
		display: block;
		background-color: red;
		width: 8px;
		height: 2px;
		position: absolute;
		left: -4px;
		top: calc(50% - 1px);
		content: "";
	}
</style>

<div id="processing-data" class="hide">

</div>

<script>
	var max_id = 0,
		requestBusy = false,
		$data = $('#processing-data'),
		$dataTable = $('#data-table tbody');

	$(document).on('ready', function(){

		setTimeout(function(){
			var scrollHeight = document.documentElement.scrollHeight,
				clientHeight = document.documentElement.clientHeight;

			scrollHeight = Math.max(scrollHeight, clientHeight);
			window.scrollTo(0, scrollHeight - document.documentElement.clientHeight)
		}, 100);

		setMaxId();

		setInterval(function(){

			if (requestBusy === true) {
				return false;
			}

			requestBusy = true;

			console.log('@');

			$.post(
				'<?= $request ?>',
				{max_id: max_id},
				function(response){

					requestBusy = false;

					if (response == false) return;

					$data.html(response);

					$dataTable.append($data.find('tbody tr'));

					$data.html('');

					setMaxId();
				},
				'json'
			);

		}, 20000);
	});

	function setMaxId()
	{
		max_id = $dataTable.find('tr:last-child').data('key');
	}
</script>