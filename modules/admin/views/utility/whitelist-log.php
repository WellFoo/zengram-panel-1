<?php

/* @var $this yii\web\View
 * @var $accounts
 */

use app\models\Account;
use yii\bootstrap\Html;

$this->title = 'Ошибочные whitelist\'ы';

$dataProvider = new \yii\data\ActiveDataProvider([
	'query' => $accounts,
	'pagination'    => [
		'pageSize'  => 5000,
	]
]);


echo \yii\grid\GridView::widget([
	'dataProvider' => $dataProvider,
	'rowOptions' => function ($item) {
		/** @var Account $item */
		return [
			'class' => 'type-'.$item->whitelist_added,
		];
	},
	'columns'      => [
		'instagram_id',
		[
			'format' => 'raw',
			'attribute' => 'login',
			'value' => function($item) {
				/** @var $item Account */
				return Html::a('@'.$item->login,
					'https://instagram.com/'.$item->login.'/',
					[
						'target' => '_blank'
					]);
			},
		],
		[
			'attribute' => 'user_id',
			'format' => 'raw',
			'value' => function($item) {
				/** @var $item Account */
				return Html::a(
					$item->user_id,
					['/admin/users?UsersSearch[id]=' . $item->user_id]
				);
			},
		],
		'added',
	],
]);

?>

<style>
	.type-2 { /* warning */
		background-color: #fcf8e3 !important;
	}

	.type-3 { /* error */
		background-color: #f2dede !important;
	}
</style>