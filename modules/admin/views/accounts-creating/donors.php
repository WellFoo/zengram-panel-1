<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $donors
 */
use yii\helpers\Html;

$this->title = 'Доноры';
?>
<div class="donors">

	<h2><?= $this->title ?></h2>

	<?= \yii\grid\GridView::widget([
		'dataProvider' => $donors,
		'columns' => [
			[
				'attribute' => 'login',
				'format'    => 'raw',
				'value'     => function ($item) {
					/** @var \app\modules\admin\models\Donor $item */
					$result = Html::img($item->avatar, ['width' => 64]);
					$result.= PHP_EOL;
					$result.= Html::a('@'.$item->login, ['donor-edit', 'id' => $item->id]);
					$result.= PHP_EOL;
					$result.= Html::tag('span', '('.$item::sexesLabels()[$item->sex].')', ['class' => 'text-muted']);
					return $result;
				}
			], [
				'attribute' => 'languages',
				'value'     => function($item) {
					/** @var \app\modules\admin\models\Donor $item */
					$list = $item->languages;
					foreach ($list as $key => $value) {
						$list[$key] = $item::languagesLabels()[$value];
					}
					return implode(', ', $list);
				}
			],
			'create_date:date'
		]
	]) ?>

	<?= Html::a('Добавить', ['donor-create'], ['class' => 'btn btn-success']) ?>
</div>
