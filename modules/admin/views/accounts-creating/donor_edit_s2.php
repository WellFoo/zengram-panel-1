<?php
use app\modules\admin\models\DonorForm;
use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var \yii\web\View $this
 * @var DonorForm $donor
 */

$this->title = $donor->isNew ? 'Новый донор' : 'Донор '.$donor->login;
?>
<div class="donor-edit-s2">
	<h2><?= $this->title ?></h2>

	<?php
	$form = ActiveForm::begin([]);

	echo $form->field($donor, 'random_posting')->radioList([
		0 => 'по порядку',
		1 => 'в случайном порядке',
	]);

	echo Html::beginTag('div', ['class' => 'row', 'style' => 'margin-bottom: 10px;']);
	echo Html::error($donor, 'medias', ['class' => 'help-block help-block-error']);
	foreach ($donor->getMediaList() as $media) {
		/** @var \app\modules\admin\models\DonorMedia $media */

		echo Html::beginTag('div', ['class' => 'media col-md-6']);

			echo Html::beginTag('div', ['class' => 'media-left']);
				echo Html::beginTag('a', ['href' => '#']);
					echo Html::img($media->url, ['class' => 'media-object', 'width' => 200]);
				echo Html::endTag('a');
			echo Html::endTag('div');

			echo Html::beginTag('div', ['class' => 'media-body']);

				echo Html::beginTag('div', ['class' => 'form-group']);
					echo Html::label('Использовать для', null, ['class' => 'control-label']);
					if (empty($donor->medias[$media->id]['use'])) {
						$donor->medias[$media->id]['use'] = 'posting';
					}
					echo Html::activeRadioList($donor, 'medias['.$media->id.'][use]', [
							'posting' => 'постинга',
							'avatar'  => 'аватара',
							'not_use' => 'не использовать',
						]);
				echo Html::endTag('div');

				echo Html::beginTag('div', ['class' => 'form-group']);
					echo Html::label('Описания', null, ['class' => 'control-label']);
					foreach ($donor->languages as $code) {
						echo Html::beginTag('div', [
								'class' => 'input-group',
								'style' => 'margin-bottom: 5px;'
							]);
						echo Html::tag('span', ucfirst($code), ['class' => 'input-group-addon']);
						echo Html::activeTextarea($donor, 'medias['.$media->id.'][description]['.$code.']', [
								'class' => 'form-control',
							]);
						echo Html::endTag('div');
					}
				echo Html::endTag('div');

			echo Html::endTag('div');

		echo Html::endTag('div');
	}
	echo Html::endTag('div');
	?>

	<div class="form-group">
		<?= Html::submitButton('Готово', ['class' => 'btn btn-success']) ?>
		<?= Html::a('Назад', ['donor-edit', 'id' => $donor->id], ['class' => 'btn btn-primary']) ?>
		<?= Html::a('Отмена', ['donors'], ['class' => 'btn btn-default']) ?>
	</div>

	<?php
	$form::end();
	?>
</div>
