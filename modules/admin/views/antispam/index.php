<?php
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Статистика модуля Antispam';
?>
	<div class="pull-right">
		<?= Html::a('Перейти к настройкам модуля', '/admin/antispam-settings', ['class' => 'btn btn-success']) ?>
	</div>
	<h1><?= $this->title ?></h1>


<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		//'media_id',
		[
			'attribute'=>'media_id',
			'label' => 'Media_ID',
			'value' => function ($model) {
				if ($model->media_code != '') {
					$result = '<a href="https://www.instagram.com/p/' . $model->media_code . '/" target="_blank">' . $model->media_id . '</a>';
				} else {
					$result = $model->media_id;
				}
				return $result;
			},
			'format' => 'raw',
		],
		[
			'attribute'=>'account.login',
			'label' => 'Insta аккаунт жертвы',
			'value' => 'account.login'
		],
		[
			'attribute'=>'commentator_login',
			'label' => 'Insta аккаунт комментатора',
			'value' => 'commentator_login'
		],
		//'comment_id',
		[
			'value' => 'text',
			'label' => 'Текст удалённого комментария',
			'contentOptions' => ['style' => 'width:300px;'],
		],
		'time_deleted',
		'users.mail',
	],
]) ?>