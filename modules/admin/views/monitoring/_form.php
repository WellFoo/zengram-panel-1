<?php

use app\models\Monitoring;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

/* @var $this yii\web\View */
/* @var $model Monitoring */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="monitoring-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'type')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'category')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'value')->textInput() ?>

	<?= $form->field($model, 'period')->dropDownList(Monitoring::periodLabels()) ?>
	
	<?= $form->field($model, 'comparation')->dropDownList(Monitoring::comparationLabels()) ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'sms', [
		'template' => '{label}<div class="input-group"><div class="input-group-addon">+</div>{input}</div>{error}'
	])->widget(MaskedInput::className(), [
		'options' => ['placeholder' => '8 (802) 000-0000', 'class' => 'form-control'],
		'mask' =>[
			'9 (999) 999-9999',
			'9 (999) 999-9999, 9 (999) 999-9999',
			'9 (999) 999-9999, 9 (999) 999-9999, 9 (999) 999-9999',
		]
	]) ?>

	<label>Можно ввести несколько email или sms(1-3 номера), разделяя запятой</label>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
