<?php

use app\models\Monitoring;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $monitoring_id integer */

$this->title = 'Лог отслеживаний: ' . ' ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Отслеживания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="monitoring-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'time',
			'result',
			'success:boolean',
			'notify_sms:boolean',
			'notify_email:boolean',
		],
	]); ?>

</div>
