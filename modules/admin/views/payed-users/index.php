<?php

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\modules\admin\models\PayedUsersSearch $searchModel
 */

use app\models\Invoice;
use app\models\Users;
use app\modules\admin\models\Balance;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var Users $user */
$user = Yii::$app->user->identity;

$this->title = 'Клиенты';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-admin" xmlns="http://www.w3.org/1999/html">
	<h1><?= $this->title ?></h1>
	<?php if (Yii::$app->session->hasFlash('UserDeleted')): ?>
		<div class="alert alert-success">
			Пользователь успешно удалён
		</div>
	<?php endif; ?>

	<?php
	Pjax::begin();
	echo GridView::widget([
		'id'             => 'users-list',
		'dataProvider'   => $dataProvider,
		'filterModel'    => $searchModel,
		'filterSelector' => '[name="num_users"]',
		'pager'          => [
			'firstPageLabel' => 'Перв',
			'lastPageLabel'  => 'Посл'
		],
		'columns'        => [
			[
				'format' => 'raw',
				'value'  => function ($item) {
					/** @var Users $item */
					return Html::input('checkbox', 'delete-user[]', $item['id'], [
						'class' => 'deletable'
					]);
				},
				'visible' => $user->isAdmin
			], [
				'label'         => 'id',
				'attribute'     => 'id',
				'headerOptions' => ['style' => 'min-width:80px;'],
			], [
				'label'     => 'Почта',
				'attribute' => 'mail',
				'format'    => 'raw',
				'value'     => function ($item) {
					/** @var Users $item */
					return
						Html::a($item['mail'], 'javascript:void(0)', ['onclick' => 'showData(' . $item['id'] . ', \'log\')'])
						. Html::beginTag('br')
						. Html::tag('span', $item['regdate'], ['style' => 'font-size: 80%;']);
				}
			], [
				'label'          => 'Баланс',
				'attribute'      => 'balance',
				'format'         => 'raw',
				'contentOptions' => ['align' => 'center', 'style' => 'vertical-align:middle'],
				'value'          => function ($item) {
					/** @var Users $item */
					//balance in seconds
					return Html::a(Balance::formatBalance($item['balance']),
							'javascript:void(0)', ['id' => 'balance_' . $item['id'],
							                       'onclick' => 'showData(' . $item['id'] . ', \'balance-log\')']) . '<br>' .
					Html::a(
						'списания',
						Url::to(['/admin/users/balance-flow-log/', 'user_id' => $item->id]),
						[
							'style' => 'font-size: .8em',
							'target' => '_blank'
						]);;
				}
			], [
				'label'         => 'Добавить баланс',
				'headerOptions' => [
					'style' => 'min-width: 200px;'
				],
				'format'        => 'raw',
				'value'         => function ($item) {
					/** @var Users $item */
					$result = Html::beginTag('form', [
						'class'  => 'addNewBalance',
						'action' => \yii\helpers\Url::to(['/admin/users/balance', 'id' => $item['id']]),
						'data'   => ['id' => $item['id']]
					]);
					$result .= Html::input('text', 'balance', 1, [
						'class' => 'pull-left form-control',
						'style' => 'width: 60px; margin-right: 10px'
					]);
					$result .= Html::dropDownList('format', null, [
						'60'    => 'Мин',
						'3600'  => 'Час',
						'86400' => 'Дни'
					], [
						'class' => 'form-control pull-left',
						'style' => 'width: auto; margin-right: 10px'
					]);
					$result .= Html::button(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-plus']),
						[
							'type'  => 'submit',
							'class' => 'btn btn-default pull-left'
						]
					);
					return $result . Html::endTag('form');
				},
				'visible' => $user->isAdmin
			], [
				'label'          => 'Кол-во оплат',
				'headerOptions'  => ['style' => 'width: 10%;text-align: center'],
				'format'         => 'raw',
				'attribute'      => 'invoice_count',
				'contentOptions' => ['align' => 'center', 'style' => 'vertical-align:middle'],
				'value'          => function ($item) {
					/** @var Users $item */
					return Html::button($item['invoice_count'], [
						'class' => 'btn btn-link',
						'onclick' => 'showPayedData(' . $item['id'] . ', \'payment-log\')'
					]);
				}
			], [
				'label'          => 'Кол-во оплат за послед. месяц',
				'headerOptions'  => ['style' => 'width: 10%;text-align: center'],
				'attribute'      => 'month_invoice_count',
				'contentOptions' => ['align' => 'center', 'style' => 'vertical-align:middle'],
				'value'          => function ($item) {
					if (empty($item['month_invoice_count'])){
						return 0;
					}
					return $item['month_invoice_count'];
				}
			], [
				'label'          => 'Сумма оплат',
				'headerOptions'  => ['style' => 'width: 10%;text-align: center'],
				'format'         => 'raw',
				'attribute'      => 'invoice_sum',
				'contentOptions' => ['align' => 'center', 'style' => 'vertical-align:middle'],
				'value'          => function ($item) {
					return Html::button($item['invoice_sum'], [
						'class' => 'btn btn-link',
						'onclick' => 'showPayedData(' . $item['id'] . ', \'payment-log\')'
					]);
				}
			], [
				'label'          => 'Сумма оплат за послед. месяц',
				'headerOptions'  => ['style' => 'width: 10%;text-align: center'],
				'attribute'      => 'month_invoice_sum',
				'contentOptions' => ['align' => 'center', 'style' => 'vertical-align:middle'],
				'value'          => function ($item) {
					if (empty($item['month_invoice_sum'])){
						return 0;
					}
					return $item['month_invoice_sum'];
				}
			], [
				'label'          => 'Дата посл. платежа',
				'headerOptions'  => ['style' => 'width: 10%;text-align: center'],
				'attribute'      => 'last_payment',
				'contentOptions' => ['align' => 'center', 'style' => 'vertical-align:middle'],
			], [
				'label'          => 'Управление<br>аккаунтами',
				'encodeLabel'    => false,
				'attribute'      => 'accounts',
				'format'         => 'raw',
				'headerOptions'  => ['style' => 'width: 10%; text-align: center'],
				'contentOptions' => ['align' => 'center'],
				'value'          => function ($item) {
					/** @var Users $item */
					return Html::a($item['accounts_count'] ? $item['accounts_count'] : 0, ['/admin/projects', 'user_id' => $item['id']], [
						'class' => 'btn btn-default' . ($item['accounts_count'] == 0 ? ' disabled' : ''),
						'target' => '_blank'
					]);
				}
			], [
				'label'          => 'Инфо',
				'format'         => 'raw',
				'contentOptions' => ['align' => 'center', 'style' => 'vertical-align:middle'],
				'value'          => function ($item) {
					/** @var Users $item */
					return Html::button('', [
						'class' => 'btn btn-default fa fa-file',
						'onclick' => 'showData(' . $item['id'] . ', \'info\')'
					]);
				}
			], [
				'format' => 'raw',
				'value'  => function ($item) {
					/** @var Users $item */
					$result = Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
						['/admin/users/edit', 'id' => $item['id']]
					);
					$result .= '&nbsp;';
					$result .= Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
						['/admin/users/delete', 'id' => $item['id']],
						['onclick' => 'return confirm("Вы уверены, что хотите удалить выбранного пользователя?");']
					);
					return $result;
				},
				'visible' => $user->isAdmin
			]
		]
	]);
	?>
	<div class="row">
		<?php if ($user->isAdmin) { ?>
		<div class="col-xs-12 col-md-6 col-sm-6">
			<a href="#" class="btn btn-primary" id="delete-selected">Удалить выбранные</a>
		</div>
		<div class="col-xs-12 col-md-6 col-sm-6 text-right">
		<?php } else { ?>
		<div class="col-xs-12 text-right">
		<?php } ?>

			<label style="margin-right: 5px">Отображать по: </label>

			<div class="btn-group" data-toggle="buttons">
				<?php
				if (empty(Yii::$app->request->queryParams['num_users'])) {
					$selected_num = 50;
				} else {
					$selected_num = Yii::$app->request->queryParams['num_users'];
				}
				$users_nums = [25, 50, 100];
				foreach ($users_nums as $users_num):?>
					<label class="btn btn-primary<?= ($users_num == $selected_num) ? ' active' : '' ?>">
						<input type="radio" autocomplete="off" name="num_users"
						       value="<?= $users_num ?>"> <?= $users_num ?>
					</label>
				<?php endforeach;
				?>
			</div>
		</div>
	</div>

	<script>
		jQuery(function ($)
		{
			$('form.addNewBalance').submit(function (event)
			{
				event.preventDefault();
				event.stopPropagation();
				var form = $(this);
				var id = form.data('id');
				addBalance(form.data('id'), form.find('[name=format]').val(), form.find('[name=balance]').val());
				return false;
			});
		})
	</script>
	<?php Pjax::end(); ?>
</div>
<div id="alert-modal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Инфо аккаунта</h4>
			</div>
			<div class="modal-body">
			</div>
			<div class="modal-footer">
				<div class="pagination pull-left" style="margin: 0">

				</div>
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
	jQuery(function ($)
	{
		$('#delete-selected').on('click', function ()
		{
			var list = [];
			$('.deletable').each(function ()
			{
				if (this.checked) {
					list.push(this.value);
				}
			});
			if (!list.length || !confirm('Вы уверены, что хотите удалить выбранных пользователей?')) {
				return false;
			}
			$.post('<?= Url::to(['/admin/users/mass-delete']) ?>', {'delete-user': list}, function ()
			{
				window.location.reload();
			});
			return false;
		});

		$('.switchable').on('change', function ()
		{
			var el = this;
			el.indeterminate = true;
			el.disabled = true;
			/** @namespace el.dataset.userId */
			$.ajax({
				url: '<?= Url::to(['/admin/users/switch']) ?>/' + el.dataset.userId,
				method: 'post',
				data: {
					attrName: el.name,
					value: el.checked ? 1 : 0
				},
				dataType: 'json',
				success: function (response)
				{
					if (response.status === 'ok') {
						el.checked = response.value ? true : false;
					}
				}
			}).always(function ()
			{
				el.indeterminate = false;
				el.disabled = false;
			});
		});
	});

	function showData(user_id, type, page)
	{
		$.ajax({
			url: '/admin/users/'+type+'/',
			method: 'get',
			data: {
				id: user_id,
				page: page
			},
			dataType: 'json',
			success: function (response)
			{
				var $modal = $('#alert-modal');
				var list = $modal.find('div.modal-body');
				$modal.find('.pagination').html(response.pagination)
				list.html('');
				if (response.status === 'ok') {
					$.each(response.data, function (index, item)
					{
						list.append(item);
					});
				}
				$modal.modal('show');
			}
		})
	}

	function showPayedData(user_id, type)
	{
		$.ajax({
			url: '/admin/payed-users/'+type+'/',
			method: 'get',
			data: {
				id: user_id
			},
			dataType: 'json',
			success: function (response)
			{
				var list = $('#alert-modal').find('div.modal-body');
				list.html('');
				if (response.status === 'ok') {
					$.each(response.data, function (index, item)
					{
						list.append(item);
					});
				}
				$('#alert-modal').modal('show');
			}
		})
	}

	function addBalance(user_id, format, balance)
	{
		$.ajax({
			url: '/admin/users/balance/'+user_id,
			method: 'post',
			data: {
				balance: balance,
				format: format
			},
			success: function (response)
			{
				$('#balance_' + user_id).text(response);
			}
		})
	}
	$(document).ready(function(){
		$('table').floatThead({
			'z-index': 1040
		});
	});
</script>
<style>
	.modal-body .row{
		border-bottom: 1px solid #000000;
		margin-bottom: 3px;
	}
	.floatThead-container{
		background: #ffffff;
	}
	.dropdown-menu{
		z-index: 1100 !important;
	}
</style>