<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var \app\models\Settings $model */
/** @var \app\models\Settings $value */

$this->title = 'Settings';
?>
<div class="site-admin">

	<?php

	$form = ActiveForm::begin([
		'options' => ['class' => 'form-horizontal', 'role' => 'form'],
		//'action' => '/admin/setting/'.$model->id,
	]);

	switch ($model->pause) {
		case 0:
			$pauseText = ' (для медленной скорости)';
			break;
		case 1:
			$pauseText = ' (для средней скорости)';
			break;
		case 2:
			$pauseText = ' (для быстрой скорости)';
			break;
		default:
			$pauseText = ' (вне зависимости от скорости)';
	}

	echo '<h1>'.(isset($value) ? $value->description : $model->description).$pauseText.'</h1>';
	echo $form->field($model, 'value');

	echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);

	ActiveForm::end();
	?>


</div>