<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Разделы F.A.Q.';
$this->params['breadcrumbs'][] = $this->title;

$tree = \app\models\FAQCats::getTree();
?>
<div class="faqcats-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Добавить раздел', '#', [
			'class'   => 'btn btn-success',
			'onclick' => '$("#addcatalog").modal("show"); return false;'
		]) ?>
	</p>

	<?= \app\widgets\Tree::widget([
		'id'    => 'catalog-tree',
		'items' => $tree,
		'valueCallback' => function ($item) {
			$html = $item['label'];//Html::a($item['label'], Url::to(['/admin/faq-cats', 'id' => $item['id']]));
			$html .= Html::a(
				Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
				Url::to(['/admin/faq-cats/delete', 'id' => $item['id']]),
				[
					'class' => 'remove-catalog pull-right',
					'style' => 'margin-left: 10px;',
					'onClick' => 'return confirm("Вы уверены, что хотите удалить выбранный раздел?")'
				]
			);
			$html .= Html::a(
				Html::tag('span', '', ['class' => 'glyphicon glyphicon-plus']),
				'#',
				[
					'class' => 'add-catalog pull-right',
					'data' => ['id' => $item['id']]
				]
			);
			return $html;
		}
	]); ?>

</div>

<div class="modal" id="addcatalog" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Добавить новый раздел</h4>
			</div>
			<div class="modal-body">
				<?php
				$model = new \app\models\FAQCats();

				$form = ActiveForm::begin([
					'options' => ['class' => 'form-horizontal'],
					'action' => Url::toRoute('/admin/faq-cats/create'),
					'fieldConfig' => [
						'template' => '{label}<div class="col-sm-8">{input}</div><div class="col-sm-12 text-center">{error}</div>',
						'labelOptions' => ['class' => 'col-sm-4 control-label'],
					],
				]);

				echo $form->field($model, 'parent_id')->dropDownList(
					$model::renderTree($tree),
					[
						'id'     => 'parent-catalog',
						'prompt' => 'Корневой раздел',
						'encode' => false
					]
				);

				echo $form->field($model, 'name');

				echo Html::submitButton(Yii::t('app','Добавить'), [
					'class' => 'btn btn-success center-block'
				]);

				ActiveForm::end();
				?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#catalog-tree .add-catalog').on('click', function(){
			var id = $(this).data('id');
			$('#parent-catalog option').each(function(){
				this.selected = (this.value == id);
			});
			$('#addcatalog').modal();
		});
		$('#catalog-tree .remove-catalog').on('click', function(){

		});
	});
</script>