<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HashtagsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/** @var $place_id String */

$this->title = $place;
//$this->params['breadcrumbs'][] = $this->title;
if(!isset($place_id)){
	$place_id = '';
}
?>
<div class="hashtags-index">

	<a href="/admin/hash-tags">
		<button class="btn btn-success">Вернуться к списку регионов</button>
	</a>

	<?php
	if (isset($placeName) && isset($place_id)){
		echo '<a href="/admin/hash-tags/in-region/'.$place_id.'"><button class="btn btn-primary">Вернуться к списку хэштегов по региону '.$placeName.'</button></a>';
	}
	?>

	<h1><?= Html::encode($this->title) ?></h1>


	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel' => $searchModel,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'attribute' => 'name',
				'format' => 'text',
				'label' => 'Хэштег',
			],
			[
				'attribute' => 'counter',
				'format' => 'text',
				'label' => 'Кол-во использований',
			],
			[
				'label' => 'Просмотреть связи',
				'format' => 'raw',
				'value' => function($item) use ($place_id){
					return '<a href="/admin/hash-tags/hashtag-communications/'. $item->id  .'?place='.$place_id.'"><span class="glyphicon glyphicon-th-list"></span>';
				}
			]
		],
	]); ?>
</div>
