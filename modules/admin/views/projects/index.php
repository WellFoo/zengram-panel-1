<?php

use app\models\Whitelist;
use yii\data\Pagination;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Account;
use yii\helpers\Url;
use yii\widgets\LinkPager;
use yii\widgets\Pjax;

/* @var $this yii\web\View
 * @var $projects array
 * @var $pages Pagination
 */

/** @var \app\models\Users $user */
$user = Yii::$app->user->identity;

$this->title = 'Projects';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-admin">
	<?php if (Yii::$app->session->hasFlash('LogClear')): ?>
		<div class="alert alert-success">
			Лог очищен
		</div>
	<?php endif; ?>
	<?php if (!Yii::$app->request->getQueryParam('user_id', false) && $user->isAdmin): ?>
		<p>
			<a href="<?= Url::to(['/admin/projects/restart-stop']) ?>" class="btn btn-danger">Остановить для
				перезапуска</a>
			<a href="<?= Url::to(['/admin/projects/restart-start']) ?>" class="btn btn-success">
				Запустить для перезапуска
				<?php
				if (!empty($_SESSION['restartProjects']) and count($_SESSION['restartProjects'])) {
					echo " (" . count($_SESSION['restartProjects']) . ")";
				}
				?>
			</a>
		</p>
	<?php endif; ?>
	<?php Pjax::begin(); ?>
	<?php if (!empty(Yii::$app->session->get('Project action'))): ?>
		<div class="alert alert-success">
			<?= Yii::t('admin', Yii::$app->session->get('Project action')) ?>
		</div>
	<?php endif; ?>
	<table class="table table-striped table-bordered">
		<thead>
		<tr>
			<th>InstaID</th>
			<th>Логин</th>
			<th>Ошибки</th>
			<th>Юзер</th>
			<th class="text-center" style="width: 35%;">Статус</th>
			<?php if ($user->isAdmin) { ?>
			<th class="text-center" style="width: 30%;">Операции</th>
			<th></th>
			<?php } ?>
		</tr>
		<tr>
			<th>
				<form>
					<input type="text" class="form-control ajaxSend" name="instagram_id">
				</form>
			</th>
			<th>
				<form>
					<input type="text" class="form-control ajaxSend" name="login">
				</form>
			</th>
			<th></th>
			<th></th>
			<th></th>
			<?php if ($user->isAdmin) { ?>
			<th></th>
			<th></th>
			<?php } ?>
		</tr>
		</thead>
		<tbody>
		<?php
		$account_statuses = Account::getStatusesByInstagramIdsList(ArrayHelper::getColumn($projects, 'instagram_id'));
		if ($account_statuses === null) {
			$account_statuses = [];
		}

		$whitelist_color = [
			Whitelist::WHITELIST_EMPTY => 'default',
			Whitelist::WHITELIST_FILLED => 'primary',
			Whitelist::WHITELIST_DELAY => 'warning',
			Whitelist::WHITELIST_ERROR => 'danger',
		];

		/** @var Account $account */
		foreach ($projects as $account): ?>
			<tr>
				<td><?= Html::a($account->instagram_id, ['/admin/stats/accounts-actions', 'id' => $account->id]); ?></td>
				<td><?php echo Html::a($account->login, ['/options/index', 'id' => $account->id]); ?></td>
				<td style="position: relative;">
					<?php if ($account->comment_status): ?>
						<span class="fa-stack fa-lg" data-toggle="popover" data-title="Комментирование остановлено"
						      data-content="Комментирования отключено с <?= date('d/m/Y H:i:s', $account->comments_block_date) ?>
								на <?= $account->comments_block_expire ?> ч.">
							<i class="fa fa-commenting-o fa-stack-1x"></i>
							<i class="fa fa-ban fa-stack-2x text-danger fa-spin"></i>
						</span>
					<?php endif; ?>
					<?php if ($account->follow_status): ?>
						<span class="fa-stack fa-lg" data-toggle="popover" data-title="Фоллоу остановлен"
						      data-content="Фоллоу отключен с <?= date('d/m/Y H:i:s', $account->follow_block_date) ?>
								на <?= $account->follow_block_expire ?> ч.">
							<i class="fa fa-user fa-stack-1x"></i>
							<i class="fa fa-ban fa-stack-2x text-danger fa-spin"></i>
						</span>
					<?php endif; ?>
					<?php if ($account->message): ?>
						<span class="fa-stack fa-lg" data-toggle="popover" data-title="Проект остановлен"
						      data-content="<?= $account->getErrorMessage(); ?>">
							<i class="fa fa-instagram fa-stack-1x"></i>
							<i class="fa fa-ban fa-stack-2x text-danger fa-spin"></i>
					</span>
					<?php endif; ?>
				</td>
				<td>
					<a class="user-id" data-id="<?= $account->id ?>" href="<?= Url::to(['/admin/users?UsersSearch[id]=' . $account->user_id]) ?>">
						<?= $account->user_id ?>
					</a>
					<a href="#"
					   class="change-user"
					   data-id="<?= $account->id ?>"
					   data-login="<?= $account->login ?>"
					   data-user="<?= $account->user->mail ?>"
					>
						сменить
					</a>
				</td>
				<td>
					<div class="row">
						<div class="col-md-12 col-xs-12 col-sm-12">
							<label>Проект</label> <?= ArrayHelper::getValue($account_statuses, intval($account->instagram_id), false) ?
								'<span class="text-success">запущен</span>' : 'остановлен'; ?>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<label>Прокси:</label> <?= Account::getProxyString($account->proxy_id) ?>
						</div>
						<div class="col-md-12 col-xs-12 col-sm-12">
							<label>Добавлен:</label> <?= $account->added ?>
						</div>
						<div class="col-md-3 col-xs-3 col-sm-3"><label>Лайки</label><br><?= $account->likes ?></div>
						<div class="col-md-3 col-xs-3 col-sm-3"><label>Комменты</label><br><?= $account->comments ?>
						</div>
						<div class="col-md-3 col-xs-3 col-sm-3"><label>Фоллоу</label><br><?= $account->follow ?></div>
						<div class="col-md-3 col-xs-3 col-sm-3"><label>Анфоллоу</label><br><?= $account->unfollow ?>
						</div>
					</div>
				</td>
				<?php if ($user->isAdmin) { ?>
				<td>
					<div style="min-width: 205px">
						<div class="btn-group-vertical">
							<a class="btn btn-success"
							   href="<?= Url::to(['/admin/projects/start/', 'id' => $account->id, 'user_id' => $user_id]) ?>">
								<i class="fa fa-play fa-lg"></i> Старт
							</a><a class="btn btn-danger"
							       href="<?= Url::to(['/admin/projects/stop/', 'id' => $account->id, 'user_id' => $user_id]) ?>">
								<i class="fa fa-stop fa-lg"></i> Стоп
							</a><a class="btn btn-warning"
							       href="<?= Url::to(['/admin/projects/restart/', 'id' => $account->id, 'user_id' => $user_id]) ?>">
								<i class="fa fa-refresh fa-lg"></i> Рестарт
							</a>
						</div>
						<div class="btn-group-vertical pull-right">
							<a target="_blank" class="btn btn-info"
							   href="<?= Url::to(['/admin/utility/account-log/'.$account->instagram_id]) ?>">
								<i class="fa fa-file-text fa-lg"></i> Лог
							</a><a class="btn btn-<?=$whitelist_color[$account->whitelist_added]?>" target="_blank"
							       href="<?= Url::to(['/admin/projects/whitelist/', 'instagram_id' => $account->instagram_id]) ?>">
								<i class="fa fa-list fa-lg"></i> Whitelist
							</a>
							<a class="remove-btn btn btn-default"
							   href="<?= Url::to(['/admin/projects/delete/', 'id' => $account->id, 'user_id' => $user_id]) ?>">
								<i class="fa fa-trash fa-lg"></i> Удалить
							</a>
						</div>
						<div style="text-align: center">
							<?php
								$server = ($account->server ? $account->server : Yii::$app->params['serverIP']);
								$host = $server;
								$server_index = false;

								if (in_array($server, ['95.169.186.128', '149.202.120.193'])) {
									$host = '95.169.190.64';

									$server_index = 1;
									if ($server = '95.169.186.128') {
										$server_index = 3;
									}
								}

								$path = 'http://' . $host . '/userLog.php?id=' . $account->instagram_id . (($server_index) ? '&server=' . $server_index : '');

							?>
							<a target="_blank" href="<?= $path ?>">Старый лог</a>
						</div>
					</div>
				</td>
				<td>
					<div class="btn-group-vertical">
						<?= Html::a('Очистить лог', ['/admin/projects/clear-log/', 'id' => $account->instagram_id], ['class' => 'btn btn-default']); ?>
						<?= Html::a('Настройки', ['/admin/projects/settings/', 'id' => $account->id], ['class' => 'btn btn-default']); ?>
					</div>
				</td>
				<?php } ?>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

	<!-- Модальное окно смены пользователя -->
	<div class="modal fade" id="user-change">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">

					<h3>
						Изменение владельца аккаунта <span class="account-name"></span>
					</h3>

					<h5>Текущий владелец: <span class="account-user"></span></h5>

					<input type="hidden" name="id" value="">

					<div class="row">
						<div class="col-md-10">
							<input type="text" name="email" value="" id="user-email" class="form-control"
							       placeholder="Введите e-mail или ID нового владельца">
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-primary pull-right user-search">Поиск</button>
						</div>
					</div>

					<br><br>

					<div class="body">

					</div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<style>
		.change-user {
			margin-top: 7px;
			display: block;
			text-decoration: underline;
			font-size: .85em;
		}


	</style>

	<script>

		var

			$changeUserModal = $('#user-change')

			;

		$(document).ready(function ()
		{
			$("a.remove-btn").on("click", function(e) {
				var link = this;

				e.preventDefault();

				$("<div>Are you sure you want to continue?</div>").dialog({
					buttons: {
						"Ok": function() {
							window.location = link.href;
						},
						"Cancel": function() {
							$(this).dialog("close");
						}
					}
				});
			});
			$(function ()
			{
				$("[data-toggle='tooltip']").tooltip();
			});

			$(function ()
			{
				$("[data-toggle='popover']").popover({'trigger': 'hover', delay: {"show": 0, "hide": 500}});
			});

			$('table').floatThead({
				'z-index': 1040
			});
			$('.ajaxSend').on('keydown', function (event)
			{
				sendOnEnter(event);
			}).on('blur', function ()
			{
				$(this).closest('form').submit();
			})
		});

		$(document).on('click', '.change-user', function(e){
			var $this = $(this),
				account = $this.attr('data-id'),
				login = $this.attr('data-login'),
				user = $this.attr('data-user');

			$changeUserModal.find('.account-name').html(login);
			$changeUserModal.find('.account-user').html(user);
			$changeUserModal.find('input[name=id]').val(account);

			$changeUserModal.modal('show');

			e.preventDefault();
			return false;
		});

		$(document).on('click', '.user-search', function(e){
			$.post(
				'projects/search-user',
				{
					request: $changeUserModal.find('input[name=email]').val()
				},
				function(response){

					if (response.status != 'ok') {
						return false;
					}

					$changeUserModal.find('.body').html(response.data);
				},
				'json'
			);

			e.preventDefault();
			return false;
		});

		$(document).on('click', '.set-user', function(e){

			var $this = $(this),
				user = $this.attr('data-id');

			$.post(
				'projects/change-user',
				{
					account_id: $changeUserModal.find('input[name=id]').val(),
					user_id: user
				},
				function(response){

					if (response.status != 'ok') {
						return false;
					}

					location.reload();
				},
				'json'
			);

			e.preventDefault();
			return false;
		});


		function sendOnEnter(event)
		{
			var enterPressed;
			if (event.type === 'keydown') {
				if (event.keyCode !== 13) {
					return; // only react to enter key
				} else {
					enterPressed = true;
				}
			} else {
				// prevent processing for both keydown and change events
				if (enterPressed) {
					enterPressed = false;
					return;
				}
			}

			if (enterPressed) {
				$(this).closest('form').submit();
			}

			return false;
		}
	</script>
	<?php
	echo LinkPager::widget([
		'pagination'     => $pages,
		'firstPageLabel' => 'Перв',
		'lastPageLabel'  => 'Посл'
	]);
	Pjax::end();
	?>
</div>
<style>
	.read-message {
		position: absolute;
		left: 20px;
		top: 9px;
	}

	.popover {
		width: 300px;
	}

	.floatThead-container {
		background: #ffffff;
	}

	.dropdown-menu{
		z-index: 1100 !important;
	}
</style>