<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'F.A.Q.';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
		<?= Html::a('Добавить статью', ['create'], ['class' => 'btn btn-success']) ?>
	</p>

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns' => [
			['class' => 'yii\grid\SerialColumn'],

			'cat.name',
			'question',
			'on_top',
			'rating',

			[
				'class' => 'yii\grid\ActionColumn',
				'headerOptions' => [
					'style' => 'width: 70px;'
				]
			],
		],
	]); ?>

</div>
