<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FAQ */

$this->title = 'F.A.Q.: ' . ' ' . $model->question;
$this->params['breadcrumbs'][] = ['label' => 'F.A.Q.', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->question, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="faq-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
