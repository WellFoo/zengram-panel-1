<?php

use app\models\FAQCats;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FAQ */
/* @var $form yii\widgets\ActiveForm */

$tree = FAQCats::getTree([$model->cat_id]);
?>

<div class="faq-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'cat_id')->dropDownList(
		FAQCats::renderTree($tree), [
			'encode' => false
		]
	); ?>

	<?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'answer_short')->textarea(['rows' => 3]) ?>

	<?= $form->field($model, 'answer')->widget(\dosamigos\tinymce\TinyMce::className(), [
		'options' => ['rows' => 12],
		'language' => 'ru',
		'clientOptions' => [
			'plugins' => [
				"advlist autolink lists link charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media image table contextmenu paste"
			],
			'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
		]
	]) ?>

	<?= $form->field($model, 'on_top')->checkbox() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
