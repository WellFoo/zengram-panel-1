<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $data_provider
 * @var string $type
 * @var integer $count_all
 * @var integer $count_30k
 * @var integer $count_100k
 * @var integer $count_500k
 * @var integer $count_1m
 */

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'Популярные аккаунты';
?>

<div>
	<h2><?= $this->title ?></h2>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th>30K+</th>
				<th>100K+</th>
				<th>500K+</th>
				<th>1M+</th>
				<th>Всего</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?= $count_30k ?></td>
				<td><?= $count_100k ?></td>
				<td><?= $count_500k ?></td>
				<td><?= $count_1m ?></td>
				<td><?= $count_all ?></td>
			</tr>
		</tbody>
	</table>

	<form method="get" style="margin-bottom: 20px;">
		<div class="btn-group" role="group" aria-label="">
			<button type="submit" name="type" value="30K"
			        class="btn btn-<?= $type === '30K' ? 'success' : 'default' ?>">30K+
			</button>
			<button type="submit" name="type" value="100K"
			        class="btn btn-<?= $type === '100K' ? 'success' : 'default' ?>">100K+
			</button>
			<button type="submit" name="type" value="500K"
			        class="btn btn-<?= $type === '500K' ? 'success' : 'default' ?>">500K+
			</button>
			<button type="submit" name="type" value="1M"
			        class="btn btn-<?= $type === '1M' ? 'success' : 'default' ?>">1M+
			</button>
		</div>
	</form>

	<?php
	Pjax::begin();

	echo GridView::widget([
		'dataProvider' => $data_provider,
		'columns' => [
			[
				'attribute' => 'username',
				'format' => 'raw',
				'value' => function($item) {
					/** @var \app\modules\admin\models\PopularAccounts $item */
					return Html::a('@' . $item->username, ['/admin/accounts-grid/popular-log', 'insta_id' => $item->instagram_id]);
				}
			],
			'avatar:image',
			'biography:text',
			'external_url:url',
			'follower_count',
			'media_count'
		]
	]);

	Pjax::end();
	?>
</div>
