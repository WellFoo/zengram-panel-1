<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $data_provider
 * @var string  $date
 * @var integer $dateFrom
 * @var integer $dateTo
 * @var integer $insta_id
 * @var integer $sum
 * @var integer $user
 * @var array   $coefficient
 */
use app\modules\admin\models\CommentingProject;
use app\modules\admin\models\PopularAccountsCommentsLog;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Статистика по популярным';

$projects = CommentingProject::find()
	->asArray()
	->all();

$projects = \yii\helpers\ArrayHelper::map($projects, 'id', 'name');

?>

<h2><?= $this->title ?></h2>
<div>
<?php
Pjax::begin();

ActiveForm::begin([
	'method' => 'get',
	'action' => [''],
	'options' => [
		'class' => 'form-horizontal',
		'style' => 'margin: 20px 0;'
	]
]); ?>
	<input type="hidden" name="insta_id" value="<?= $insta_id ?>">
	<div class="row">
		<div class="col-xs-3">
			<div class="btn-group" role="group" aria-label="...">
				<button type="submit" name="date" value="day"
				        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
				</button>
				<button type="submit" name="date" value="week"
				        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
				</button>
				<button type="submit" name="date" value="month"
				        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
				</button>
			</div>

		</div>

		<div class="col-xs-5">
			<label class="control-label col-xs-3">Диапазон</label>

			<div class="input-group col-xs-9 col-xs-offset-1">
				<label for="from" class="input-group-addon">с</label>
				<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="to" class="input-group-addon">по</label>
				<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
			</div>
		</div>
		<div class="col-xs-4">
			<label class="control-label col-xs-5">Пользователь</label>

			<div class="input-group col-xs-7">

				<span class="input-group-btn">
					<?=Html::dropDownList(
						'user',
						$user,
						[
							0 => 'Не выбрано',
							//-1 => 'fast-unfollow',
							-2 => 'kuzmin.sales@gmail.com'

						] + $projects, [
							'class' => 'form-control user-filter'
						])
					?>
				</span>

			</div>
		</div>
	</div>
<?php ActiveForm::end(); ?>

	<?= $this->renderFile(\Yii::getAlias('@app/modules/management/views/commenting/index.int.php'), [
		'date_from' => $dateFrom,
		'date_to'   => $dateTo,
		'user'      => $user
	]) ?>

<div style="margin: 15px;">
	<a href="<?= \yii\helpers\Url::to(['comment-detail-log']); ?>">Логи комментариев и ошибок</a>
	<b>Всего за период: </b><?= $sum ?>
</div>

	<div style="margin: 15px;">
		<b>Коеффициент комментаторов: </b>
	</div>

	<?= GridView::widget([
		'dataProvider' => $coefficient,
		'tableOptions' => [
			'class' => 'table table-bordered table-hover'
		],
		'columns' => [
			[
				'label' => 'Проект',
				'attribute' => 'project',
			],
			[
				'label' => 'Всего комментариев',
				'attribute' => 'count',
			],
			[
				'label' => 'Максимум на комментатора',
				'attribute' => 'max',
			],
			[
				'label' => 'Коэффициент',
				'attribute' => 'coeff',
			],
		]
	]);?>

	
<?php
echo GridView::widget([
	'dataProvider' => $data_provider,
	'tableOptions' => [
		'class' => 'table table-bordered table-hover'
	],
	'columns' => [
		[
			'label' => 'Сортировка по дате',
			'attribute' => 'id',
			'format' => 'raw',
			'value' => function ($item) use($date, $dateFrom, $dateTo) {
				/** @var \app\modules\admin\models\PopularLog $item */
				if (!is_null($item->account)) {
					return Html::a('@' . $item->account->username, [
						'',
						'date' => $date,
						'dateFrom' => $dateFrom,
						'dateTo' => $dateTo,
						'insta_id' => $item->instagram_id
					]);
				} else {
					return $item->instagram_id;
				}

			}
		],
		//'account.avatar:image',
		[
			'format' => 'raw',
			'value' => function ($item) use($date, $dateFrom, $dateTo) {
				/** @var \app\modules\admin\models\PopularLog $item */
				if (!is_null($item->account)) {

					$img = Html::img($item->account->avatar);

					$last_link = PopularAccountsCommentsLog::find()
						->where([
							'code' => PopularAccountsCommentsLog::CODE_COMMENT_SUCCESS,
							'instagram_id' => $item->instagram_id,
							'project_id' => $item->project_id
						])->orderBy('date DESC')
						->one();

					if (!is_null($last_link)) {

						$code = PopularAccountsCommentsLog::getDescriptionData($last_link, 'media_code');

						if ($code === '-') {
							return $img;
						}

						return Html::a(
							$img,
							'https://www.instagram.com/p/'.$code,
							[
								'target' => '_blank'
							]
						);

					}

					return $img;

				} else {
					return 'Сторонний аккаунт';
				}


			}
		],
		//'user.mail',
		[
			'value' => function ($item) {

				if (!is_null($item->user)) {
					return $item->user->mail;
				} else {

					/* @var \app\modules\admin\models\PopularLog $item */
					/* @var $project CommentingProject */
					$project = CommentingProject::findOne($item->project_id);

					if (is_null($project)) {
						return 'fast-unfollow.com';
					} else {
						return $project->name;
					}
				}
			}
		],
		[
			'attribute' => 'comments',
			'format' => 'raw',
			'value' => function ($item) use($date, $dateFrom, $dateTo) {
				/** @var \app\modules\admin\models\PopularLog $item */

				if (!is_null($item->user)) {

					return Html::a($item->comments, [
						'popular-detail-log',
						'date' => $date,
						'dateFrom' => $dateFrom,
						'dateTo' => $dateTo,
						'insta_id' => $item->instagram_id
					]);

				} else {

					return Html::a($item->comments, [
						'comment-detail-log',
						'date' => $date,
						'dateFrom' => $dateFrom,
						'dateTo' => $dateTo,
						'insta_id' => $item->instagram_id
					]);

				}

			}
		],
		'media_update'
	]
]);

Pjax::end();
?>
</div>

<script>
	$('.user-filter').change(function(){
		$(this).closest('form').submit();
		return false;
	});
</script>
