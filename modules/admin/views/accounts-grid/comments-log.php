<?php
/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $data_provider
 * @var \yii\data\ActiveDataProvider $searchModel,
 * @var string  $date
 * @var string  $text
 * @var integer $dateFrom
 * @var integer $dateTo
 * @var integer $insta_id
 * @var integer $type
 * @var array   $summary
 */
use app\modules\admin\models\CommentingProject;
use app\modules\admin\models\PopularAccountsCommentsLog;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


$projects = CommentingProject::find()
	->asArray()
	->all();

$projects = \yii\helpers\ArrayHelper::map($projects, 'id', 'name');
$this->title = 'Статистика по популярным'.($insta_id ? ' аккаунта '.$insta_id : '');
?>

<h2><?= $this->title ?></h2>
<div>
<?php
Pjax::begin();

ActiveForm::begin([
	'method' => 'get',
	'action' => [''],
	'options' => [
		'class' => 'form-horizontal',
		'style' => 'margin: 20px 0;'
	]
]); ?>
	<?php if($insta_id): ?>
	<input type="hidden" name="insta_id" value="<?= $insta_id ?>">
	<?php endif; ?>
	<div class="row">
		<div class="col-xs-3">
			<div class="btn-group" role="group" aria-label="...">
				<button type="submit" name="date" value="day"
				        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
				</button>
				<button type="submit" name="date" value="week"
				        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
				</button>
				<button type="submit" name="date" value="month"
				        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
				</button>
			</div>

		</div>

		<div class="col-xs-5">
			<label class="control-label col-xs-3">Диапазон</label>

			<div class="input-group col-xs-9 col-xs-offset-1">
				<label for="from" class="input-group-addon">с</label>
				<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="to" class="input-group-addon">по</label>
				<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
			</div>
		</div>
		<div class="col-xs-4">
			<label class="control-label col-xs-5">Пользователь</label>

			<div class="input-group col-xs-7">

				<span class="input-group-btn">
					<?=Html::dropDownList(
						'user',
						$user,
						[
							0 => 'Не выбрано',
							//-1 => 'fast-unfollow',
							-2 => 'kuzmin.sales@gmail.com'

						] + $projects, [
							'class' => 'form-control user-filter'
						])
					?>
				</span>

			</div>
		</div>
	</div>

	<br>

	<div class="row">

		<div class="col-xs-5">
			<label class="control-label col-xs-4">Отображение</label>

			<div class="input-group col-xs-8">

				<span class="input-group-btn">
					<?=Html::dropDownList(
						'type',
						$type,
						[
							0 => 'Стандартный',
							1 => 'Группировка по ошибкам'

						], [
						'class' => 'form-control user-filter'
					])
					?>
				</span>

			</div>
		</div>

		<div class="col-xs-5">
			<label class="control-label col-xs-4">Текст</label>

			<div class="input-group col-xs-8">

				<input name="text" value="<?= $text ?>" class="form-control">

			</div>
		</div>

	</div>



<?php ActiveForm::end(); ?>

<?php

switch ($type) {
	case 0:
	default:
	echo GridView::widget([
		'dataProvider' => $data_provider,
		'filterModel' => $searchModel,
		'tableOptions' => [
			'class' => 'table table-bordered table-hover'
		],
		'columns' => [
			'id',
			[
				'attribute' => 'date',
				'value' => function ($item) {
					/** @var PopularAccountsCommentsLog $item */
					return date('d-m-Y H:i:s', $item->date);
				}
			], [
				'attribute' => 'code',
				'value' => function ($item) {
					/** @var PopularAccountsCommentsLog $item */
					return PopularAccountsCommentsLog::getCodeDescription($item->code);
				},
				'filter' => PopularAccountsCommentsLog::codeDescriptions()
			], [
				'label' => 'Медиа',
				'format' => 'raw',
				'attribute' => 'media',
				'value' => function ($item) {

					$code = PopularAccountsCommentsLog::getDescriptionData($item, 'media_code');
					$media = PopularAccountsCommentsLog::getDescriptionData($item, 'media_id');

					if ($code === '-') {
						return $media;
					}

					return Html::a(
						$media,
						'https://www.instagram.com/p/'.$code,
						[
							'target' => '_blank'
						]
					);
				}
			], [
				'label' => 'Комментатор',
				'attribute' => 'user_id',
				'value' => function ($item) {
					return PopularAccountsCommentsLog::getDescriptionData($item, 'user_id');
				}
			], [
				'label' => 'Текст',
				'value' => function ($item) {
					return PopularAccountsCommentsLog::getDescriptionData($item, 'text');
				}
			], [
				'label' => 'Ошибка инстаграма',
				'value' => function ($item) {
					return PopularAccountsCommentsLog::getDescriptionData($item, 'error');
				}
			],
			'instagram_id',
			'project_id',


		]
	]);

	break;
	case 1:

		$columns = [
			[
				'label' => 'Текст комментария',
				'attribute' => 'text',
				'footer' => 'Всего'
			],
			[
				'label' => 'Использований',
				'attribute' => 'count',
				'footer' => $summary['count']
			],
			[
				'label' => 'Успешно',
				'attribute' => 'success',
				'footer' => $summary['success']
			],
		];

		foreach ($searchModel as $value => $label) {
			$columns[] = [
				'label' => $label,
				'attribute' => $value,
				'footer' => $summary[$value]
			];
		}

		echo GridView::widget([
			'dataProvider' => $data_provider,
			'showFooter' => true,
			'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => '-'],
			'tableOptions' => [
				'class' => 'table table-bordered table-hover'
			],
			'columns' => $columns
		]);

		break;
}

Pjax::end();
?>

<script>
	$('.user-filter').change(function(){
		$(this).closest('form').submit();
		return false;
	});
</script>
</div>
