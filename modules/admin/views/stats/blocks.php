<?php
/**
 * @var \yii\web\View $this
 * @var string  $date
 * @var string  $date_form
 * @var string  $date_to
 * @var integer $comments
 * @var integer $follows
 * @var integer $follows_timeout
 * @var integer $spam
 * @var integer $unfollow
 */

use app\modules\admin\models\AccountBlocks;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Статистика по блокировкам';
?>

<h2><?= $this->title ?></h2>

<?php
ActiveForm::begin([
	'method' => 'get',
	'options' => [
		'class' => 'form-horizontal',
		'style' => 'margin: 20px 0;'
	]
]); ?>
<div class="row">
	<div class="col-xs-3">
		<div class="btn-group" role="group" aria-label="...">
			<button type="submit" name="date" value="day"
			        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
			</button>
			<button type="submit" name="date" value="week"
			        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
			</button>
			<button type="submit" name="date" value="month"
			        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
			</button>
		</div>

	</div>

	<div class="col-xs-9">
		<label class="control-label col-xs-2">Диапазон</label>

		<div class="input-group col-xs-6 col-xs-offset-1">
			<label for="date-from" class="input-group-addon">с</label>
			<input name="from" value="<?= $date_form ? date('Y-m-d', $date_form) : '' ?>" id="date-from"
			       class="form-control date-picker" data-date-format="yyyy-mm-dd">
			<label for="date-to" class="input-group-addon">по</label>
			<input name="to" value="<?= $date_to ? date('Y-m-d', $date_to) : '' ?>" id="date-to"
			       class="form-control date-picker" data-date-format="yyyy-mm-dd">

			<span class="input-group-btn">
				<button type="submit" name="date" value="range"
				        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
				</button>
			</span>
		</div>
	</div>
</div>
<?php
ActiveForm::end();

$labels = AccountBlocks::actionsLabels();
?>

<div class="text-center">
	<table class="table table-bordered col-xs-3" style="width: auto;">
		<thead>
			<tr>
				<th width="33%" style="text-align: center;"><?= $labels[AccountBlocks::ACTION_COMMENTS] ?></th>
				<th width="33%" style="text-align: center;"><?= $labels[AccountBlocks::ACTION_UNFOLLOWS] ?></th>
				<th width="33%" style="text-align: center;"><?= $labels[AccountBlocks::ACTION_FOLLOWS] ?></th>
				<th width="33%" style="text-align: center;"><?= $labels[AccountBlocks::ACTION_FOLLOWS_TIMEOUT] ?></th>
				<th width="33%" style="text-align: center;"><?= $labels[AccountBlocks::ACTION_SPAM] ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?= Html::a($comments, ['/admin/stats/blocks-details', 'action' => AccountBlocks::ACTION_COMMENTS]) ?></td>
				<td><?= Html::a($unfollow, ['/admin/stats/blocks-details', 'action' => AccountBlocks::ACTION_UNFOLLOWS]) ?></td>
				<td><?= Html::a($follows, ['/admin/stats/blocks-details', 'action' => AccountBlocks::ACTION_FOLLOWS]) ?></td>
				<td><?= Html::a($follows_timeout, ['/admin/stats/blocks-details', 'action' => AccountBlocks::ACTION_FOLLOWS_TIMEOUT]) ?></td>
				<td><?= Html::a($spam, ['/admin/stats/blocks-details', 'action' => AccountBlocks::ACTION_SPAM]) ?></td>
			</tr>
		</tbody>
	</table>
</div>