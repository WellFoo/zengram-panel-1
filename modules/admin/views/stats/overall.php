<?php

use app\modules\admin\models\OverallStatistics;

/* @var $this yii\web\View
 * @var $stats array
 */

$this->title                   = 'Полная статистика';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-admin">

	<table class="table">
		<thead>
		<tr>
			<th>Дата</th>
			<th>Пользователи</th>
			<th>Аккаунты</th>
			<th>Платежи</th>
		</tr>
		</thead>
		<tbody>
		<?php

		/** @var OverallStatistics[] $stats */
		foreach($stats as $over_stat): ?>
				<?php $stat = $over_stat->getStats() ?>
			<tr>
				<td><?= $stat->date; ?></td>
				<td><?= $stat->users; ?></td>
				<td><?= $stat->accounts; ?></td>
				<td><?= $stat->invoices; ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
