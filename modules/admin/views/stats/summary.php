<?php
/**
 * @var \yii\web\View $this
 * @var app\models\AccountStats[] $summary
 * @var string[] $overall
 * @var string[] $service
 * @var string[] $moscow
 * @var string $date
 * @var integer $dateFrom
 * @var integer $dateTo
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

if (!empty($summary[0])) {
	if ($summary[0]['count']) {
		//время в секундах, переводим в сутке. в итоге получаем среднее кол-во действий в сутки
		if ($summary[0]['likes_time']){
			$summary[0]['likes_avg'] = ($summary[0]['likes'] / $summary[0]['likes_time'])*86400;
		} else {
			$summary[0]['likes_avg'] = 0;
		}
		if ($summary[0]['follow_time']){
			$summary[0]['follows_avg'] = ($summary[0]['follows'] / $summary[0]['follow_time'])*86400;
		} else {
			$summary[0]['follows_avg'] = 0;
		}
		if ($summary[0]['unfollow_time']){
			$summary[0]['unfollows_avg'] = ($summary[0]['unfollows'] / $summary[0]['unfollow_time'])*86400;
		} else {
			$summary[0]['unfollows_avg'] = 0;
		}
		if ($summary[0]['comment_time']){
			$summary[0]['comments_avg'] = ($summary[0]['comments'] / $summary[0]['comment_time'])*86400;
		} else {
			$summary[0]['comments_avg'] = 0;
		}
	} else {
		$summary[0]['likes_avg'] = 0;
		$summary[0]['follows_avg'] = 0;
		$summary[0]['comments_avg'] = 0;
	}
}
if (!empty($summary[1])) {
	if ($summary[1]['count']) {
		if ($summary[1]['likes_time']){
			$summary[1]['likes_avg'] = ($summary[1]['likes'] / $summary[1]['likes_time'])*86400;
		} else {
			$summary[1]['likes_avg'] = 0;
		}
		if ($summary[1]['follow_time']){
			$summary[1]['follows_avg'] = ($summary[1]['follows'] / $summary[1]['follow_time'])*86400;
		} else {
			$summary[1]['follows_avg'] = 0;
		}
		if ($summary[1]['unfollow_time']){
			$summary[1]['unfollows_avg'] = ($summary[1]['unfollows'] / $summary[1]['unfollow_time'])*86400;
		} else {
			$summary[1]['unfollows_avg'] = 0;
		}
		if ($summary[1]['comment_time']){
			$summary[1]['comments_avg'] = ($summary[1]['comments'] / $summary[1]['comment_time'])*86400;
		} else {
			$summary[1]['comments_avg'] = 0;
		}
	} else {
		$summary[1]['likes_avg'] = 1;
		$summary[1]['follows_avg'] = 1;
		$summary[1]['comments_avg'] = 1;
	}
}
if (!is_null($service)) {
	if ($service['count']) {
		if ($service['likes_time']){
			$service['likes_avg'] = ($service['likes'] / $service['likes_time'])*86400;
		} else {
			$service['likes_avg'] = 0;
		}
		if ($service['follow_time']){
			$service['follows_avg'] = ($service['follows'] / $service['follow_time'])*86400;
		} else {
			$service['follows_avg'] = 0;
		}

		if ($service['unfollow_time']){
			$service['unfollows_avg'] = ($service['unfollows'] / $service['unfollow_time'])*86400;
		} else {
			$service['unfollows_avg'] = 0;
		}
		if ($service['comment_time']){
			$service['comments_avg'] = ($service['comments'] / $service['comment_time'])*86400;
		} else {
			$service['comments_avg'] = 0;
		}
	} else {
		$service['likes_avg'] = 0;
		$service['follows_avg'] = 0;
		$service['comments_avg'] = 0;
	}
}

if (!is_null($moscow)) {
	if ($moscow['count']) {
		if ($moscow['likes_time']){
			$moscow['likes_avg'] = ($moscow['likes'] / $moscow['likes_time'])*86400;
		} else {
			$moscow['likes_avg'] = 0;
		}
		if ($moscow['follow_time']){
			$moscow['follows_avg'] = ($moscow['follows'] / $moscow['follow_time'])*86400;
		} else {
			$moscow['follows_avg'] = 0;
		}
		if ($moscow['unfollow_time']){
			$moscow['unfollows_avg'] = ($moscow['unfollows'] / $moscow['unfollow_time'])*86400;
		} else {
			$moscow['unfollows_avg'] = 0;
		}
		if ($moscow['comment_time']){
			$moscow['comments_avg'] = ($moscow['comments'] / $moscow['comment_time'])*86400;
		} else {
			$moscow['comments_avg'] = 0;
		}
	} else {
		$moscow['likes_avg'] = 0;
		$moscow['follows_avg'] = 0;
		$moscow['comments_avg'] = 0;
	}
}

if ($overall['count']) {
	if ($overall['likes_time']){
		$overall['likes_avg'] = ($overall['likes'] / $overall['likes_time'])*86400;
	} else {
		$overall['likes_avg'] = 0;
	}
	if ($overall['follow_time']){
		$overall['follows_avg'] = ($overall['follows'] / $overall['follow_time'])*86400;
	} else {
		$overall['follows_avg'] = 0;
	}
	if ($overall['unfollow_time']){
		$overall['unfollows_avg'] = ($overall['unfollows'] / $overall['unfollow_time'])*86400;
	} else {
		$overall['unfollows_avg'] = 0;
	}
	if ($overall['comment_time']){
		$overall['comments_avg'] = ($overall['comments'] / $overall['comment_time'])*86400;
	} else {
		$overall['comments_avg'] = 0;
	}
} else {
	$overall['likes_avg'] = 0;
	$overall['follows_avg'] = 0;
	$overall['comments_avg'] = 0;
}

$this->title = 'Статистика действий';
?>
<div class="accounts-actions-index">

	<h1><?= $this->title ?></h1>

	<?php Pjax::begin(); ?>
	<form method="get" class="form-horizontal" style="margin: 20px 0;">
		<div class="row">
			<div class="col-xs-3">
				<div class="btn-group" role="group" aria-label="...">
					<button type="submit" name="date" value="day"
					        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
					</button>
					<button type="submit" name="date" value="week"
					        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
					</button>
					<button type="submit" name="date" value="month"
					        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
					</button>
				</div>

			</div>

			<div class="col-xs-9">
				<label class="control-label col-xs-2">Диапазон</label>

				<div class="input-group col-xs-6 col-xs-offset-1">
					<label for="date-from" class="input-group-addon">с</label>
					<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">
					<label for="date-to" class="input-group-addon">по</label>
					<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
					       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
				</div>
			</div>
		</div>
	</form>

	<table class="table table-bordered table-hover">
		<thead>
		<tr>
			<th colspan="16" class="text-center">Общая статистика за период</th>
		</tr>
		<tr align="center">
			<th></th>
			<th>Проектов</th>
			<th>Средняя скорость</th>
			<th class="text-center">Лайки</th>
			<th class="text-center">Комментарии</th>
			<th class="text-center">Подписки</th>
			<th class="text-center">Лайки (сред.)</th>
			<th class="text-center">Подписки (сред.)</th>
			<th class="text-center">Комменты (сред.)</th>
			<th class="text-center">Отписки (сред.)</th>
			<th class="text-center">Пришло</th>
			<th class="text-center">Ушло</th>
			<th class="text-center">Отписки</th>
			<th class="text-center">Фото</th>
			<th class="text-center">Комментарии (ручные)</th>
			<th class="text-center">Сбросы паролей</th>
		</tr>
		</thead>
		<tbody>
		<?php if (isset($summary[0])) : ?>
			<tr style="cursor: pointer" align="center" onclick="goToActions({
				'AccountStatsSearch[account_id]':'',
				'AccountStatsSearch[mail]':'',
				'AccountStatsSearch[login]':'',
				'AccountStatsSearch[trial]':'0'
			})">
				<th align="left">оплативших</th>
				<th><?= ArrayHelper::getValue($summary[0], 'count', 0) ?></th>
				<th><?= $summary[0]['speeds'] ?></th>
				<td><?= $summary[0]['likes'] ?></td>
				<td><?= $summary[0]['comments'] ?></td>
				<td><?= $summary[0]['follows'] ?></td>
				<td><?= Yii::$app->formatter->asDecimal($summary[0]['likes_avg'], 2) ?></td>
				<td><?= Yii::$app->formatter->asDecimal($summary[0]['follows_avg'], 2) ?></td>
				<td><?= Yii::$app->formatter->asDecimal($summary[0]['comments_avg'], 2) ?></td>
				<td><?= Yii::$app->formatter->asDecimal($summary[0]['unfollows_avg'], 2) ?></td>
				<td><?= $summary[0]['came'] ?></td>
				<td><?= $summary[0]['gone'] ?></td>
				<td><?= $summary[0]['unfollows'] ?></td>
				<td><?= $summary[0]['photos'] ?></td>
				<td><?= $summary[0]['manual_comments'] ?></td>
				<td><?= $summary[0]['password_resets'] ?></td>
			</tr>
		<?php endif;
		if (isset($summary[1])) : ?>
			<tr style="cursor: pointer" align="center" onclick="goToActions({
				'AccountStatsSearch[account_id]':'',
				'AccountStatsSearch[mail]':'',
				'AccountStatsSearch[login]':'',
				'AccountStatsSearch[trial]':'1'
			})">
				<th align="left">бесплатных</th>
				<th><?= ArrayHelper::getValue($summary[1], 'count', 0) ?></th>
				<th><?= $summary[1]['speeds'] ?></th>
				<td><?= $summary[1]['likes'] ?></td>
				<td><?= $summary[1]['comments'] ?></td>
				<td><?= $summary[1]['follows'] ?></td>
				<td><?= Yii::$app->formatter->asDecimal($summary[1]['likes_avg'], 2) ?></td>
				<td><?= Yii::$app->formatter->asDecimal($summary[1]['follows_avg'], 2) ?></td>
				<td><?= Yii::$app->formatter->asDecimal($summary[1]['comments_avg'], 2) ?></td>
				<td><?= Yii::$app->formatter->asDecimal($summary[1]['unfollows_avg'], 2) ?></td>
				<td><?= $summary[1]['came'] ?></td>
				<td><?= $summary[1]['gone'] ?></td>
				<td><?= $summary[1]['unfollows'] ?></td>
				<td><?= $summary[1]['photos'] ?></td>
				<td><?= $summary[1]['manual_comments'] ?></td>
				<td><?= $summary[1]['password_resets'] ?></td>
			</tr>
		<?php endif;
		if (!is_null($service) && $service['count']) : ?>
		<tr style="cursor: pointer" align="center" onclick="goToActions({
				'AccountStatsSearch[account_id]':'',
				'AccountStatsSearch[mail]':'',
				'AccountStatsSearch[login]':'',
				'AccountStatsSearch[is_service]':'1'
			})">
			<th align="left">сервисных</th>
			<th><?= ArrayHelper::getValue($service, 'count', 0) ?></th>
			<th><?= $service['speeds'] ?></th>
			<td><?= $service['likes'] ?></td>
			<td><?= $service['comments'] ?></td>
			<td><?= $service['follows'] ?></td>
			<td><?= Yii::$app->formatter->asDecimal($service['likes_avg'], 2) ?></td>
			<td><?= Yii::$app->formatter->asDecimal($service['follows_avg'], 2) ?></td>
			<td><?= Yii::$app->formatter->asDecimal($service['comments_avg'], 2) ?></td>
			<td><?= Yii::$app->formatter->asDecimal($service['unfollows_avg'], 2) ?></td>
			<td><?= $service['came'] ?></td>
			<td><?= $service['gone'] ?></td>
			<td><?= $service['unfollows'] ?></td>
			<td><?= $service['photos'] ?></td>
			<td><?= $service['manual_comments'] ?></td>
			<td><?= $service['password_resets'] ?></td>
		</tr>
		<?php endif;
		if (!is_null($moscow) && $moscow['count']) : ?>
		<tr style="cursor: pointer" align="center" title="Подробнее" onclick="goToActions()">
			<th align="left">Москва</th>
			<th><?= ArrayHelper::getValue($moscow, 'count') ?></th>
			<th><?= $moscow['speeds'] ?></th>
			<td><?= $moscow['likes'] ?></td>
			<td><?= $moscow['comments'] ?></td>
			<td><?= $moscow['follows'] ?></td>
			<td><?= Yii::$app->formatter->asDecimal($moscow['likes_avg'], 2) ?></td>
			<td><?= Yii::$app->formatter->asDecimal($moscow['follows_avg'], 2) ?></td>
			<td><?= Yii::$app->formatter->asDecimal($moscow['comments_avg'], 2) ?></td>
			<td><?= Yii::$app->formatter->asDecimal($moscow['unfollows_avg'], 2) ?></td>
			<td><?= $moscow['came'] ?></td>
			<td><?= $moscow['gone'] ?></td>
			<td><?= $moscow['unfollows'] ?></td>
			<td><?= $moscow['photos'] ?></td>
			<td><?= $moscow['manual_comments'] ?></td>
			<td><?= $moscow['password_resets'] ?></td>
		</tr>
		<?php endif; ?>
		<tr style="cursor: pointer" align="center" title="Подробнее" onclick="goToActions()">
			<th align="left">итого</th>
			<th><?= $overall['count'] ?></th>
			<th><?= $overall['speeds'] ?></th>
			<td><?= $overall['likes'] ?></td>
			<td><?= $overall['comments'] ?></td>
			<td><?= $overall['follows'] ?></td>
			<td><?= Yii::$app->formatter->asDecimal($overall['likes_avg'], 2) ?></td>
			<td><?= Yii::$app->formatter->asDecimal($overall['follows_avg'], 2) ?></td>
			<td><?= Yii::$app->formatter->asDecimal($overall['comments_avg'], 2) ?></td>
			<td><?= Yii::$app->formatter->asDecimal($overall['unfollows_avg'], 2) ?></td>
			<td><?= $overall['came'] ?></td>
			<td><?= $overall['gone'] ?></td>
			<td><?= $overall['unfollows'] ?></td>
			<td><?= $overall['photos'] ?></td>
			<td><?= $overall['manual_comments'] ?></td>
			<td><?= $overall['password_resets'] ?></td>
		</tr>
		</tbody>
	</table>
	<div class="hidden"><a href="" data-pjax="false" target="_blank" id="route"></a></div>
	<table class="table table-bordered table-striped">
		<tr>
			<td colspan="3">Статистика по прокси</td>
		</tr>
		<tr>
			<td colspan="3">Минимальное кол-во пользователей</td>
		</tr>
		<tr>
			<td>Прокси ID</td>
			<td>Прокси</td>
			<td>Мин кол-во</td>
		</tr>
		<?php /** @var array[] $minProxies */
		foreach ($minProxies as $minProxy):?>
			<tr>
				<td><?= $minProxy['id'] ?></td>
				<td><?= $minProxy['proxy'] ?></td>
				<td><?= $minProxy['min'] ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="3">Максимальное кол-во пользователей</td>
		</tr>
		<tr>
			<td>Прокси ID</td>
			<td>Прокси</td>
			<td>Макс кол-во</td>
		</tr>
		<?php /** @var array[] $maxProxies */
		foreach ($maxProxies as $maxProxy):?>
			<tr>
				<td><?= $maxProxy['id'] ?></td>
				<td><?= $maxProxy['proxy'] ?></td>
				<td><?= $maxProxy['max'] ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td colspan="3">Среднее кол-во пользователей</td>
		</tr>
		<tr>
			<td>Прокси ID</td>
			<td>Прокси</td>
			<td>Среднее кол-во</td>
		</tr>
		<?php /** @var array[] $avgProxies */
		foreach ($avgProxies as $avgProxy):?>
			<tr>
				<td><?= $avgProxy['id'] ?></td>
				<td><?= $avgProxy['proxy'] ?></td>
				<td><?= floatval(number_format($avgProxy['avg'], 2, '.', '')) ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
	<?php
	Pjax::end();
	?>

	<script>
		//чтобы открыть новую вкладку при клике на строку таблицы
		function goToActions(params){
			var url = '<?= Url::to(['/admin/stats/accounts-actions']) ?>';
			if (window.location.search){
				if (params) {
					url += window.location.search + '&' + jQuery.param(params)
				} else {
					url += window.location.search;
				}
			} else if(params) {
				url+= '?' + jQuery.param(params);
			}
			var link = jQuery('#route');
			link.attr('href', url);
			link[0].click();
		}
	</script>
</div>