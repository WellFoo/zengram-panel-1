<?php
/**
 * @var $this \yii\web\View
 * @var $data_all    array
 * @var $data_active array
 */
$this->title = 'Статистика использования функций';
?>
<h2><?= $this->title ?></h2>

<table class="table table-bordered table-striped">
	<tr>
		<td width="20%"></td>
		<th>Всего</th>
		<th>Активных</th>
	</tr>
	<tr>
		<th>Личных кабинетов</th>
		<td><?= $data_all['users'] ?></td>
		<td><?= $data_active['users'] ?></td>
	</tr>
	<tr>
		<th>Инстаграм аккаунтов</th>
		<td><?= $data_all['accounts'] ?></td>
		<td><?= $data_active['accounts'] ?></td>
	</tr>
	<tr>
		<th>Работа по городам</th>
		<td><?= $data_all['geo_place'] ?> (в том числе по геотегам <?= $data_all['geotags'] ?>)</td>
		<td><?= $data_active['geo_place'] ?> (в том числе по геотегам <?= $data_active['geotags'] ?>)</td>
	</tr>
	<tr>
		<th>Работа по регионам</th>
		<td><?= $data_all['geo_regions'] ?></td>
		<td><?= $data_active['geo_regions'] ?></td>
	</tr>
	<tr>
		<th>Работа по хештегам</th>
		<td><?= $data_all['hashtags'] ?></td>
		<td><?= $data_active['hashtags'] ?></td>
	</tr>
	<tr>
		<th>Работа по конкурентам</th>
		<td><?= $data_all['competitors'] ?></td>
		<td><?= $data_active['competitors'] ?></td>
	</tr>
</table>