<?php

use app\modules\admin\models\OverallStatistics;
use yii\widgets\Pjax;

/* @var $this yii\web\View
 * @var $stats array
 */

/** @var array[] $countedRegions */
$this->title = 'Полная статистика';
Pjax::begin();
//$this->params['breadcrumbs'][] = $this->title;
?>
	<div class="site-admin">
		<form method="get" class="form-horizontal" style="margin: 20px 0;">
			<div class="row">
				<div class="col-xs-3">
					<div class="btn-group" role="group" aria-label="...">
						<button type="submit" name="date" value="day"
						        class="btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
						</button>
						<button type="submit" name="date" value="week"
						        class="btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
						</button>
						<button type="submit" name="date" value="month"
						        class="btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
						</button>
					</div>

				</div>

				<div class="col-xs-9">
					<label class="control-label col-xs-2">Диапазон</label>

					<div class="input-group col-xs-6 col-xs-offset-1">
						<label for="date-from" class="input-group-addon">с</label>
						<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
						       class="form-control date-picker" data-date-format="yyyy-mm-dd">
						<label for="date-to" class="input-group-addon">по</label>
						<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
						       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
					</div>
				</div>
			</div>
			<div class="row" style="margin-bottom: 10px; margin-top: 10px">
				<label class="control-label col-xs-2">Сортировка:</label>
				<div class="col-xs-4">
					<select name="by" class="form-control">
						<option value="name" <?= (!empty($_GET['by']) && $_GET['by'] === 'name') ? 'selected' : '' ?>>Название</option>
						<option value="count" <?= (empty($_GET['by']) || $_GET['by'] === 'count') ? 'selected' : '' ?>>Количество</option>
					</select>
				</div>
				<div class="col-xs-4">
					<select name="sort" class="form-control">
						<option value="ASC" <?= (empty($_GET['by']) || $_GET['sort'] === 'ASC') ? 'selected' : '' ?>>По возрастанию
						</option>
						<option value="DESC" <?= (!empty($_GET['by']) && $_GET['sort'] === 'DESC') ? 'selected' : '' ?>>По убыванию
						</option>
					</select>
				</div>
				<div class="col-xs-2">
					<button type="submit" class="btn btn-default">Вперед</button>
				</div>
			</div>
		</form>
		<h3>Статистика по странам</h3>
		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th>Страна</th>
				<th>Кол-во запусков проектов</th>
			</tr>
			<?php /** @var array[] $countries */
			foreach ($countries as $country): ?>
				<tr>
					<td><?= $country['name'] ? $country['name'] : '- Не известна -' ?></td>
					<td><?= $country['count'] ?></td>
				</tr>
			<?php endforeach ?>
		</table>
		<h3>Статистика по регионам</h3>
		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th>Регион</th>
				<th>Кол-во запусков проектов</th>
			</tr>
			<?php /** @var array[] $regions */
			foreach ($regions as $region): ?>
				<tr>
					<td><?= $region['name'] ? $region['name'] : '- Не известен -' ?></td>
					<td><?= $region['count'] ?></td>
				</tr>
			<?php endforeach ?>
		</table>
		<h3>Статистика по городам</h3>
		<table class="table table-striped table-bordered table-hover">
			<tr>
				<th>Город</th>
				<th>Кол-во запусков проектов</th>
			</tr>
			<?php /** @var array[] $cities */
			foreach ($cities as $city): ?>
				<tr>
					<td><?= $city['name'] ?></td>
					<td><?= $city['count'] ?></td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
<?php Pjax::end(); ?>