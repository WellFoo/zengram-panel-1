<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\UsageStats[] $data
 */

$this->title = 'Статика работы клиента';
?>
<div class="accounts-stats-usage">
	<h1><?= $this->title ?></h1>

	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Дата</th>
				<th>Наработка (средняя)</th>
				<th>Наработка (общая)</th>
				<th>Зарегистрировалось</th>
				<th>Триальных</th>
				<th>Оплативших</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($data as $item) { ?>
			<tr>
				<th><?= $item->date ?></th>
				<td><?= $item->getUsageAvg() ?></td>
				<td><?= $item->getUsageSum() ?></td>
				<td><?= $item->regs ?></td>
				<td><?= $item->trial ?></td>
				<td><?= $item->payed ?></td>
			</tr>
		<?php } ?>
		</tbody>
	</table>
</div>
