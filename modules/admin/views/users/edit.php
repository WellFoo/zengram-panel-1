<?php

use app\models\Users;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/** @var array $cats */
$cat_list = [];
foreach ($cats as $item) {
	$cat_list[$item['id']] = $item['name'];
	if ($item['default']) {
		$cat_list[$item['id']] .= ' (по умолчанию)';
	}
}

$this->title                   = 'Клиент';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-admin">

	<?php

	$form = ActiveForm::begin([
		'layout' => 'horizontal',
		'fieldConfig' => [
			'horizontalCssClasses' => [
				'label' => 'col-sm-1',
				'offset' => 'col-sm-offset-1',
				'wrapper' => 'col-sm-11'
			]
		],
	]);

	echo $form->field($model, 'mail');

	echo $form->field($model, 'proxy_cat_id')->dropDownList(
		$cat_list,
		[
			'id' => 'new-proxy-cat',
			'prompt' => 'Ипользовать по умолчанию'
		]
	);

	echo $form->field($model, 'role')->dropDownList([
		Users::ROLE_USER      => 'Пользователь',
		Users::ROLE_MANAGER   => 'Менеджер',
		Users::ROLE_FINANCIER => 'Финансист',
		Users::ROLE_ADMIN     => 'Администратор',
	]);

	echo $form->field($model, 'is_service')->checkbox();
	echo $form->field($model, 'sameBlack')->checkbox();
	echo $form->field($model, 'sameMade')->checkbox();
	echo $form->field($model, 'exactGeo')->checkbox();
	echo $form->field($model, 'allowImages')->checkbox();
	echo $form->field($model, 'skip_popular_update')->checkbox();
	echo $form->field($model, 'comment_popular')->checkbox();
	echo $form->field($model, 'upload_photo')->checkbox();
	echo $form->field($model, 'antispam')->checkbox();
	echo $form->field($model, 'parallel_unfollows')->checkbox();
	echo $form->field($model, 'unfollow_mode_multiactions')->checkbox();
	echo $form->field($model, 'use_timer')->checkbox();
	echo $form->field($model, 'follow_for_private')->checkbox();

	echo $form->field($model, 'gender')->dropDownList([
		Users::UNDEFINED => 'Не определен',
		Users::MALE      => 'Мужчина',
		Users::FEMALE    => 'Женщина',
	]);

	echo $form->field($model, 'person')->dropDownList([
		Users::UNDEFINED  => 'Не определен',
		Users::INDIVIDUAL => 'Физик',
		Users::COMMERCIAL => 'Коммерческий',
		Users::EMPLOYED   => 'Самозанятый'
	]);



	echo Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);

	ActiveForm::end();
	?>


</div>