<?php

/**
 *
 */

use app\models\MailTriggers;
use app\models\MailUserTriggers;
use app\models\Users;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Рассылки';

$dateTo = strtotime(date('Y-m-d'));

$date = Yii::$app->request->get('date', 'day');
$trigger = (int) Yii::$app->request->get('trigger', 0);
$email = Yii::$app->request->get('email', '');
$id = (int) Yii::$app->request->get('id', '');

switch ($date) {
	case 'day':
	default:
		$dateFrom = $dateTo - 86400;
		break;

	case 'week':
		$dateFrom = $dateTo - 86400 * 7;
		break;

	case 'month':
		$dateFrom = $dateTo - 86400 * 30;
		break;

	case 'range':
		$dateFrom = strtotime(Yii::$app->request->get('from'));
		$dateTo = strtotime(Yii::$app->request->get('to'));
		break;
}

$mails = MailUserTriggers::find()
	->select([
		Users::tableName().'.mail',
		MailUserTriggers::tableName().'.action',
		MailUserTriggers::tableName().'.user_id',
		MailUserTriggers::tableName().'.date',
	])
	->andWhere(['>=', 'date', $dateFrom])
	->andWhere(['<', 'date', $dateTo])
	->leftJoin(Users::tableName(),
		Users::tableName().'.id = '.MailUserTriggers::tableName().'.user_id');

if (!empty($trigger)) {
	$mails->andWhere(['action' => $trigger]);
}

if (!empty($id)) {
	$mails->andWhere(['user_id' => $id]);
}

if (!empty($email)) {
	$mails->andWhere(['ilike', 'mail', $email]);
}

$data_provider = new ArrayDataProvider([
	'allModels' => $mails->asArray()->all(),
	'sort' => [
		'attributes' => ['mail', 'date', 'user_id', 'action'],
	]
]);

ActiveForm::begin([
	'method' => 'get',
	'options' => [
	'class' => 'form-horizontal',
	'style' => 'margin: 20px 0;'
	]
]);

?>

	<div class="row">
		<div class="btn-group" role="group" aria-label="...">
			<button value="day"
			        class="day-button btn btn-<?= $date === 'day' ? 'success' : 'default' ?>">Вчера
			</button>
			<button value="week"
			        class="day-button btn btn-<?= $date === 'week' ? 'success' : 'default' ?>">Неделя
			</button>
			<button value="month"
			        class="day-button btn btn-<?= $date === 'month' ? 'success' : 'default' ?>">Месяц
			</button>
		</div>
		<input name="date" type="hidden" value="<?= $date ?>">
		<div class="col-xs-5">
			<span class="input-group-btn">
					<?=Html::dropDownList(
						'trigger',
						$trigger,
						[0 => 'Не выбрано'] + MailTriggers::getActionDescriptions(), [
						'class' => 'form-control trigger-filter'
					])
					?>
				</span>
		</div>

		<div class="col-xs-4">
			<div class="input-group">
				<label for="from" class="input-group-addon">с</label>
				<input name="from" value="<?= $dateFrom ? date('Y-m-d', $dateFrom) : '' ?>" id="date-from"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">
				<label for="to" class="input-group-addon">по</label>
				<input name="to" value="<?= $dateTo ? date('Y-m-d', $dateTo) : '' ?>" id="date-to"
				       class="form-control date-picker" data-date-format="yyyy-mm-dd">

					<span class="input-group-btn">
						<button type="submit" name="date" value="range"
						        class="btn btn-<?= $date === 'range' ? 'success' : 'default' ?>">Вперед
						</button>
					</span>
			</div>
		</div>

	</div>

	<br><br>

<?php

echo GridView::widget([
	'dataProvider' => $data_provider,
	'showHeader' => true,
	'tableOptions' => [
		'class' => 'table table-bordered table-hover'
	],
	'columns' => [
		[
			'attribute' => 'date',
			'label' => 'Дата',
			'value' => function ($item) {
				return date('d-m-Y H:i:s', $item['date']);
			}
		],
		[
			'attribute' => 'user_id',
			'format' => 'raw',
			'header' => '<input type="text" class="trigger-filter form-control" name="id" value="'.$id.'">',
			'label' => 'ID пользователя'
		],
		[
			'attribute' => 'mail',
			'format' => 'raw',
			'header' => '<input type="text" class="trigger-filter form-control" name="email" value="'.$email.'">',
			'label' => 'E-mail'
		],
		[
			'attribute' => 'action',
			'label' => 'Триггер',
			'value' => function ($item) {
				return MailTriggers::getActionDescription($item['action']);
			}
		],
	]
]);

ActiveForm::end();

?>

<script>
	$('.trigger-filter').change(function(){
		$(this).closest('form').submit();
		return false;
	});

	$('.day-button').click(function(){
		$('input[name = date]').val($(this).attr('value'));
		$(this).closest('form').submit();
		return false;
	});

</script>
