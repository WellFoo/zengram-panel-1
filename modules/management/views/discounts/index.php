<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\Discounts[] $discounts
 */
use yii\helpers\Html;

$this->title = 'Скидки'
?>
<div class="discounts">

	<h2><?= $this->title ?></h2>

	<p><?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?></p>

	<?= \yii\grid\GridView::widget([
		'dataProvider' => $discounts,
		'columns' => [
			'id',
			'code',
			'date_start:date',
			'date_end:date',
			'size',
			[
				'format' => 'raw',
				'header' => 'оплат (попыток)',
				'value' => function ($item) {
					/** @var \app\models\Discounts $item */
					// TODO отрефакторить (вынести запрос этих данных в отдельную модель для поиска)
					$cntAll = \app\models\Invoice::find()
						->where(['discount_id' => $item->id])
						->count();
					$cnt = \app\models\Invoice::find()
						->where(['discount_id' => $item->id])
						->andWhere(['status' => 3])
						->count();
					return $cnt . ' (' . $cntAll . ')';
				}
			], [
				'format' => 'raw',
				'headerOptions' => [
					'width' => '75px',
				],
				'value' => function ($item) {
					/** @var \app\models\Discounts $item */
					return Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-eye-open']),
						['/page/price', 'discount' => $item->code],
						['title' => 'Просмотр', 'target' => '_blank']
					)
					.'&nbsp;'.
					Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-pencil']),
						['update', 'id' => $item->id],
						['title' => 'Изменить']
					)
					.'&nbsp;'.
					Html::a(
						Html::tag('span', '', ['class' => 'glyphicon glyphicon-trash']),
						['remove', 'id' => $item->id],
						[
							'title' => 'Удалить',
							'onclick' => 'return confirm("Вы уверены, что хотите удалить скидку?");'
						]
					);
				}
			]
		]
	]) ?>
</div>
