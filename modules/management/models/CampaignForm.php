<?php

namespace app\modules\management\models;

use yii\base\Model;
use Yii;

class CampaignForm extends Model
{
	public $list_id;
	public $subject;
	public $html_body;
	public $track_read;
	public $track_links;

	public function rules()
	{
		return [
			[['list_id', 'subject', 'html_body'], 'required'],
			['list_id', 'integer'],
			[['subject', 'html_body'], 'string'],
			[['track_read', 'track_links'], 'boolean'],
			[['track_read', 'track_links'], 'default', 'value' => 'false']
		];
	}

	public function attributeLabels()
	{
		return [
			'list_id'     => 'Список',
			'subject'     => 'Тема',
			'html_body'   => 'Письмо',
			'track_read'  => 'Отслеживать прочтение писем',
			'track_links' => 'Отслеживать переходы по ссылкам',
		];
	}

	public function start()
	{
		if (!$this->validate()) {
			return false;
		}

		$message = new UniSenderEmail([
			'list_id' => $this->list_id,
			'from_name' => Yii::$app->params['company'],
			'from_address' => Yii::$app->params['adminEmail'],
			'subject' => $this->subject,
			'html_body' => $this->html_body,
		]);

		if (!$message->save()) {
			return false;
		}

		$campaign = new UniSenderCampaign([
			'message_id' => $message->id
		]);

		if (!$campaign->create()) {
			return false;
		}

		$model = new Campaigns([
			'unisender_id' => $campaign->id,
			'message_id'   => $message->id,
			'name'         => $message->subject,
		]);
		$model->save();

		return true;
	}
}