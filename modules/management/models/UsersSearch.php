<?php

namespace app\modules\management\models;

use app\models\Account;
use app\models\AccountStats;
use app\models\Invoice;
use app\models\Users;
use app\models\UsersInfo;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

/**
 * @property mixed started
 */
class UsersSearch extends Users
{
	public $haveProjects;
	public $started;
	public $noBalance;
	public $active;
	public $tryPay;
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['is_payed', 'is_service', 'haveProjects', 'noBalance', 'started', 'active', 'tryPay'], 'string']
		];
	}

	public function search($params)
	{
		$query = parent::find()->select([
			new Expression('DISTINCT '.Users::tableName().'.id'),
			Users::tableName().'.balance',
			Users::tableName().'.is_service',
			Users::tableName().'.mail',
			Users::tableName().'.regdate',
			Users::tableName().'.reg_ip',
			Users::tableName().'.is_payed',
		]);
		$dataProvider = new ActiveDataProvider([
			'query' => $query
		]);
		$query->orderBy(self::tableName().'.id ASC');
		if (!($this->load($params) && $this->validate())) {
			return $dataProvider;
		}

		if (strlen($this->is_payed)) {
			$query->andWhere(['is_payed' => intval($this->is_payed)]);
		}

		if (strlen($this->is_service)) {
			$query->andWhere(['is_service' => intval($this->is_service)]);
		}

		if (strlen($this->haveProjects)) {
			if ($this->haveProjects){
				$query->andWhere(['IN','id', Account::find()->select('user_id')]);
			} else {
				$query->andWhere(['NOT IN','id', Account::find()->select('user_id')]);
			}
		}

		if (strlen($this->isAdmin)) {
			$query->andWhere(['isAdmin' => intval($this->isAdmin)]);
		}
		if (strlen($this->started)) {
			if ($this->started){
				//259200 hardcoded при регистрации
				$query->andWhere(['!=', 'balance', 259200]);
			} else {
				$query->andWhere(['balance' => 259200]);
			}
		}
		if (strlen($this->noBalance)) {
			if ($this->noBalance){
				$query->andWhere(['<=', 'balance', 0]);
			} else {
				$query->andWhere(['>', 'balance', 0]);
			}
		}
		if (strlen($this->active)) {
			if ($this->active){
				$query->andWhere(['IN','id', AccountStats::find()->where(['>','date', new Expression("(NOW() - '7 day'::interval)")])->select('user_id')]);
			} else {
				$query->andWhere(['NOT IN','id', AccountStats::find()->where(['>','date', new Expression("(NOW() - '7 day'::interval)")])->select('user_id')]);
			}
		}
		if (strlen($this->tryPay)) {
			$query->joinWith('invoices');
			$query->orderBy(Users::tableName().'.id');
			if ($this->tryPay) {
				$query->andWhere(['status' => [Invoice::STATUS_PENDING, Invoice::STATUS_ACCEPTED, Invoice::STATUS_SUCCESS, Invoice::STATUS_FAIL]]);
			} else {
				$query->andWhere('status IS NULL');
			}
		}
		return $dataProvider;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getLastAction(){
		return $this->hasOne(AccountStats::className(), ['user_id' => 'id'])->select([new Expression('DISTINCT user_id'), 'date'])->orderBy('date DESC');
	}

	public function getIsStarted(){
		return $this->balance != 259200;
	}
}