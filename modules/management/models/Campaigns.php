<?php

namespace app\modules\management\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "campaigns".
 *
 * @property integer $id
 * @property integer $unisender_id
 * @property integer $message_id
 * @property string  $name
 * @property string  $create_date
 */
class Campaigns extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'campaigns';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['unisender_id', 'message_id'], 'required'],
			[['unisender_id', 'message_id'], 'integer'],
			[['create_date', 'name'], 'safe'],
			[['unisender_id'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'unisender_id' => 'Unisender ID',
			'message_id'   => 'Message ID',
			'name'         => 'Название',
			'create_date'  => 'Дата создания',
		];
	}
}
