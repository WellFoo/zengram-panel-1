<?php

namespace app\modules\management\models;

use app\components\UniSenderApiTrait;
use yii\base\Model;

class UniSenderCampaign extends Model
{
	use UniSenderApiTrait;

	const STATUS_SCHEDULED      = 'scheduled';
	const STATUS_WAITS_CENSOR   = 'waits_censor';
	const STATUS_WAITS_SCHEDULE = 'waits_schedule';

	public $id;
	public $status;
	public $count;
	public $track_read;
	public $track_links;
	public $message_id;

	public function rules()
	{
		return [
			['message_id', 'required'],
			[['id', 'message_id'], 'integer'],
			[['track_read', 'track_links'], 'boolean'],
			[['track_read', 'track_links'], 'default', 'value' => 'false']
		];
	}

	public function create()
	{
		if (!$this->validate()) {
			return false;
		}

		$result = self::getApi()->createCampaign(
			$this->message_id,
			$this->track_read,
			$this->track_links
		);

		if (empty($result)){
			return false;
		}
		$this->id = $result['campaign_id'];
		$this->status = $result['status'];
		$this->count = $result['count'];

		return true;
	}
}