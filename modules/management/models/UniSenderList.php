<?php namespace app\modules\management\models;

use app\components\UniSenderApiTrait;
use yii\base\Model;

class UniSenderList extends Model
{
	use UniSenderApiTrait;

	private $_fields = ['email', 'email_list_ids'];
	private $_data = [];

	public $id;
	public $title;

	public static function create($title)
	{
		return new self([
			'id' => self::getApi()->createList($title),
			'title' => $title
		]);
	}

	public static function findAll($asArray = false)
	{
		if ($asArray) {
			return self::getApi()->getLists();
		}

		$list = [];
		foreach (self::getApi()->getLists() as $item) {
			$list[] = new self($item);
		}
		return $list;
	}

	public function addField($field)
	{
		if (!in_array($field, $this->_fields)) {
			$this->_fields[] = $field;
		}
	}
	
	public function createFields()
	{
		$system_vars = ['ACCESS_TOKEN', '"email_list_ids', 'email', 'title', 'email_list_ids'];

		foreach ($this->_fields as $field) {
			if (in_array($field, $system_vars)) {
				continue;
			}
			var_dump(self::getApi()->createField($field));
		}
	}

	public function setFields($data)
	{
		foreach ($data as $item) {
			$this->addField($item);
		}
	}

	public function addUser($user)
	{
		$toData = [];
		$user['email_list_ids'] = $this->id;
		foreach ($user as $key => $value) {
			$idx = array_search($key, $this->_fields);
			if ($idx !== false) {
				$toData[$idx] = $value;
			}
		}
		$this->_data[] = $toData;
		if (count($this->_data) >= 10) {
			$this->importUsers();
		}
	}

	public function importUsers()
	{
		if (!count($this->_data)) {
			return;
		}

		self::getApi()->importContacts($this->_fields, $this->_data);
		$this->_data = [];
	}
}