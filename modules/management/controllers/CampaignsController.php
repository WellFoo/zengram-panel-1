<?php

namespace app\modules\management\controllers;

use app\modules\management\models\CampaignForm;
use app\modules\management\models\Campaigns;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class CampaignsController extends Controller
{
	public function actionIndex()
	{
		$campaigns = new ActiveDataProvider([
			'query' => Campaigns::find(),
			'sort'  => [
				'defaultOrder' => ['create_date' => SORT_DESC]
			]
		]);

		return $this->render('index', [
			'campaigns' => $campaigns
		]);
	}

	public function actionCreate()
	{
		$model = new CampaignForm();

		if ($model->load(\Yii::$app->request->post()) && $model->start()) {
			return $this->redirect(['index']);
		}

		return $this->render('create', [
			'model' => $model
		]);
	}
}