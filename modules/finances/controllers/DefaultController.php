<?php

namespace app\modules\finances\controllers;

use app\models\PaymentHistoryItem;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class DefaultController extends Controller
{
	public $defaultAction = 'pay-pal';

	public function actionIndex()
	{
		return $this->render('index');
	}

	public function actionPayPal()
	{
		set_time_limit(0);

		$date_start = new \DateTime(\Yii::$app->request->get('from', date('Y-m-01', strtotime('-1 month'))));
		$date_end = new \DateTime(\Yii::$app->request->get('to', date('Y-m-01')));
		\Yii::info($date_start->format('Y-m-d'));
		\Yii::info($date_end->format('Y-m-d'));

		$error = null;

		$payments = PaymentHistoryItem::find()
			->where(['<',  'date', $date_end->getTimestamp()])
			->andWhere(['>=', 'date', $date_start->getTimestamp()]);

		$service = \Yii::$app->request->get('service', '');
		if (!empty($service)) {
			$payments->andWhere(['service' => PaymentHistoryItem::$services[$service]]);
		}

		$dataProvider = new ActiveDataProvider([
			'query' => $payments,
			'sort' => [
				'defaultOrder' => [
					'date' => SORT_DESC
				]
			]
		]);

		return $this->render('pay-pal', [
			'dataProvider' => $dataProvider,
			'dateFrom'     => $date_start->getTimestamp(),
			'dateTo'       => $date_end->getTimestamp(),
			'service'      => $service,
			'error'        => $error,
		]);
	}
}
