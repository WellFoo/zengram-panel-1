<?php
namespace app\modules\instashpion\components;

use Yii;
use yii\console\Exception;

trait Api
{
    private $_servers = [];

    private function getApiKey()
    {
        return Yii::$app->params['worker_api_key'];
    }

    private function setServers($servers)
    {
        $this->_servers = $servers;
    }

    private function getServers()
    {
        return $this->_servers;
    }

    private function getRandomServer()
    {
        if (empty($this->getServers()) && !empty(Yii::$app->params['workers'])) {
            $this->setServers(Yii::$app->params['workers']);
        }

        if (empty($this->getServers())) {
            throw new Exception('Empty api servers list');
        }

        $servers = $this->getServers();

        return $servers[array_rand($servers)];
    }

    public function request($method, $params = [])
    {
        $params['action'] = $method;
        $params['api_key'] = $this->getApiKey();

        $query = 'http://' . $this->getRandomServer() . '/api/index.php?' . http_build_query($params);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $query);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        $response = curl_exec($curl);
        $result = json_decode($response, true);

        curl_close($curl);

        if (!$result) {
            throw new Exception('Server return empty response (query: '.$query.') ' . print_r($response));
        }

        if ($result['status'] == 'error') {
            throw new Exception($result['message']);
        }

        return $result['data'];
    }
}