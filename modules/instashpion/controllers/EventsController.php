<?php

namespace app\modules\instashpion\controllers;

use app\modules\instashpion\components\Api;
use app\components\Controller;
use app\modules\instashpion\models\Analize;

class EventsController extends Controller
{
	use Api;

	public function actionStartUpdateList($list_id)
	{
		$analize = Analize::findOne(['list_id' => $list_id]);

		if ($analize) {
			$analize->analize_list_status = Analize::STATUS_LIST_WORK;
		}
	}

	public function actionCompletedUpdateList($list_id)
	{
		$analize = Analize::findOne(['list_id' => $list_id]);

		if ($analize) {
			$analize->analize_list_status = Analize::STATUS_LIST_COMPLETE;

			$result = $this->request('getLikesByList', [
				'list_id'       => $list_id,
				'completed_callback' => \Yii::$app->request->getHostInfo().'/instashpion/events/completed-process-likes'
			]);

			$analize->task_id = $result['task_id'];

			$analize->save();
		}
	}

	public function actionCompletedReupdateList($list_id)
	{
		$analize = Analize::findOne(['list_id' => $list_id]);

		if ($analize) {
			$analize->analize_list_status = Analize::STATUS_LIST_COMPLETE;

			$result = $this->request('getLikesByList', [
				'list_id'       => $list_id,
				'completed_callback' => \Yii::$app->request->getHostInfo().'/instashpion/events/completed-process-likes'
			]);

			$analize->task_id = $result['task_id'];

			$analize->save();
		}
	}

	public function actionCompletedProcessLikes($task_id)
	{
		$analize = Analize::findOne(['task_id' => $task_id]);

		if ($analize) {
			$analize->analize_likes_status = 2;

			$analize->save();
		}
	}
}
