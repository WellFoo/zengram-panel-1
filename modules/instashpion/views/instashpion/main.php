<?php
use app\models\ShpionBalance;
use yii\db\Expression;
?>

window.progressIntervals = {};
window.victim = null;

window.store = window.app = new App({
	balance: <?= $balance?>,
	from: {
		y: '<?=date('Y')?>',
		m: '<?=date('m')?>',
		d: '<?=date('d')?>',
	}
});

app.get('analize').on('change:id', function (model) {
    if (model.get('id') > 0) {
        // app.get('analize').startUpdater();
     }

});

var users = new UsersCollection();

window.users = users;

$(document).ready(function () {
    var store = JSON.parse(localStorage.getItem('store'));

<?php if (\Yii::$app->user->isGuest) { ?>
    if (store) {

        store.analize.result = new UsersCollection(store.analize.result);
        store.analize.new_follows = new UsersCollection();
        store.analize.complete = true;
		store.analize.updatePending = false;

        if (parseInt(store.analize.id) <= 0) {
            store.analize.process = true;
            store.analize = {};
        }

		var history = new HistoryCollection();

		_.each(store.history, function (item) {
			var analize = new AnalizeModel(item.analize);
			analize.set('result', new UsersCollection(item.analize.result));
			analize.set('new_follows', new UsersCollection(item.analize.new_follows));
			analize.set('updatePending', false);

			history.add(new HistoryItem({
				user: new UserModel(item.user),
				analize: analize,
				hidden: false
			}));
		});

        window.store = window.app = new App({
            user: new UserModel(store.user),
            analize: new AnalizeModel(store.analize),
            history: history,
            isBrowser: true,
			balance: <?=$balance?>,
			from: {
				y: '<?=date('Y',$from)?>',
				m: '<?=date('m',$from)?>',
				d: '<?=date('d',$from)?>',
				h: '<?=date('H',$from)?>',
				m: '<?=date('m',$from)?>'
			},
			timer: <?=(($timer) ? 'true' : 'false')?>
        });
	}
<?php } else { ?>
	var history = new HistoryCollection();
	<?php
		$analizes = \app\modules\instashpion\models\Analize::find()->where(['user_id' => \Yii::$app->user->id])->all();

		$balances = ShpionBalance::find()
			->select(['user_id', 'id', 'date', new Expression('array_to_json(victims) as victims'), 'is_guest'])
			->where('user_id = :user_id', [
				':user_id' => \Yii::$app->user->id,
				//':interval' => time() - (60 * 60 * 24 * 30)
			])
			->orderBy('date')
			->all();

		$history = [];
		$hiddens = [];
		$analize_history = [];

		foreach ($balances as $model) {
			Yii::info($model);

			$victims = json_decode($model->victims);

			if (!$victims) {
				$victims = [];
			}

			if ($model->is_guest == 1) {
				$analize_history = array_merge($analize_history, $victims);
			}

			if ($model->date < time() - \app\modules\instashpion\controllers\MainController::BONUS_TIME) {
				$hiddens = array_merge($hiddens, $victims);
			}

			$history = array_merge($history, $victims);
		}

		$existed = [];

		Yii::info($history);
		Yii::info($hiddens);

		foreach($history as $victim_id)
		{
			if (in_array($victim_id, $analize_history)) {
				$analize = \app\modules\instashpion\models\Analize::findOne(['id' => $victim_id]);
				$victim = \app\modules\instashpion\models\Victim::findOne(['id' => $analize->victim_id]);
			} else {
				$victim = \app\modules\instashpion\models\Victim::findOne(['id' => $victim_id]);
				$analize = $victim->lastAnalize();
			}

			if (!$analize) {
				continue;
			}
			?>
				history.add(new HistoryItem({
					user: new UserModel({
						avatar: '<?=$victim->avatar?>',
						username: '<?=$victim->username?>',
						instagram_id: '<?=$victim->instagram_id?>',
						name: '<?=$victim->name?>',
						media_count: '<?=$victim->media_count?>',
						follower_count: '<?=$victim->follower_count?>',
						following_count: '<?=$victim->following_count?>'
					}),
					hidden: <?=(in_array($victim_id, $hiddens) ? 'true' : 'false')?>,
					analize: new AnalizeModel({
						id: '<?=$analize->id?>'
					})
				}));
			<?php
		}
	?>
	window.h = history;

	var first = history.findWhere({hidden: false});

	if (first) {
		var user = first.get('user').clone();
		var analize = first.get('analize').clone();

		analize.set({process: true, updated: false});
	} else {
		var user = new UserModel();
		var analize = new AnalizeModel();
	}

	window.store = window.app = new App({
		user: user,
		analize: analize,
		history: history,
		isBrowser: true,
		balance: <?=$balance?>,
		from: {
			y: '<?=date('Y',$from)?>',
			m: '<?=date('m',$from)?>',
			d: '<?=date('d',$from)?>',
			h: '<?=date('H',$from)?>',
			m: '<?=date('m',$from)?>'
		},
		timer: <?=(($timer) ? 'true' : 'false')?>
	});


<?php } ?>
        /*if (store.handlerUser) {
            window.store.set({handlerUser: new HandlerUser(store.handlerUser)});
        }*/

    var currAnalize = app.get('analize');

	if (currAnalize) {
		currAnalize.startUpdater();
	}

    setTimeout(function () {
        ReactDOM.render(<AppComponent store = {window.store} />, document.getElementById('search-form'));
    }, 1000);
});