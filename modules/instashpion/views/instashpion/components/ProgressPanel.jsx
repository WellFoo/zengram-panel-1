var ProgressPanel = React.createClass({
    interval: null,

    mixins: [
        Backbone.React.Component.mixin
    ],

    getInitialState() {
        return {
            analize_id: null,

            follows:    0,
            likes:      0,
            relikes:    0,
            rated:      0,

            result:     [],

            showingMedia:   null,
            showModal:      false,
            showingType:    1,

            completed: false,
            press: false,
            sort: 'media',
            sortReverse: false,
            showMore: false,
            showGuestModal: false,

        }
    },

    componentDidMount() {

    },

    componentWillMount() {
        this.on(this, {
            models: {
                store: app,
                analize: app.get('analize')
            },
            collections: {
                users: app.get('analize').get('result')
            }
        });

        this.setState({analize_id: this.props.analize_id});
        var self = this;
    },

    shouldComponentUpdate(nextProps, nextState) {
        console.log(nextState.store.analize.get('id'), this.state.analize.id);
        if (nextState.store.analize.get('id') != this.state.analize.id) {
            this.off(this);

            console.log('Обновили прогресс');

            this.on(this, {
                models: {
                    store: app,
                    analize: app.get('analize')
                },
                collections: {
                    users: app.get('analize').get('result')
                }
            });
        }

        return true;
    },

    componentWillUnmount() {
        app.get('analize').stopUpdater();
        this.off(this);
    },


    showMedia(user_id, type = 1) {
        this.setState({
            showingMedia: user_id,
            showModal: true,
            showingType: type
        });
    },

    renderMedia(media, i) {
        return (
            <div key = {i} className="photo-content">
                <a href = {"https://instagram.com/p/" + media.code + '/'} target = '_blank'>
                    <img src = {media.image} alt = "image" />
                </a>
            </div>
        );
    },

    renderModal() {
        if (this.state.showModal == false || this.state.showingMedia === null) {
            return null;
        }

        var self = this;

        var user = app.get('analize').get('result').findWhere({
            id: this.state.showingMedia
        });

        var list = (this.state.showingType == 1) ? user.get('media') : user.get('relikes');

        if (this.state.showingType == 3) {
            list = user.get('newMedia');
        }

        if (this.state.showingType == 4) {
            list = user.get('newRelikes');
        }

        return (
            <div>
                <div className = "modal-overlay" onClick = {() => { self.setState({showModal: false}); }}></div>
                <div className="modal-dialog modal-media">
                    <div className="modal-content">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onClick = {() => { self.setState({showModal: false}); }}>×</span></button>
                        <div className="modal-body">
                            <h2 className="modal-title" style = {{textAlign: 'center', width: '100%'}}>Отлайканные медиа</h2>

                            <div className="body-modal">
                                {_.map(list, this.renderMedia)}
                            </div>
                            <br />
                            <center>
                                <a className = "btn btn-danger" onClick = {() => { self.setState({showModal: false}); }}>Закрыть</a>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    payReg(method, e) {
        e.preventDefault();
        e.stopPropagation();

        if (method == 'robokassa') {
            $('#pay-reg-cb').val('/payment/invoice?price=9');
            //this.refs.payreg.value = '/payment/invoice';
        }

        if (method == 'interkassa') {
            $('#pay-reg-cb').val('/payment/interkassa?price=9');
            //this.refs.payreg.value = '/payment/interkassa';
        }

        if (method == 'yandex') {
            $('#pay-reg-cb').val('/payment/yandex?price=9');
            //this.refs.payreg.value = '/payment/yandex';
        }

        if (method == 'yandex-cards') {
            $('#pay-reg-cb').val('/payment/yandex-cards?price=9');
            //this.refs.payreg.value = '/payment/yandex-cards';
        }

        if (method == 'paypal') {
            $('#pay-reg-cb').val('/payment/paypal?price=9');
            //this.refs.payreg.value = '/payment/paypal';
        }

        $('#payRegModal').modal('show');

        app.get('analize').set({showPayRegModal: false});

        //this.refs.payregform.submit();
    },

    renderPayRegModal() {
        var self = this;

        if (app.get('analize').get('showPayRegModal') !== true) {
            return null;
        }

        return (
            <div>
                <div className = "modal-overlay" onClick = {() => { app.get('analize').set({showPayRegModal: false}) }}></div>
                <div className="modal-dialog modal-reg">
                    <div className="modal-content" style = {{padding: '20px 10px 40px 10px'}}>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true" onClick = {() => { app.get('analize').set({showPayRegModal: false}) }}>×</span></button>
                        <div className="modal-body" style = {{textAlign: 'center'}}>
                            <h2 className="modal-title" style = {{lineHeight: '1.4',fontSize: '26px',color: '#ff6666'}}>
                                Пополнить баланс
                            </h2>

                            <p style = {{
                                fontSize: '19px',
                                margin: '20px 0px'
                            }}>
                                После оплаты Вы получите 3 анализа.
                            </p>

                            <p>
                                Выберите наиболее удобный способ оплаты для продолжения
                            </p> <br />
                            <form ref = 'payregform'>
                                <input type = "hidden" name = "price" value = "9" />

                                <div id="currencies" style = {{
                                    width: '450px',
                                    display: 'inline-block',
                                    margin: '0px auto',
                                    textAlign: 'left'
                                }}>
                                    <span className="">
                                        <button type="button" className="pay-button yandex-cards-submit" onClick = {self.payReg.bind(this, 'yandex-cards')}><img src="/img/currencies/BankCard.png" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.payReg.bind(this, 'interkassa')}><img src="/img/currencies/logo-sberbank-online.png" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.payReg.bind(this, 'interkassa')}><img src="/img/currencies/AlfaBank.gif" /></button>
                                        <button type="button" className="pay-button yandex-submit" onClick = {self.payReg.bind(this, 'yandex')}><img src="/img/currencies/YandexMerchant.gif" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.payReg.bind(this, 'interkassa')}><img src="/img/currencies/Qiwi.gif" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.payReg.bind(this, 'interkassa')}><img src="/img/currencies/Beeline.gif" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.payReg.bind(this, 'interkassa')}><img src="/img/currencies/MTS.gif" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.payReg.bind(this, 'interkassa')}><img src="/img/currencies/megafon_icon_100.png" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.payReg.bind(this, 'interkassa')}><img src="/img/currencies/tinkoff_icon_100.png" /></button>
                                        <button type="button" className="pay-button interkassa-submit" onClick = {self.payReg.bind(this, 'interkassa')}><img src="/img/currencies/bitcoin_icon_100.png" /></button>
                                        <button type="button" className="pay-button paypal-submit" onClick = {self.payReg.bind(this, 'paypal')}><img src="/img/currencies/Paypal.gif" /></button>
                                    </span>
                                </div>
                            </form>

                            <br />
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    renderGuestModal() {
        if ((this.state.showGuestModal === false && isGuest) || !isGuest) {
            return null;
        }

        var self = this;

        return (
            <div>
                <div className = "modal-overlay" onClick = {() => { self.setState({showGuestModal: false}); }}></div>
                <div className="modal-dialog modal-media">
                    <div className="modal-content">
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" onClick = {() => { self.setState({showGuestModal: false}); }}>×</span>
                        </button>
                        <div className="modal-body"style = {{ padding: '20px 99px 30px 99px'}}>
                            <div className="body-modal" style = {{
                                color: '#ff6666',
                                margin: '0 auto 25px auto',
                                fontSize: '18px',
                                lineHeight: '22px',
                                textAlign: 'center'
                            }}>
                                <h2 style = {{fontSize: '24px', marginBottom: '35px'}}>Уважаемый клиент!</h2>

                                <p style = {{marginBottom: '15px'}}>Для осуществления просмотра без ограничений Вам необходимо пополнить баланс.</p>
                                <p>Кроме того, Вы можете использовать функционал instashpion БЕСПЛАТНО если являетесь клиентом сервиса zengram.ru, совершали там оплату и работа по Вашему аккаунту идет более 12 часов!</p>

                            </div>
                            <br /><br />
                            <center className="addAccount">
                                <button className = "btn btn-success" onClick = {(() => {
                                    if (!isUser) {
                                        app.get('analize').set('showPayRegModal', true);
                                        self.setState({showGuestModal: false});
                                        app.set('tick', !app.get('thick'));
                                    } else {
                                        app.get('analize').set('showPayModal', true);
                                        self.setState({showGuestModal: false});
                                        app.set('tick', !app.get('thick'));
                                    }
                                })} style = {{width: '45%', float: (isUser ? 'none' : 'left')}} >Оплатить</button>

                                <button className = "btn btn-success" onClick = {(() => { $('#mainRegModal').modal('show'); self.setState({showGuestModal: false}); })} style = {{width: '45%', display: (isUser ? 'none' : 'inline-block')}}>Регистрация</button>
                                {/*<button className = "btn btn-danger" onClick = {() => { self.setState({showGuestModal: false}); }} style = {{width: '45%', float: 'right'}}>Закрыть</button>*/}
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        );
    },

    renderUser(user, iteration) {
        var self = this;

        return (
            <li className={"data__result-item" + (user.blured ? ' blured' : '')} key = {iteration} style = {{filter: 'blur(' + (user.blured ? 5 : 0) + "px)"}} onClick = {((!user.blured) ? (() => {}) : (() => { self.setState({showGuestModal: true}) }))}>
                <div className="data__result-item-block photo">
                    {(() => {
                        if (user.blured) {
                            return <img src={'/img/demo.png'} className="data__result-photo" style = {{width: '85px', filter:'blur(5px)', opacity: '.5'}} />;
                        } else {
                            return (
                                <a href = {'https://instagram.com/' + user.get('username')} className = "data__result-photo" style = {{top: '20px'}} target ={'_blank'}>
                                    <img src={user.get('photo')} className="data__result-photo" style = {{width: '85px'}} />

                                </a>
                            );
                        }
                    })()}

                </div>
                <div className="data__result-item-block username">
                    <p className="data__result-name">
                        {(() => {
                            if (user.blured) {
                                return <span>@{user.get('username')}</span>;
                            } else {
                                return <a href = {'https://instagram.com/' + user.get('username')} target ={'_blank'}>@{user.get('username')} {(user.get('insider')) ? '*' :''}</a>;
                            }
                        })()}

                    </p>
                </div>
                <div className="data__result-item-block scan-value">
                    {(() => {
                        if (user.get('is_private')) {
                            return (
                                <span>
                                    <i className="fa fa-question" style={{color: '#aaa', display: 'block'}}></i>
                                    <small style={{marginTop: '10px', color: '#aaa', display: 'block'}}>Приватный пользователь</small>
                                </span>
                            );
                        }

                        // Обратные лайки
                        if (_.size(user.get('media')) > 0) {
                            return (
                                <p style = {{ float: 'none', position: 'relative', display: 'inline-block', top: '0px', transform: 'none', left: '0px' }} className="data__result-for-target" onClick = {((isGuest && user.blured) ? (() => {}) : this.showMedia.bind(this, user.get('id'), 1))}>
                                    {_.size(user.get('media'))}
                                </p>
                            );
                        } else {
                            return <p style = {{float:'none',position: 'relative',display: 'inline-block',top: '0px',transform: 'none',left: '0px'}} className="data__result-for-target">0</p>;
                        }

                        // Новые лайки от жертвы
                        if (_.size(user.get('newMedia')) > 0) {
                            return <p style = {{ float: 'none', position: 'relative', display: 'inline-block', top: '0px', transform: 'none', left: '0px', fontWeight: 'bold', color: 'red'}} className="data__result-for-target" onClick = {((isGuest && user.blured) ? (() => {}) : self.showMedia.bind(self, user.get('id'), 3))}>
                                + {_.size(user.get('newMedia'))}
                            </p>;
                        }
                    })()}
                </div>
                <div className="data__result-item-block scan-value">
                    {(() => {
                        // Обратные лайки
                        if (_.size(user.get('relikes')) > 0) {
                            return (
                                <p style = {{float: 'none',position: 'relative',display: 'inline-block',top: '0px',transform: 'none',left: '0px'}} className="data__result-from-target" onClick = {((isGuest && user.blured) ? (() => {}) : self.showMedia.bind(self, user.get('id'), 2))}>
                                    {_.size(user.get('relikes'))}
                                </p>
                            );
                        } else {
                            return <p style = {{float:'none',position: 'relative',display: 'inline-block',top: '0px',transform: 'none',left: '0px'}} className="data__result-from-target">0</p>;
                        }
                    })()}

                    {(() => {
                        // Новые обратные лайки
                        if (_.size(user.get('newRelikes')) > 0) {
                            return <p style = {{float:'none',position: 'relative',display: 'inline-block',top: '0px',transform: 'none',left: '0px',fontWeight: 'bold',color: 'red'}} className="data__result-for-target" onClick = {((isGuest && user.blured) ? (() => {}) : self.showMedia.bind(self, user.get('id'), 4))}> + {_.size(user.get('newRelikes'))}</p>;
                        }
                    })()}
                </div>
            </li>
        );
    },

    getUsersResult() {
        var self = this;
        var sorted = (self.state.sortReverse) ? 1 : -1;

        var total = 20;

        if (this.state.showMore) {
            total = 50;
        }

        if (isGuest) {
            total = 10;
        }

        var models = app.get('analize').get('result').sortBy(function(user) {
            return (_.size(user.get(self.state.sort)) * sorted);
        }).slice(0, total);

        var groups = _.groupBy(models, function (user) {
            return _.size(user.get(self.state.sort))
        });

        if (_.size(groups) > 0) {
            if (sorted == -1) {
                groups = _.toArray(groups).reverse();
            }
        }

        var users = [];

        _.each(groups, function (group) {
            var res = _.sortBy(group, function(user){
                var sort = 'relikes';

                if (self.state.sort == 'relikes') {
                    sort = 'media';
                }

                return (_.size(_(user.get(sort)).toArray()) * -1);
            });

            var secondary_groups = _.groupBy(res, function (user) {
                var sort = 'relikes';

                if (self.state.sort == 'relikes') {
                    sort = 'media';
                }

                return _.size(user.get(sort));
            });

            if (sorted == -1) {
                secondary_groups = _.toArray(secondary_groups).reverse();
            }

            _.each(secondary_groups, function (sec) {
                var gC = new UsersCollection(sec);

                var privates = gC.where({is_private: true});
                var no_privates = gC.where({is_private: false});

                _.each(privates, function (i) {
                    console.log('private', i);
                    users.push(i);
                });

                _.each(no_privates, function (i) {
                    users.push(i);
                });
            });

        });

        users = _.map(users, (user, index) => {
            user.blured = (!(index & 1) && isGuest);

            return user;
        });

        return users;
    },

    stop() {
        app.get('analize').stop();
    },

    sorted(field) {
        console.log('Сортировка по полю' + field);

        if (this.state.sort == field) {
            console.log('Обратная: ' + ((field) ? 'Да': 'Нет'));
            this.setState({sortReverse: !this.state.sortReverse});
        } else {
            this.setState({sortReverse: false});
        }

        this.setState({sort: field});
    },

    renderTable() {
        if (this.state.result === null) {
            return null;
        }
        var self = this;
        var users = this.getUsersResult();

        var guestModalHandler = () => { self.setState({showGuestModal: true}); };

        return (
            <div className="data__result">
                {(() => {
                    if (_.size(self.getUsersResult()) > 0) {
                        return (
                            <div className="data__result-header-wrap">
                                <div className="data__result-header">
                                    <div className="data__result-header-item mobile-hide">Пользователь</div>
                                    <div className="data__result-header-item mobile-hide">Имя пользователя</div>
                                    <div className="data__result-header-item sort-field " onClick = {isGuest ? guestModalHandler : self.sorted.bind(self, 'media')}>
                                        Лайков от <span className = "mobile-hide">{window.store.get('user').get('username')}</span>
                                        <span style = {{width: '10px', display: 'inline-block'}}> </span>
                                        {(() => {
                                            return (
                                                <i className = {(
                                                    "glyphicon sort-icon glyphicon-sort-by-attributes" +
                                                    ((!self.state.sortReverse && self.state.sort == 'media') ? '-alt' : '') +
                                                    ((self.state.sort != 'media') ? ' dis-sort' : '')
                                                )}> </i>
                                            );
                                        })()}
                                        <div className = "comment-field mobile-hide">Анализ по 6 последним медиа</div>
                                    </div>
                                    <div className="data__result-header-item sort-field " onClick = {isGuest ? guestModalHandler : self.sorted.bind(self, 'relikes')}>
                                        Лайков <span className = "mobile-show">для</span> <span className = "mobile-hide">{window.store.get('user').get('username')}</span>
                                        <span style = {{width: '10px', display: 'inline-block'}}> </span>
                                        {(() => {
                                            return (
                                                <i className = {(
                                                    "glyphicon sort-icon glyphicon-sort-by-attributes" +
                                                    ((!self.state.sortReverse && self.state.sort == 'relikes') ? '-alt' : '') +
                                                    ((self.state.sort != 'relikes') ? ' dis-sort' : '')
                                                )}> </i>
                                            );
                                        })()}
                                        <div className = "comment-field mobile-hide">Анализ по 20 последним медиа</div>
                                    </div>
                                </div>
                            </div>
                        );
                    } else {
                        if (!self.state.analize.updated && self.state.analize.process && app.get('isBrowser')) {
                            return null;
                        }

                        return <center><Spin label = 'Формирование таблицы результата' color = '#ccc' size = "32px" loading = {true} /></center>;
                    }
                })()}

                <div className="data__result-list-wrap">
                    <ul className="data__result-list">
                        {_.map(users, this.renderUser)}
                    </ul>
                </div>
                {(()=> {
                    if (!self.state.showMore && _.size(users) > 0 && app.get('analize').get('result').size() > 20 && !isGuest) {
                        return (
                            <center>
                                <a className = "btn btn-default btn-lg"  onClick = {(()=>{self.setState({'showMore': true})})}>Показать больше</a>
                            </center>
                        );
                    }
                })()}
            </div>
        );
    },

    renderStopBtn() {
        if (!this.state.analize.complete && !(!this.state.analize.updated && app.get('isBrowser'))) {
            return (
                <div className="btn btn-lg btn-danger btn-analize" onClick={this.stop}>
                    <Spin label="Стоп" loading={false} size="20px" color="#fff"/>
                </div>
            );
        }
        return null;
    },

    render() {
        var self = this;
        var style = {width: 650 + 'px', margin: '0px auto'};

        var analize     = app.get('analize'),
            progress    = analize.get('progress'),
            user        = app.get('user');

        if (user === null || analize.get('id') === null) {
            return null;
        }

        if (this.state.completed == false) {
            style.display = 'block';
        } else {
            style.display = 'none';
        }

        var accounts_count = user.get('following_count'),
            workers_count = 3,
            handlers_count = 80;

        var rightFollows = (((accounts_count / 200) * 2) + 30);
        var rightLikes = (((((accounts_count) * 80) / handlers_count) / workers_count) + 60);

        var startFollows = false;
        var startLikes = false;

        if (this.state.analize.progress.follows >= 99) {
            startLikes = true;
        }

        if (!self.state.analize.pending) {
            startFollows = true;
        }

        var rightRelikes = rightLikes - (rightLikes / 100 * 5);

        return (
            <div>
                <span style = {{display: 'none'}}>{this.state.users.length}</span>
                {(() => {
                    if (!app.get('analize').get('updated') && app.get('analize').get('process') && app.get('isBrowser')) {
                        return (<center><Spin label = "Анализировать" loading = {true} size = "60px" color = "#999" /></center>);
                    }

                    if (app.get('analize').get('complete') === false) {
                        return (
                            <div className="progress-panel" style={style}>
                                <ProgressItem label='Подготовка данных' loop = {true} completed = {((self.state.analize.pending) ? false : true)} start = {true} />
                                <ProgressItem label='Обработка подписок' progress={self.state.analize.progress.follows} right={rightFollows} start={startFollows}/>
                                <ProgressItem label='Анализ отлайканных' progress={self.state.analize.progress.likes} right={rightLikes} start={startLikes}/>
                                <ProgressItem label='Анализ отлайкавших' progress={self.state.analize.progress.likes} right={rightRelikes} start={startLikes}/>
                            </div>
                        );
                    }
                })()}

                {this.renderStopBtn()}
                {this.renderTable()}
                {this.renderModal()}
                {this.renderGuestModal()}
                {this.renderPayRegModal()}
            </div>
        );
    }
});