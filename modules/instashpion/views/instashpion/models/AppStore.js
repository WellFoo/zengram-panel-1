var App = Backbone.Model.extend({
    defaults: {
        searchResult: new UsersCollection([]),
        user: null,
        analize: new AnalizeModel(),
        handlerUser: null,
        userInfoLoading: false,
        press: false,
        isBrowser: false,
        balance: 0,
        history: new HistoryCollection(),
        tick: false
    },

    startAnalize: function() {
        this.get('analize').start()
    }
});
