var UserModel = Backbone.Model.extend({
    idAttribute: "instagram_id",
    defaults: {
        loadedInfo: false,
        showPrivateModal: true,
        showMaxFollowsModal: true,
	    following_count: null,
        is_private: false,
        inside: false
    },

    isLoaded: function () {
        return this.get('loadedInfo');
    }
});