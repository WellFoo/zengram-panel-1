<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\models\Account;
use app\models\Actions;
use app\models\Users;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use app\modules\instashpion\assets\ModuleAsset;
use yii\helpers\Url;

ModuleAsset::register($this);

$pages = \app\models\Page::find()->all();
$canonical = $_SERVER['HTTP_HOST'] . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$user_id = Yii::$app->user->id;

if (Yii::$app->user->id === $user_id) {
	$user = Yii::$app->user->identity;
} else {
	$user = Users::findIdentity($user_id);
}

if (isset($user->isAdmin) and $user->isAdmin) {
	$this->registerJsFile('/js/jquery.sparkline.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
}
if (Yii::$app->user->isGuest) {
	$this->registerJsFile('/js/loginPage.js', ['position' => $this::POS_END]);
}

if (empty($this->title)) {
	$this->title = Yii::t('main', 'Buy Zengram App Free - Get More Real Instagram Followers, Likes and Comments Fast');
}

if (Yii::$app->user->isGuest) {
	$this->registerJsFile('/js/loginPage.js', ['position' => $this::POS_END]);
}

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php
$show_border = false;
?>
<!-- HEADER -->
<?=$this->render('//layouts/_header', compact('user', 'pages', 'show_border'))?>
<!-- HEADER End -->

<?= $content ?>

<?=$this->render('//layouts/_footer.php', compact('user', 'pages'))?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
