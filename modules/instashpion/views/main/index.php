<?php
	/* @var $this yii\web\View */

	use app\components\Counters;

	use yii\bootstrap\Html;
	use yii\widgets\ActiveForm;

	\webtoucher\cookie\AssetBundle::register($this);

	$this->title = 'Узнать, посмотреть, кто и кому ставит лайки в инстаграме!';
	$this->registerMetaTag([
		'name' => 'description',
		'content' => 'Наш сервис позволяет узнать, кому ставит интересуемый человек лайки в инстаграме'
	]);
?>

<div id = "search-form"></div>

<!-- Modal Registration -->
<div id="payRegModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

			<?php $form = ActiveForm::begin([
				'id' => 'pay-reg-form',
				'options' => [
					'class' => 'reg-form',
					'onSubmit' => Counters::renderAction('register')
				],
				'fieldConfig' => [
					'template' => "{input}{error}",
				],
				'enableAjaxValidation' => true,
				'validationUrl' => ['/site/ajaxregvalidate']
			]);
			$regForm = new \app\models\RegisterForm();
			?>

			<h2 class="reg-title"><?= Yii::t('app', 'Sign up') ?></h2>

			<p class="reg-text"><?= Yii::t('main', 'Is it your first visit?') ?></p>

			<p class="alert alert-danger" style="display: none"></p>

			<?= $form->field($regForm, 'mail')->input('text', ['placeholder' => Yii::t('main', 'Input the email*')]) ?>
			<input type="text" class="hidden">
			<input type="text" id = "pay-reg-cb" name = "cb" class="hidden" value = "" />
			<?= $form->field($regForm, 'password')->passwordInput(['placeholder' => Yii::t('main', 'Create the password')]) ?>

			<p class="reg-descr">
				<?= Yii::t('main', '*It is necessary to input the real email. All notifications will be send to it. You can renew the password of your account only with your email.') ?>
			</p>

			<?= Html::submitButton(Yii::t('app', 'Sign up')) ?>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<!-- Modal Registration end -->


<!-- Modal Registration -->
<div id="mainRegModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

			<?php $form = ActiveForm::begin([
				'id' => 'main-reg-form',
				'options' => [
					'class' => 'reg-form',
					'onSubmit' => Counters::renderAction('register')
				],
				'fieldConfig' => [
					'template' => "{input}{error}",
				],
				'enableAjaxValidation' => true,
				'validationUrl' => ['/site/ajaxregvalidate']
			]);
			$regForm = new \app\models\RegisterForm();
			?>

			<h2 class="reg-title"><?= Yii::t('app', 'Sign up') ?></h2>

			<p class="reg-text"><?= Yii::t('main', 'Is it your first visit?') ?></p>

			<p class="alert alert-danger" style="display: none"></p>

			<?= $form->field($regForm, 'mail')->input('text', ['placeholder' => Yii::t('main', 'Input the email*')]) ?>
			<input type="text" class="hidden">
			<input type="text" id = "pay-reg-cb" name = "cb" class="hidden" value = "/" />
			<?= $form->field($regForm, 'password')->passwordInput(['placeholder' => Yii::t('main', 'Create the password')]) ?>

			<p class="reg-descr">
				<?= Yii::t('main', '*It is necessary to input the real email. All notifications will be send to it. You can renew the password of your account only with your email.') ?>
			</p>

			<?= Html::submitButton(Yii::t('app', 'Sign up')) ?>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<!-- Modal Registration end -->

<!-- Modal Registration -->
<div id="regModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

			<?php $form = ActiveForm::begin([
				'id' => 'reg-form',
				'options' => [
					'class' => 'reg-form',
					'onSubmit' => Counters::renderAction('register')
				],
				'fieldConfig' => [
					'template' => "{input}{error}",
				],
				'enableAjaxValidation' => true,
				'validationUrl' => ['/site/ajaxregvalidate']
			]);
			$regForm = new \app\models\RegisterForm();
			?>

			<h2 class="reg-title"><?= Yii::t('app', 'Sign up') ?></h2>

			<p class="reg-text"><?= Yii::t('main', 'Is it your first visit?') ?></p>

			<p class="alert alert-danger" style="display: none"></p>

			<?= $form->field($regForm, 'mail')->input('text', ['placeholder' => Yii::t('main', 'Input the email*')]) ?>
			<input type="text" class="hidden">
			<?= $form->field($regForm, 'password')->passwordInput(['placeholder' => Yii::t('main', 'Create the password')]) ?>

			<p class="reg-descr">
				<?= Yii::t('main', '*It is necessary to input the real email. All notifications will be send to it. You can renew the password of your account only with your email.') ?>
			</p>

			<?= Html::submitButton(Yii::t('app', 'Sign up')) ?>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<!-- Modal Registration end -->
<!-- Modal Login -->
<div id="loginModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

			<?php $form = ActiveForm::begin([
				'id' => 'login-form',
				'options' => ['class' => 'login-form'],
				'action' => ['site/login'],
				'fieldConfig' => [
					'template' => "{input}{error}",
				],
				'enableAjaxValidation' => true,
				'validationUrl' => ['/site/ajaxvalidate']
			]);
			$loginModel = new \app\models\LoginForm();
			?>
			<h2 class="login-title"><?= Yii::t('app', 'Log in') ?></h2>

			<p class="login-text"><?= Yii::t('main', 'Input username and password <br>for an access for the control panel') ?></p>

			<p class="alert alert-danger" style="display: none"></p>

			<?= $form->field($loginModel, 'mail')->input('email', ['placeholder' => Yii::t('main', 'Input the username *')]) ?>
			<?= $form->field($loginModel, 'password')->passwordInput(['placeholder' => Yii::t('main', 'Input the password')]) ?>

			<p class="descr-text"><?= Yii::t('main', '*Use your email as a username') ?></p>
			<?= Html::submitButton(Yii::t('app', 'Log in'), ['class' => 'btn btn-success']) ?>
			<?php ActiveForm::end(); ?>
			<a onClick="showPassword()" class="reset-password pull-right" data-toggle="modal" data-target="#passwordModal" title="<?= Yii::t('main', 'forgot password') ?>">
				<i class="fa fa-lock"></i> <span class="hidden-xs"><?= Yii::t('main', 'forgot password') ?></span>
			</a>
		</div>
	</div>
</div>
<!-- Modal Login end -->


<script type="text/babel">
	var isGuest = false;
	var isPay = <?=(!$is_payed) ? 'false' : 'true'?>;
	var isUser = <?=(Yii::$app->user->isGuest) ? 'false' : 'true'?>;
	
	<?php if (Yii::$app->user->isGuest) {?>
		isGuest = true;
	<?php
		} else {
			if (!$is_payed) {
				?>
					isGuest = true;
				<?php
			}
		}
	?>


	<?=$this->render('/instashpion/components/DinamicNumbers.jsx')?>

	<?=$this->render('/instashpion/components/DinamicNumbers.jsx')?>

	<?=$this->render('/instashpion/models/UserModel.js')?>
	<?=$this->render('/instashpion/collections/UsersCollection.js')?>

	<?=$this->render('/instashpion/models/AnalizeModel.js')?>

	<?=$this->render('/instashpion/components/Spin.jsx')?>
	<?=$this->render('/instashpion/components/AccountInfo.jsx')?>
	<?=$this->render('/instashpion/components/ProgressItem.jsx')?>
	<?=$this->render('/instashpion/components/ProgressPanel.jsx')?>
	<?=$this->render('/instashpion/components/SearchForm.jsx')?>
	<?=$this->render('/instashpion/components/AppComponent.jsx')?>

	<?=$this->render('/instashpion/models/HandlerUser.js')?>

	<?=$this->render('/instashpion/models/AppStore.js')?>
	
	<?=$this->render('/instashpion/main.php', [
		'balance' => $balance,
		'from' => $from,
		'timer' => $timer,
		'is_payed' => $is_payed
	])?>
</script>