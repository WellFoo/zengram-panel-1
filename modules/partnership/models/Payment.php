<?php namespace app\modules\partnership\models;

use app\modules\partnership\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pfx_partnership_payments".
 *
 * @property integer $id
 * @property string  $date
 * @property integer $referal_id
 * @property integer $partner_id
 * @property double  $amount
 * @property float   $bonus
 * @property integer $referral_level
 * @property string  $description
 * @property float   $refunded
 *
 * @property Referal $referal
 */
class Payment extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%partnership_payments}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['refunded', 'default', 'value' => 0.0],
			[['referal_id', 'amount', 'bonus', 'referral_level', 'refunded'], 'required'],
			[['referal_id', 'referral_level'], 'integer'],
			[['description'], 'string'],
			[['amount', 'bonus', 'refunded'], 'number'],
			['date', 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'             => Module::t('module', 'ID'),
			'date'           => Module::t('module', 'Date'),
			'referal_id'     => Module::t('module', 'Referal ID'),
			'amount'         => Module::t('module', 'Amount'),
			'bonus'          => Module::t('module', 'Bonus'),
			'referral_level' => Module::t('module', 'Referral level'),
			'refunded'       => Module::t('module', 'Refunded'),
		];
	}

	public function getReferal()
	{
		return self::hasOne(Referal::className(), ['id' => 'referal_id']);
	}
}
