<?php namespace app\modules\partnership\models;

use app\modules\partnership\Module;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "pfx_partnership_refunds".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property boolean $is_refunded
 * @property string  $date
 * @property string  $wallet
 * @property double  $amount
 * @property string  $target
 *
 * @property Partner $partner
 */
class Refund extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	const TYPE_WALLET = 'wallet';
	const TYPE_INNER  = 'inner';

	public static function tableName()
	{
		return '{{%partnership_refunds}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['partner_id', 'amount'], 'required'],
			[['partner_id'], 'integer'],
			[['date'], 'safe'],
			[['amount'], 'number'],
			[['wallet'], 'string'],
			[['is_refunded'], 'boolean'],
			['target', 'in', 'range' => [self::TYPE_INNER, self::TYPE_WALLET]]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id'          => Module::t('module', 'ID'),
			'partner_id'  => Module::t('module', 'Partner ID'),
			'date'        => Module::t('module', 'Date'),
			'amount'      => Module::t('module', 'Amount'),
			'is_refunded' => Module::t('module', 'Refunded'),
			'target'      => Module::t('module', 'Target'),
			'wallet'      => Module::t('module', 'Wallet'),
		];
	}

	public function getPartner()
	{
		return self::hasOne(Partner::className(), ['id' => 'partner_id']);
	}

	public function close()
	{
		$payments = Payment::find()
			->where(['partner_id' => $this->partner_id])
			->andWhere('bonus > refunded')
			->orderBy(['date' => SORT_ASC]);

		$sum = $this->amount;
		foreach ($payments->each() as $payment) {
			/** @var Payment $payment */
			$to_refund = $payment->bonus - $payment->refunded;
			$refund_sum = $sum - $to_refund;
			if ($refund_sum < 0) {
				$payment->refunded += $sum;
				$payment->save();
				break;
			}
			$sum = $refund_sum;
			$payment->refunded += $to_refund;
			$payment->save();
		}

		$this->partner->balance -= $this->amount;
		$this->partner->save();
		$this->is_refunded = true;
		$this->save();
	}

	public static function updateRefundStatus()
	{

		$id = (int) Yii::$app->request->post('refund-id', 0);
		$action = Yii::$app->request->post('action', null);

		if ($id === 0 || is_null($action)) {
			return false;
		}

		/* @var $refund Refund */
		$refund = Refund::findOne($id);

		if (is_null($refund) || is_null($refund->partner) || $refund->is_refunded == 1) {
			return false;
		}

		if ($action == 'decline') {

			$message = Yii::$app->request->post('message', '');

			// TODO: отсылка сообщения пользователю

			$refund->delete();

			return true;
		}

		if ($refund->partner->balance < $refund->amount) {
			return false;
		}

		$refund->wallet = $refund->partner->walet;
		$refund->date = date('Y-m-d H:i:s');

		$transaction = Yii::$app->db->beginTransaction();

		try {
			$refund->close();
			$transaction->commit();
		} catch (\Exception $e) {
			$transaction->rollBack();
		}

		return true;

	}
}
