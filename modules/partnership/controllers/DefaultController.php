<?php

namespace app\modules\partnership\controllers;

use app\modules\partnership\components\Controller;
use app\modules\partnership\models\Manager;
use app\modules\partnership\models\Partner;
use app\modules\partnership\models\User;
use app\modules\partnership\forms\PartnerForm;
use Yii;

class DefaultController extends Controller
{
	protected $authorizationRequired = false;

	public function actionIndex()
	{
		/** @var User $user */
		$user = \Yii::$app->user->identity;

		if (is_null($user)) {
			return $this->render('index');
		}

		$manager = Manager::findOne(['user_id' => $user->id]);
		if (!is_null($manager)) {
			return $this->redirect(['manager/index']);
		}

		if ($user->isPartner) {
			return $this->redirect(['cabinet/index']);
		}

		$confirm = \Yii::$app->request->post('confirm_oferta', null);
		if ($confirm !== '1') {
			$model = new PartnerForm();
			return $this->render('index', ['model' => $model]);
		}

		$partner = new Partner([
			'user_id' => $user->id,
		]);
		$partner->generateReferalKey();
		if ($user->isReferral) {
			$partner->parent_id = $user->referral->partner_id;
		}
		$partner->save();

		return $this->redirect('/partnership/cabinet/index/?paymentinfo=1');

	}
}
