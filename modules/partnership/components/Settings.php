<?php namespace app\modules\partnership\components;

use app\modules\partnership\Module;
use yii\base\Model;

class Settings extends Model
{
	public $percent_1lvl;
	public $percent_2lvl;
	public $min_refund;

	public function rules()
	{
		return [
			[['percent_1lvl', 'percent_2lvl', 'min_refund'], 'required'],
			[['percent_1lvl', 'percent_2lvl'], 'number', 'min' => 0, 'max' => 1],
			['min_refund', 'number', 'min' => 0]
		];
	}

	public function attributeLabels()
	{
		return [
			'percent_1lvl' => Module::t('module', 'Bonus percentage for referral payments ({level} level)', ['level' => 1]),
			'percent_2lvl' => Module::t('module', 'Bonus percentage for referral payments ({level} level)', ['level' => 2]),
			'min_refund' => Module::t('module', 'Munimal refund'),
		];
	}

	public function getFileName()
	{
		$file = \Yii::getAlias('@app/modules/partnership/config/settings.json');
		if (!is_file($file)) {
			touch($file);
		}
		return $file;
	}

	public function restore()
	{
		$file = $this->getFileName();
		$data = json_decode(file_get_contents($file), true);
		if (!is_null($data)) {
			$this->load([$this->formName() => $data]);
		}
	}

	public function save($validate = true)
	{
		if ($validate && !$this->validate()) {
			return false;
		}
		$file = $this->getFileName();
		file_put_contents($file, json_encode($this->toArray()));
		return true;
	}
}