<?php namespace app\modules\partnership\components;

use Yii;

class Controller extends \yii\web\Controller
{
	protected $authorizationRequired = true;

	public function init()
	{
		if (Yii::$app->user->isGuest && $this->authorizationRequired) {
			//Yii::$app->user->loginRequired();
			Yii::$app->user->logout();
			Yii::$app->response->redirect(['/site/login']);
			Yii::$app->end();
		}
	}
}