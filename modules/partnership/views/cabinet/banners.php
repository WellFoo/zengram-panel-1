<?php
/**
 * @var \yii\web\View $this
 * @var \app\modules\partnership\models\Partner $partner
 */

use app\modules\partnership\asset\ModuleAsset;
use app\modules\partnership\models\Banner;
use app\modules\partnership\Module;
use yii\helpers\Html;

ModuleAsset::register($this);

$this->title = Module::t('module', 'Banners');
$this->registerCssFile('/css/partner.css');
?>

<style>
	.js-btn,
	.js-btn:hover {
		text-decoration: none;
		border-bottom: 1px dashed;
		font-style: italic;
	}

	.banner-container {
		margin-bottom: 20px;
		text-align: center;
	}
</style>

<div class="link-f clearfix">
	<a class="i2" href="/partnership/cabinet/index/" style="background-color: #E8E8E8;">Рефералы</a>
	<a class="i1 active" href="/partnership/cabinet/banners/" style="background-color: #FFF;">Рекламные материалы</a>
</div>

<div class="reflinkb">ВАША РЕФЕРАЛЬНАЯ ССЫЛКА</div>
<input value="<?= $partner->referal_link ?>" type="text" class="refinput">

<?= Html::a(
	Module::t('module', 'copy to clipboard'),
	'#сopy',
	[
		'data-clipboard-text' => $partner->referal_link,
		'class'               => 'js-btn',
		'data-original-title' => 'Ссылка скопирована в буфер обмена',
		'data-content'        => 'Чтобы вставить ссылку в нужное место, нажмите Ctrl+V или правую кнопку мыши + вставить',
		'id'                  => 'copy_btn',
	]
) ?>

<div class="row">
	<div class="name-cat">Инструкции</div>

	<div class="faq">
		<div class="faqitem">
			<div class="name">Итак, Вы стали нашим партнёром!</div>
			<p>
				1. Сразу после регистрации в личном кабинете партнера Вам доступна уникальная реферальная ссылка.<br> Её можно найти в разделе “Рекламные материалы” в 2-х вариантах:
			</p>
			<ul>
				<li>ссылка в виде url</li>
				<li>баннер с вшитой ссылкой</li>
			</ul>


			<p>
				2. Рекламные материалы очень удобно использовать для привлечения новых пользователей в наш сервис и для Вашего заработка. <br>Вы можете поставить ссылку в тексте своей статьи или
				разместить банер на своем сайте.
			</p>
			<p>
				3. В личном кабинете партнёра в разделе “Рефералы” отображается информация по:
			</p>
			<ul>
				<li>количеству заказов Ваших рефералов 1-го и 2-го уровня;</li>
				<li>сумме заказов Ваших рефералов;</li>
				<li>Вашему бонусу и кошельку для вывода средств;</li>
				<li>дате оплаты по Вашей реферальной ссылке;</li>
				<li>срокам и фактическим суммам выводов средств.</li>
			</ul>

			<p>
				4. Вывести заработанные средства Вы можете с помощью кнопки “Вывести средства”, которая находится в верхнем левом углу. <br>Вывод средств на сумму менее 1 000 руб. производится во
				второй понедельник месяца. <br>Вывод средств на сумму более 1 000 руб. производится в течение 3-х рабочих дней после Вашей заявки. <br>Заявку можно отправить с помощью специальной
				формы в личном кабинете.
			</p>
			<p>
				5. ВНИМАНИЕ! Вы можете вывести средства на свой кошелек или использовать баланс партнёра для продвижения своих проектов в zengram.
			</p>
			<div class="name">Условия партнерской программы</div>

			<p>От заказов всех Ваших рефералов Вы получаете 5%. Например, Ваш реферал сделал заказ на сумму 4999&nbsp;руб. Ваше вознаграждение составит 249.95&nbsp;руб.
				Дополнительный 1% от суммы заказов каждого Вашего реферала второго уровня. Например. Ваш реферал стал партнером. По его реферальной ссылке сделали заказ на сумму 4999&nbsp;руб. Вы
				получаете 1%, то есть 49,99&nbsp;руб.
			</p>
			<p>
				P.S.: Если Вы увидели какой-то баг, нашли ошибку, у Вас остались какие-либо вопросы, то напишите нам на <a href="mailto:support@zengram.ru">support@zengram.ru</a>.<br> Мы ответим Вам в
				кратчайшие сроки.
			</p>
			<p>
				С уважением, команда zengram.ru
			</p>
		</div>
	</div>

	<div class="name-cat">Баннеры</div>

	<div class="faq">

		<div class="faqitem">
			<div class="row">
				<div class="col-md-3 col-lg-3 iframe-container">
					<iframe width="240px" height="400px" seamless="seamless" scrolling="no"
					        src="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '240x400', 'nonclickable' => 1], true) ?>"
					        data-url="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '240x400'], true) ?>"
					></iframe>
					<div class="banner-links">
						<a href="javascript:void(0)" class="view-code">Код баннера</a><br>
						<a href="javascript:void(0)" class="view-banner">Просмотреть</a>
					</div>
				</div>
				<div class="col-md-3 col-lg-3 col-xs-3 col-sm-3 iframe-container">
					<iframe width="200px" height="200px" seamless="seamless" scrolling="no" style="margin: 0 auto; display: block;"
					        src="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '200x200', 'nonclickable' => 1], true) ?>"
					        data-url="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '200x200'], true) ?>"
					></iframe>
					<div class="banner-links">
						<a href="javascript:void(0)" class="view-code">Код баннера</a><br>
						<a href="javascript:void(0)" class="view-banner">Просмотреть</a>
					</div>
				</div>
				<div class="col-md-3 col-xs-3 col-lg-3 col-sm-3 iframe-container">
					<iframe width="125px" height="125px" seamless="seamless" scrolling="no" style="margin: 0 auto; display: block;"
					        src="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '125x125', 'nonclickable' => 1], true) ?>"
					        data-url="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '125x125'], true) ?>"
					></iframe>
					<div class="banner-links">
						<a href="javascript:void(0)" class="view-code">Код баннера</a><br>
						<a href="javascript:void(0)" class="view-banner">Просмотреть</a>
					</div>
				</div>
			</div>

			<div class="row" style="margin-top:40px;">
				<div class="col-md-5 col-xs-5 col-lg-5 col-sm-5 iframe-container">
					<iframe width="468px" height="60px" seamless="seamless" scrolling="no"
					        src="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '468x60', 'nonclickable' => 1], true) ?>"
					        data-url="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '468x60'], true) ?>"
					></iframe>
					<div class="banner-links">
						<a href="javascript:void(0)" class="view-code">Код баннера</a><br>
						<a href="javascript:void(0)" class="view-banner">Просмотреть</a>
					</div>
				</div>
			</div>
			<div class="row" style="margin-top:40px;">
				<div class="col-md-8 col-xs-8 col-lg-8 col-sm-8 iframe-container">
					<iframe width="728px" height="90px" seamless="seamless" scrolling="no"
					        src="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '728x90', 'nonclickable' => 1], true) ?>"
					        data-url="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '728x90'], true) ?>"
					></iframe>
					<div class="banner-links">
						<div class="clearfix" style="padding-bottom: 10px;"></div>
						<a href="javascript:void(0)" class="view-code">Код баннера</a><br>
						<a href="javascript:void(0)" class="view-banner">Просмотреть</a>
					</div>
				</div>
			</div>
			<div class="row" style="margin-top:40px;">
				<div class="col-md-12 col-xs-12 col-lg-12 col-sm-12 iframe-container">
					<iframe width="1050px" height="90px" seamless="seamless" scrolling="no"
					        src="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '1050x90', 'nonclickable' => 1], true) ?>"
					        data-url="<?= \yii\helpers\Url::to(['/banner', 'ref' => $partner->ref_key, 'size' => '1050x90'], true) ?>"
					></iframe>
					<div class="banner-links">
						<div class="clearfix" style="padding-bottom: 10px;"></div>
						<a href="javascript:void(0)" class="view-code">Код баннера</a><br>
						<a href="javascript:void(0)" class="view-banner">Просмотреть</a>
					</div>
				</div>
			</div>
			<!--
			<div class="name">Что нужно чтобы начать работу?</div>
			<p>Вам необходимо добавить сайт, который вы хотите продвигать. Выбрать регионы продвижения, добавить ключевые слова и релевантные им страницы сайта, оплатить бюджет проекта.</p>
			-->
		</div>
	</div>
</div>


<div id="partnership-banner" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Код баннера</h4>
			</div>
			<div class="modal-body">
				<textarea name="" id="banner-code" cols="60" rows="10"></textarea>

			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="partnership-banner-show" class="modal fade" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" style="max-width: none;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
						aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Баннер в реальную величину</h4>
			</div>
			<div class="modal-body img-banner" style="overflow-x: auto">

			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="container-fluid">
	<div class="row">


		<?php /*  foreach(Banner::find()->each() as $banner):
			// @var Banner $banner
			$code = str_replace('{{URL}}', $partner->referal_link, $banner->code);
			?>
			<div class="col-md-3 banner-container">
				<div class="banner"><?= $code ?></div>
				<a class="js-btn" href="#" data-clipboard-text="<?= htmlspecialchars($code) ?>"><?= Module::t('module', 'Copy banner code')?></a>
			</div>
		<?php endforeach; */ ?>

	</div>
</div>

<script>
	//jQuery(function() {
	//	new Clipboard('.js-btn');
	//});
</script>

<script>
	jQuery(function ()
	{
		new Clipboard('#copy_btn');
		$('#copy_btn').popover({
			placement: 'top',
			container: 'body',
			trigger: 'click'
		});

		/*
		 $('#copy_btn').mouseout(function(){
		 $('#copy_btn').popover('hide');
		 //$('#copy_btn').trigger('click');
		 });
		 */

	});
	$('.view-code').click(function ()
	{
		var code = getIframeCode($(this).closest('.iframe-container').find('iframe'));

		$('#banner-code').text(code);
		//<a href="<?= $partner->referal_link ?>">Код баннера</a>
		$('#partnership-banner').modal('show');
	});

	$('.view-banner').click(function ()
	{
		var code = getIframeCode($(this).closest('.iframe-container').find('iframe'));
		$('.img-banner').html(code);
		$('#partnership-banner-show').modal('show');
	});

	function getIframeCode(element)
	{
		var iframe = $(element),
			iframeLink = iframe.data('url'),
			iframeWidth = iframe.width(),
			iframeHeight = iframe.height();
		return '<iframe width="' + iframeWidth + 'px" height="' + iframeHeight + 'px" seamless="seamless" scrolling="no" src="' + iframeLink + '"></iframe>';
	}

</script>