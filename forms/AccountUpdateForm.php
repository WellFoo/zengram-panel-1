<?php

namespace app\forms;

use app\models\Account;
use app\models\AccountProcessLog;
use core\logger\Logger;
use yii\base\Model;
use Yii;

class AccountUpdateForm extends Model
{
	/** @var Account */
	private $_account;
	public $id;
	public $login;
	public $password;

	public function rules()
	{
		return [
			['login', 'filter', 'filter' => 'trim'],
			[['login', 'password'], 'required'],
			['login', 'string', 'max' => 50],
			['password', 'string', 'max' => Account::MAX_PASSWORD_LENGTH],
			['login', 'checkLogin'],
			['id', 'number']
		];
	}

	public function attributeLabels()
	{
		return [
			'login'    => Yii::t('app', 'Login'),
			'password' => Yii::t('app', 'Password')
		];
	}

	public function checkLogin($attribute, $value)
	{
		if ($this->hasErrors()) {
			return;
		}

		if ($this->_account === null) {
			$this->_account = Account::findOne($this->id);
		}

		if ($this->_account === null) {
			$this->addError($attribute, \Yii::t('app', 'Account not found'));
			return;
		}

		$this->_account->login = $this->login;
		$this->_account->password = $this->password;

		if (!$this->_account->validate(['login'])) {
			$this->addErrors($this->_account->getErrors());
			return;
		}

		if (!$this->_account->checkLogin(true, $this->_account->user_id)) {
			$this->addErrors($this->_account->getErrors());
		}
	}

	public function save($validate = false)
	{
		if ($this->validate()) {
			$this->_account->login = $this->login;
			$this->_account->password = $this->password;

			if ($this->_account->password != $this->password) {
				AccountProcessLog::log([
					'worker_ip' => ip2long($this->_account->server),
					'instagram_id' => $this->_account->instagram_id,
					'user_id' => $this->_account->user_id,
					'code' => Logger::Z010_ACCOUNT_PASSWORD_CHANGE
				]);
			}

			if ($this->_account->login != $this->login) {
				AccountProcessLog::log([
					'worker_ip' => ip2long($this->_account->server),
					'instagram_id' => $this->_account->instagram_id,
					'user_id' => $this->_account->user_id,
					'code' => Logger::Z010_ACCOUNT_LOGIN_CHANGE
				]);
			}

			return $this->_account->save($validate);
		}
		return false;
	}

	public function setAccount($value)
	{
		$this->_account = $value;
		$this->id = $this->_account->id;
		$this->login = $this->_account->login;
	}
}