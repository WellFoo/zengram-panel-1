<?php

use app\components\Config;

require(__DIR__.'/lang.php');
require(__DIR__.'/../components/Config.php');

define('LANGUAGE_CONFIG_DIR', __DIR__.DIRECTORY_SEPARATOR.LANGUAGE);

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');
return [
	'id' => 'basic-console',
	'basePath' => dirname(__DIR__),
	'bootstrap' => ['log'],
	'language' => LANGUAGE,
	'controllerNamespace' => 'app\commands',
	'modules' => [
		'gii' => 'yii\gii\Module',
	],
	'components' => [
		'mailer'  => Config::load('mailer'),
		'db'      => Config::load('db'),
		'log_db'  => Config::load('log_db'),
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'urlManager' => [
			'class'           => 'yii\web\UrlManager',
			'showScriptName'  => false,
			'enablePrettyUrl' => true,
			'scriptUrl'       => 'https://zengram.ru/',
			'baseUrl'         => 'https://zengram.ru/',
		],
		'log' => [
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => ['error', 'warning'],
				],
			],
		],
		'curl' => [
			'class' => 'firstgoer\CurlComponent',
			'options' => [
				'timeout' => 2
			]
		],
		'i18n'   => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\PhpMessageSource',
				],
			],
		],
		'formatter' => [
			'class' => 'yii\i18n\Formatter',
		],
		'ga' => [
			'class' => 'baibaratsky\yii\google\analytics\MeasurementProtocol',
			'trackingId' => LANGUAGE == 'ru' ? 'UA-66183269-1': 'UA-70044419-1', // Put your real tracking ID here

			// These parameters are optional:
			'useSsl' => false, // If you'd like to use a secure connection to Google servers
			'overrideIp' => true, // By default, IP is overridden by the user's one, but you can disable this
			'anonymizeIp' => false, // If you want to anonymize the sender's IP address
			'asyncMode' => false, // Enables the asynchronous mode (see below)
		],
	],
	'params' => Config::load('params'),
];
