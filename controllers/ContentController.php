<?php

namespace app\controllers;

use app\components\RPCHelper;
use app\components\YiiImagick;
use app\models\Account;
use app\components\Controller;
use app\models\BalanceFlow;
use app\models\MediaCount;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use app\models\Users;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;

class ContentController extends Controller
{
	public function actionIndex($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$filter = Yii::$app->request->get('filter');
		if ($filter !== 'new') {
			$filter = 'all';
		}

		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		/* @var $account Account */
		$account = Account::findOne($id);
		if ($account === null) {
			throw new NotFoundHttpException(Yii::t('app', 'Account id #{id} not found', ['id' => $id]));
		}

		$account->medias = $this->getMediaList($account, $filter === 'new');

		return $this->render('index', [
			'account' => $account,
			'filter'  => $filter
		]);
	}

	/**
	 * @param Account $account
	 * @param boolean $new_only
	 * @param boolean $max_id
	 * @param integer $limit
	 * @return array
	 */
	private function getMediaList($account, $new_only = false, $max_id = false, $limit = 18)
	{
		$result = ['data' => [], 'more' => false];
		$empty_results = 0;

		do {
			$response = RPCHelper::getMedia($account, $max_id);
			if ($response['status'] !== 'ok') {
				$result['error'] = $response['message'];
				break;
			}
			list($medias, $max_id) = $this->markNewComments($response['data']['data'], $account->id, $new_only);
			if (!count($medias)) {
				$empty_results++;
			} else {
				$empty_results = 0;
			}

			$result['data'] = array_merge($result['data'], $medias);
			$result['more'] = !empty($response['data']['more']);
		} while (count($result['data']) < $limit && $result['more'] && $empty_results < 3);

		$result['data'] = array_slice($result['data'], 0, $limit);
		return $result;
	}

	private function markNewComments($medias, $account_id, $new_only = false)
	{
		$result = [];
		$max_id = false;

		for ($i = 0; $i < count($medias); $i++) {
			/** @var MediaCount $commentsCount */
			$commentsCount = MediaCount::find()->where(['media_id' => $medias[$i]['id']])->one();

			if (!is_null($commentsCount)) {
				$medias[$i]['new_comments'] = $commentsCount->new_comments;
			} else {
				$commentsCount             = new MediaCount();
				$commentsCount->media_id   = $medias[$i]['id'];
				$commentsCount->account_id = $account_id;
				$commentsCount->comments   = $medias[$i]['comment_count'];
				$commentsCount->save();
				$medias[$i]['new_comments'] = 0;
			}

			if (!($new_only && $medias[$i]['new_comments'] == 0)) {
				$result[] = $medias[$i];
			}

			if ($i === count($medias) - 1) {
				$max_id = $medias[$i]['id'];
			}
		}

		return [$result, $max_id];
	}

	public function actionCommentscounter()
	{
		$mediaId  = Yii::$app->request->post('mediaId');
		$comments = Yii::$app->request->post('comments');
		/** @var MediaCount $counter */
		$counter  = MediaCount::find()->where("media_id = '".$mediaId."'")->one();
		if ($counter) {
			$counter->comments = $comments;
			$counter->new_comments = 0;
			if (!$counter->save()) {
				return 'Error';
			}
		} else {
			throw new NotFoundHttpException(Yii::t('app', 'Media {media_id} not found', ['media_id' => $mediaId]));
		}
		return '';
	}

	public function actionMore($id)
	{
		/* @var $account Account */
		$account = Account::findOne($id);
		if ($account === null) {
			throw new NotFoundHttpException(Yii::t('app', 'Account id #{id} not found', ['id' => $id]));
		}

		$max_id = Yii::$app->request->get('max_id', false);

		$filter = Yii::$app->request->get('filter');
		if ($filter !== 'all') {
			$filter = 'new';
		}

		$account->medias = $this->getMediaList($account, $filter === 'new', $max_id);

		return $this->renderPartial('medias', [
			'account' => $account,
			'prev'    => true,
		]);
	}

	public function actionComments($id)
	{
		/* @var $account Account */
		$account = Account::findOne($id);
		if ($account === null) {
			throw new NotFoundHttpException(Yii::t('core', 'Account id #{id} not found', ['id' => $id]));
		}

		$media_id = Yii::$app->request->get('media_id');
		if (is_null($media_id)) {
			throw new BadRequestHttpException();
		}

		$max_id = Yii::$app->request->get('max_id');

		$response = RPCHelper::getMediaComments($account, $media_id, $max_id);
		if ($response['status'] !== 'ok') {
			throw new ServerErrorHttpException();
		}

		$account->mediaComments = $response['data'];

		return $this->renderPartial('comments', [
			'account'  => $account,
			'media_id' => $media_id
		]);
	}

	public function actionComment($id)
	{
		$media_id = Yii::$app->request->post('media_id');
		$comment = Yii::$app->request->post('comment');
		if (is_null($media_id) || is_null($comment)) {
			throw new BadRequestHttpException();
		}

		/* @var $account Account */
		$account = Account::findOne($id);
		if ($account === null) {
			throw new NotFoundHttpException(Yii::t('core', 'Account id #{id} not found', ['id' => $id]));
		}

		/** @var Users $user */
		$user = Yii::$app->user->identity;
		if ($user->balance <= 0) {
			return Yii::t('core', 'Not enough balance');
		}

		$response = RPCHelper::sendComment($account, $media_id, $comment);
		if ($response['status'] !== 'ok') {
			return Yii::t('core', 'Instagram error.') . ' ' . $response['error'];
		}

		$response = RPCHelper::getMediaComments($account, $media_id);
		if ($response['status'] !== 'ok') {
			return Yii::t('app', 'Instagram error.').' '.$response['error'];
		}

		$account->mediaComments = $response['data'];
		$account->manual_comments++;
		$account->save();

		return $this->renderPartial('comments', [
			'account'  => $account,
			'media_id' => $media_id
		]);
	}



//	public function actionCommentRemove($id)
//	{
//		if (!isset($_POST['media_id']) || !isset($_POST['comment_id'])) {
//			throw new BadRequestHttpException();
//		}
//
//		/* @var $account Account */
//		$account = Account::findOne($id);
//		if ($account === null) {
//			throw new NotFoundHttpException(Yii::t('app', 'Account id #{id} not found', ['id' => $id]));
//		}
//
//		if (Yii::$app->user->identity->balance <= 0) {
//			return Yii::t('app', 'Not enough balance');
//		}
//
//		$account->removeComment($_POST['media_id'], $_POST['comment_id']);
//		if (!empty($account->removeCommentResult['error'])) {
//			return Yii::t('app', 'Instagram error.').' '.$account->removeCommentResult['error'];
//		}
//		$account->getMediaComments($_POST['media_id'], false);
//		return $this->renderPartial('comments', ['account' => $account, 'media_id' => $_POST['media_id']]);
//	}

	public function actionCommentRemove($id)
	{
		$media_id = Yii::$app->request->post('media_id');
		$comment_id = Yii::$app->request->post('comment_id');
		if (is_null($media_id) || is_null($comment_id)) {
			throw new BadRequestHttpException();
		}

		/* @var $account Account */
		$account = Account::findOne($id);
		if ($account === null) {
			throw new NotFoundHttpException(Yii::t('core', 'Account id #{id} not found', ['id' => $id]));
		}

		/** @var Users $user */
		$user = Yii::$app->user->identity;
		if ($user->balance <= 0) {
			return Yii::t('core', 'Not enough balance');
		}

		$response = RPCHelper::removeComment($account, $media_id, $comment_id);
		if ($response['status'] !== 'ok') {
			return Yii::t('core', 'Instagram error.').' '.ArrayHelper::getValue($response, 'error');
		}

		$response = RPCHelper::getMediaComments($account, $media_id);
		if ($response['status'] !== 'ok') {
			return Yii::t('app', 'Instagram error.').' '.$response['error'];
		}

		$account->mediaComments = $response['data'];

		return $this->renderPartial('comments', [
			'account'  => $account,
			'media_id' => $media_id
		]);
	}

	public function actionUpload()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$file = UploadedFile::getInstanceByName('file');
		if (!in_array($file->extension, ['png', 'gif', 'jpg', 'jpeg'])){
			return ['error' => Yii::t('app', 'Unsupported image type')];
		}
		$resultPath = '/userdata/uploads/'. Yii::$app->user->id.'_'.time().'.'.$file->extension;
		$result = $file->saveAs(Yii::$app->basePath.$resultPath);
		if (!$result){
			return ['error' => Yii::t('app', 'File upload failed, please try again later')];
		}
//		$imagesize = getimagesize(Yii::$app->basePath.$resultPath);
//		if ($imagesize[0] < 320 || $imagesize[0] > 1080 || $imagesize[1] < 320 || $imagesize[1] > 1080) {
//			return ['error' => Yii::t('app', 'Image have incorrect size. Width and height should be between 320 and 1080 pixels')];
//		}
		return ['result' => $result, 'src' => $resultPath];
	}

//	public function actionCrop($id)
//	{
//		Yii::$app->response->format = Response::FORMAT_JSON;
//		/** @var Account $account */
//		$account = Account::findOne($id);
//		if ($account === null) {
//			return ['error' => Yii::t('app', 'Account id #{id} not found', ['id' => $id])];
//		}
//		if (Yii::$app->user->identity->balance < Account::IMAGE_UPLOAD_COST) {
//			return ['error' => Yii::t('app', 'Not enough balance')];
//		}
//		$data = Yii::$app->request->post();
//		if (empty($data['image_src']) || !file_exists(Yii::$app->basePath.$data['image_src'])){
//			return ['error' => Yii::t('content', 'No image found')];
//		}
//		/** @var \app\components\YiiImagick $image */
//		$image = new YiiImagick(Yii::$app->basePath.$data['image_src']);
//		$originalSize = [$image->getImageWidth(), $image->getImageHeight()];
//
//		if (empty($data['zoom'])){
//			$data['zoom'] = 1;
//		} else {
//			$data['crop_width'] = $data['crop_width']/$data['zoom'];
//			$data['crop_height'] = $data['crop_height']/$data['zoom'];
//			$data['crop_x'] = $data['crop_x']/$data['zoom'];
//			$data['crop_y'] = $data['crop_y']/$data['zoom'];
//		}
//		$min_side_image = $originalSize[0] < $originalSize[1] ? $originalSize[0] : $originalSize[1];
//		$min_side_crop = $data['crop_width'] < $data['crop_height'] ? $data['crop_width'] : $data['crop_height'];
//		if ($min_side_image < $min_side_crop){
//			$min_side = $min_side_image;
//			$data['crop_x'] = 0;
//			$data['crop_y'] = 0;
//		} else {
//			$min_side = $min_side_crop;
//		}
////
////		if ($data['crop_width'] != $data['crop_height']){
////			if ($data['crop_width'] < $data['crop_height']){
////				$data['crop_height'] = $data['crop_width'];
////			} else {
////				$data['crop_width'] = $data['crop_height'];
////			}
////		}
//		$image->crop($min_side, $min_side, $data['crop_x'], $data['crop_y']);
//		if ($image->getImageWidth() > 1080){
//			$image->resize(1080, 1080);
//		}
//
//		if ($image->getImageWidth() < 320){
//			$image->resize(320, 320);
//		}
//
//		$image->placeRandomWatermark();
//
//		$quality = mt_rand(85,99);
//		file_put_contents(Yii::$app->basePath.'/test.jpeg',$image->render('jpeg', $quality));
//		$account->sendImage(base64_encode($image->render('jpeg', $quality)), $data['description'],
//			['size' => $originalSize,
//			'quality' => $quality,
//			'crop' => [$data['crop_x'], $data['crop_y']],
//			'zoom' => number_format($data['zoom'], 2, '.', '')]
//		);
//		if (!empty($account->imageResult['error'])) {
//			return ['error' => $account->imageResult['error']];
//		}
//		@unlink(Yii::$app->basePath.$data['image_src']);
//
//		/* @var $user Users */
//		$user = Users::findOne(['id' => Yii::$app->user->id]);
//		$user->balance -= Account::IMAGE_UPLOAD_COST;
//		$user->save();
//		$bf = new BalanceFlow([
//			'user_id' => Yii::$app->user->id,
//			'value'=> Account::IMAGE_UPLOAD_COST,
//			'description' => 'Загрузка изображения',
//			'type' => 'sub'
//		]);
//		$bf->save();
//		$account->posted_photos++;
//		$account->save();
//		return $account->imageResult;
////		return ['data' => $this->renderPartial('medias', ['account' => $account])];
//	}
//
//	public function actionImages($id)
//	{
//		return ['error' => Yii::t('app', 'Function temporary disabled')];
//		/* @var $account Account */
//		$account = Account::findOne($id);
//		if ($account === null) {
//			return ['error' => Yii::t('app', 'Account id #{id} not found', ['id' => $id])];
//		}
//
//		list($imageinfo, $data) = explode(',', $_POST['image']);
//		preg_match('#^data:image/(\w+);base64#i', $imageinfo, $matches);
//		$extension = $matches[1];
//		$account->sendImage($data, $extension, $_POST['caption']);
//		if (!empty($account->imageResult['error'])) {
//			return json_encode(['error' => $account->imageResult['error']]);
//		}
//
//		/* @var $user Users */
//		$user = Users::findOne(['id' => Yii::$app->user->id]);
//		$user->balance -= Account::IMAGE_UPLOAD_COST;
//		$user->save();
//		$bf = new BalanceFlow([
//				'user_id' => Yii::$app->user->id,
//				'value'=> Account::IMAGE_UPLOAD_COST,
//				'description' => 'Загрузка изображения',
//				'type' => 'sub'
//		]);
//		$bf->save();
//
//		$account->posted_photos++;
//		$account->save();
//
//		return json_encode(['data' => $this->renderPartial('medias', ['account' => $account])]);
//	}


	public function actionCrop($id)
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		/** @var Account $account */
		$account = Account::findOne($id);
		if ($account === null) {
			return ['error' => Yii::t('core', 'Account id #{id} not found', ['id' => $id])];
		}
		if (Yii::$app->user->identity->balance < Account::IMAGE_UPLOAD_COST) {
			return ['error' => Yii::t('core', 'Not enough balance')];
		}
		$data = Yii::$app->request->post();
		if (empty($data['image_src']) || !file_exists(Yii::$app->basePath.$data['image_src'])){
			return ['error' => Yii::t('content', 'No image found')];
		}
		/** @var \app\components\YiiImagick $image */
		$image = new YiiImagick(Yii::$app->basePath.$data['image_src']);
		$originalSize = [$image->getImageWidth(), $image->getImageHeight()];

		if (empty($data['zoom'])){
			$data['zoom'] = 1;
		} else {
			$data['crop_width'] = $data['crop_width']/$data['zoom'];
			$data['crop_height'] = $data['crop_height']/$data['zoom'];
			$data['crop_x'] = $data['crop_x']/$data['zoom'];
			$data['crop_y'] = $data['crop_y']/$data['zoom'];
		}
		$min_side_image = $originalSize[0] < $originalSize[1] ? $originalSize[0] : $originalSize[1];
		$min_side_crop = $data['crop_width'] < $data['crop_height'] ? $data['crop_width'] : $data['crop_height'];
		if ($min_side_image < $min_side_crop){
			$min_side = $min_side_image;
			$data['crop_x'] = 0;
			$data['crop_y'] = 0;
		} else {
			$min_side = $min_side_crop;
		}
//
//		if ($data['crop_width'] != $data['crop_height']){
//			if ($data['crop_width'] < $data['crop_height']){
//				$data['crop_height'] = $data['crop_width'];
//			} else {
//				$data['crop_width'] = $data['crop_height'];
//			}
//		}
		$image->crop($min_side, $min_side, $data['crop_x'], $data['crop_y']);
		if ($image->getImageWidth() > 1080){
			$image->resize(1080, 1080);
		}

		if ($image->getImageWidth() < 320){
			$image->resize(320, 320);
		}

		$image->placeRandomWatermark();

		$quality = mt_rand(85,99);
		file_put_contents(Yii::$app->basePath.'/test.jpeg',$image->render('jpeg', $quality));

		$result = RPCHelper::sendImage(
			$account,
			base64_encode($image->render('jpeg', $quality)),
			$data['description'],
			[
				'size' => $originalSize,
				'quality' => $quality,
				'crop' => [$data['crop_x'], $data['crop_y']],
				'zoom' => number_format($data['zoom'], 2, '.', '')
			]
		);

		if ($result['status'] !== 'ok') {
			return ['error' => $result['error']];
		}

		@unlink(Yii::$app->basePath.$data['image_src']);

		/* @var $user Users */
		$user = Users::findOne(['id' => Yii::$app->user->id]);
		$user->balance -= Account::IMAGE_UPLOAD_COST;
		$user->save();
		$bf = new BalanceFlow([
			'user_id' => Yii::$app->user->id,
			'value'=> Account::IMAGE_UPLOAD_COST,
			'description' => 'Загрузка изображения',
			'type' => 'sub'
		]);
		$bf->save();
		$account->posted_photos++;
		$account->save();
		return $account->imageResult;
//		return ['data' => $this->renderPartial('medias', ['account' => $account])];
	}

	public function actionImages(/** @noinspection PhpUnusedParameterInspection */$id)
	{
		return ['error' => Yii::t('core', 'Function temporary disabled')];
		/* @var $account Account */
		/** @noinspection PhpUnreachableStatementInspection */
		$account = Account::findOne($id);
		if ($account === null) {
			return ['error' => Yii::t('core', 'Account id #{id} not found', ['id' => $id])];
		}

		list($imageinfo, $data) = explode(',', $_POST['image']);
		preg_match('#^data:image/(\w+);base64#i', $imageinfo, $matches);
		$extension = $matches[1];

		// TODO: Переделать загрузку под новый формат
		$result = RPCHelper::sendImage(
			$account,
			$data,
			$_POST['caption'],
			[
				'size' => $originalSize,
				'quality' => $quality,
				'crop' => [$data['crop_x'], $data['crop_y']],
				'zoom' => number_format($data['zoom'], 2, '.', '')
			]
		);
		if ($result['status'] !== 'ok') {
			return json_encode(['error' => $result['error']]);
		}

		/* @var $user Users */
		$user = Users::findOne(['id' => Yii::$app->user->id]);
		$user->balance -= Account::IMAGE_UPLOAD_COST;
		$user->save();
		$bf = new BalanceFlow([
			'user_id' => Yii::$app->user->id,
			'value'=> Account::IMAGE_UPLOAD_COST,
			'description' => 'Загрузка изображения',
			'type' => 'sub'
		]);
		$bf->save();

		$account->posted_photos++;
		$account->save();

		return json_encode(['data' => $this->renderPartial('medias', ['account' => $account])]);
	}


}
