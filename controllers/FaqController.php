<?php

namespace app\controllers;

use app\components\Controller;
use app\models\FAQ;
use app\models\FAQCats;
use app\models\FAQRates;
use Yii;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FaqController extends Controller
{
	public function actionIndex($cat_id = null)
	{
		if ($cat_id) {
			$condition = ['cat_id' => $cat_id];
		} else {
			$condition = '1 = 1';
		}

		$query = FAQ::find()
			->where($condition)
			->groupBy(['id', 'rating'])
			->orderBy(['rating' => SORT_DESC]);


		$perPage = Yii::$app->request->get('per-page', 5);
		if ($perPage === 'all') {
			$perPage = 1000;
		}
		$paginator = new Pagination([
			'totalCount' => $query->count(),
			'pageSize'   => $perPage
		]);

		$items = $query
			->limit($paginator->limit)
			->offset($paginator->offset)
			->all();

		$rates = FAQRates::find()->where(['user_id' => Yii::$app->user->id])->asArray()->all();

		return $this->render('index', [
			'items'           => $items,
			'to_top'          => FAQ::findAll(['on_top' => 1]),
			'rates'           => ArrayHelper::map($rates, 'faq_id', 'rating'),
			'maxRate'         => FAQ::find()->max('rating'),
			'paginator'       => $paginator,
			'currentCategory' => FAQCats::findOne($cat_id),
		]);
	}

	public function actionRating()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		$id = Yii::$app->request->post('id', null);
		if (is_nan($id)) {
			throw new BadRequestHttpException();
		}

		$rating = Yii::$app->request->post('rating', null);
		if (!in_array($rating, ['yes', 'no'])) {
			throw new BadRequestHttpException();
		}

		/** @var FAQ $term */
		$term = FAQ::findOne($id);
		if (is_null($term)) {
			throw new NotFoundHttpException();
		}

		$term->doRate($rating);
		return ['status' => 'ok'];
	}
}