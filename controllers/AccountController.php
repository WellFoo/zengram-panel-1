<?php

namespace app\controllers;

use app\components\Counters;
use app\components\RPCHelper;
use app\forms\AccountCreateForm;
use app\forms\AccountUpdateForm;
use app\models\Account;
use app\components\Controller;
use app\models\AccountProcessLog;
use app\models\Users;
use app\models\UsersLog;
use app\models\UsersInfo;
use core\logger\Logger;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use Yii;

class AccountController extends Controller
{
	public function actionAdd()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;
		$model = new AccountCreateForm();
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			/** @var Account $account */
			$account = Account::find()->where(['id' => $model->new_id])->one();
			$place_json = ArrayHelper::getValue(Yii::$app->request->post(), 'place', '[]');
			$place = json_decode($place_json, true);
			if (!empty($place['place'])) {
				OptionsController::placeCreate($account, $place['place']);
			}
			$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => 'Добавлен аккаунт '.$model->login]);
			$logEntry->save();

			AccountProcessLog::log([
				'worker_ip' => ip2long($account->server),
				'instagram_id' => $account->instagram_id,
				'user_id' => $account->user_id,
				'code' => Logger::Z010_ACCOUNT_ADD,
			]);

			/** @var UsersInfo $modelInfo */
			$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
			if ($modelInfo === null){
				$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id]);
			}
			$modelInfo->try_add = 1;
			$modelInfo->save();

			$user_id = Yii::$app->user->id;
			if (!empty(Yii::$app->request->queryParams['preview'])) {
				if (Yii::$app->user->identity->isAdmin) {
					$user_id = Yii::$app->request->queryParams['preview'];
				}
			}

			Counters::roiStatsSendEvent('add_account');

			return [
				'result' => 'ok',
				'login' => $account->login,
				'account_id' => $model->new_id,
				'city' => !empty($place['place']['name']) ? $place['place']['name'] : '',
				'settings' => Url::to(['/options/index', 'id' => $model->new_id]),
				'model' => $account->getAttributes(),
				'options' => $account->options->getAttributes(),
				'account' => $this->renderPartial('//account/_account', [
					'speeds' => [Yii::t('views', 'slow'), Yii::t('views', 'medium'), Yii::t('views', 'fast')],
					'user' => Users::findIdentity($user_id),
					'account' => $account,
					'status' => 0,
					'newComments' => 0
			])];
		} else {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['errors' => $model->getErrors()];
		}
	}

	public function actionDelete($id)
	{
		if (!$id) {
			Yii::$app->session->setFlash('AccountDeletedError');
			Yii::$app->getResponse()->redirect(array('site/index'));
			return;
		}
		$account = Account::findOne(['user_id' => Yii::$app->user->id, 'id' => $id]);

		if ($account == null || !$account->id) {
			throw new ForbiddenHttpException(Yii::t('app', 'You cannot view that resource'));
		}
		/* @type Account $account */
		$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => 'Удален аккаунт '.$account->login]);
		$logEntry->save();

		AccountProcessLog::log([
			'worker_ip' => ip2long($account->server),
			'instagram_id' => $account->instagram_id,
			'user_id' => $account->user_id,
			'code' => Logger::Z010_ACCOUNT_DELETE,
		]);

		/** @var UsersInfo $modelInfo */
		$modelInfo = UsersInfo::findOne(['user_id' => Yii::$app->user->id]);
		if ($modelInfo === null){
			$modelInfo = new UsersInfo(['user_id' => Yii::$app->user->id]);
		}
		$modelInfo->deleted_account = 1;
		$modelInfo->save();
		$account->delete();

		Yii::$app->session->setFlash('AccountDeleted');
		Yii::$app->getResponse()->redirect(array('/'));
	}

	public function actionChange($id)
	{
		if (!$id) {
			Yii::$app->session->setFlash('AccountDeletedError');
			Yii::$app->getResponse()->redirect(array('site/index'));
			return "";
		}
		/* @var $account  Account */
		$account = Account::findOne(['id' => $id]);

		if ($account == null || !$account->id || (!Yii::$app->user->identity->isAdmin && $account->user_id != Yii::$app->user->id )) {
			throw new ForbiddenHttpException(Yii::t('app', 'You cannot view that resource'));
		}

		$model = new AccountUpdateForm();
		$model->setAccount($account);
		if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
			$logEntry = new UsersLog(['user_id' => Yii::$app->user->id, 'text' => 'Изменен пароль от '.$model->login]);
			$logEntry->save();
			//return Yii::$app->getResponse()->redirect(array('/'));
			return Yii::$app->getResponse()->redirect(['/', 'show' => 'changed']);
		} else {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ['errors' => $model->getErrors()];
		}
	}
	
	public function actionChangeForm($id){

		if (!$id) {
			Yii::$app->session->setFlash('AccountDeletedError');
			Yii::$app->getResponse()->redirect(array('/'));
			return "";
		}
		/* @var $account  Account */
		$account = Account::findOne(['id' => $id]);

		if ($account == null || !$account->id || (!Yii::$app->user->identity->isAdmin && $account->user_id != Yii::$app->user->id )) {
			throw new ForbiddenHttpException(Yii::t('app', 'You cannot view that resource'));
		}
		$model = new AccountUpdateForm();
		$model->setAccount($account);
		return $this->renderAjax('change', [
			'model' => $model,
		]);

	}

	public function actionAjaxValidate()
	{
		if (!Yii::$app->request->isAjax) {
			throw new BadRequestHttpException();
		}

		Yii::$app->response->format = Response::FORMAT_JSON;

		if (isset($_POST['AccountCreateForm'])) {
			$model = new AccountCreateForm();
		} else {
			$model = new AccountUpdateForm();
		}

		$model->load(Yii::$app->request->post());
		return ActiveForm::validate($model);
	}

	public function actionClearcomments($id = null)
	{
		Account::unmarkNewComments($id);
	}

	public function actionPubimages($id = null)
	{
		if (is_null($id)) {
			$queues   = Yii::$app->postdb->createCommand(
				'SELECT id, encode(image::bytea, \'base64\') as image, status, comment, users, added, pause_from, pause_to,
				array_to_string(fail_users, \', \') as fails, array_to_string(success_users, \', \') as success
				FROM imagesqueue WHERE user_id='.Yii::$app->user->id.' ORDER BY id DESC'
			)->queryAll();
			$projects = Account::find()->where('user_id = '.Yii::$app->user->id)->all();
			return $this->render(
				'/account/pubimages',
				[
					'queues'   => $queues,
					'projects' => $projects,
				]);
		} else {
			$logs     = Yii::$app->postdb->createCommand(
				'SELECT id, account_id, text, stop
				FROM imagelog WHERE queue_id='.$id
			)->queryAll();
			$projects = Account::find()->where('user_id = '.Yii::$app->user->id)->all();
			return $this->render(
				'/account/pubimageslog',
				[
					'logs'     => $logs,
					'projects' => $projects
				]);
		}
	}

	public function actionRefundPolicyAccept()
	{
		Yii::$app->response->format = Response::FORMAT_JSON;

		if (!Yii::$app->request->post('accept')) {
			return ['status' => 'error'];
		}

		$userInfo = UsersInfo::findOne(Yii::$app->user->id);
		if (is_null($userInfo)) {
			$userInfo = new UsersInfo([
				'user_id' => Yii::$app->user->id,
			]);
		}

		$userInfo->refund_policy_accepted = time();

		return [
			'status' => $userInfo->save() ? 'ok' : 'error'
		];
	}
}
