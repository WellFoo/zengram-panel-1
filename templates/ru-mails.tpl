<!-- 0. Шаблон сообщения -->
--main--

<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>zengram</title>
	<style type="text/css">
		#outlook a {padding:0;}
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		.ExternalClass {width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
		a img {border:none;}
		.image_fix {display:block;}
		p {margin: 0px 0px !important;}

		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		table[class=full] { width: 100%; clear: both; }

		/*################################################*/
		/*IPAD STYLES*/
		/*################################################*/
		@media only screen and (max-width: 640px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				color: #ffffff; /* or whatever your want */
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				color: #ffffff !important;
				pointer-events: auto;
				cursor: default;
			}
			table[class=devicewidth] {width: 440px!important;max-width: 440px!important;text-align:center!important;}
			table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
			table[class="sthide"]{display: none!important;}
			img[class="bigimage"]{width: 420px!important;height:219px!important;}
			img[class="col2img"]{width: 420px!important;height:258px!important;}
			img[class="image-banner"]{width: 440px!important;height:106px!important;}
			td[class="menu"]{text-align:center !important; padding: 0 0 10px 0 !important;}
			td[class="logo"]{padding:10px 0 5px 0!important;margin: 0 auto !important;}
			img[class="logo"]{padding:0!important;margin: 0 auto !important;}

		}
		/*##############################################*/
		/*IPHONE STYLES*/
		/*##############################################*/
		@media only screen and (max-width: 480px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				color: #ffffff; /* or whatever your want */
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				color: #ffffff !important;
				pointer-events: auto;
				cursor: default;
			}
			table[class=devicewidth] {width: 280px!important;max-width: 280px!important;text-align:center!important;}
			table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
			table[class="sthide"]{display: none!important;}
			img[class="bigimage"]{width: 260px!important;height:136px!important;}
			img[class="col2img"]{width: 260px!important;height:160px!important;}
			img[class="image-banner"]{width: 280px!important;height:68px!important;}

		}
	</style>

	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="block">
	<!-- start of header -->
	<table width="100%" bgcolor="#f2f2f2" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
		<tbody>
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit">
					<tbody>
					<tr>
						<td>
							<!-- logo -->
							<table width="100%" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth">
								<tbody>
								<tr>
									<td width="30%">&nbsp;</td>
									<td width="40%" class="logo">
										<center><img src="https://web.pechkin-mail.ru/editor/userfiles/303365/2logo.png" alt="&nbsp;" border="0" style="display:block; border:none; outline:none; text-decoration:none; margin-left: auto; margin-right: auto; margin: 30px auto!important;" st-image="edit" class="logo"></center>
									</td>
									<td width="30%">&nbsp;</td>
								</tr>
								</tbody>
							</table>
							<!-- End of logo -->
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- end of header -->
</div><div class="block">
	<!-- image + text -->
	<table width="100%" bgcolor="#f2f2f2" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="bigimage">
		<tbody>
		<tr>
			<td>
				<table bgcolor="#ffffff" width="580" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
					<tbody>
					<tr>
						<td width="100%" height="20"></td>
					</tr>
					<tr>
						<td>
							<table width="500" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
								<tbody>

								<tr>
									<td width="100%" height="20"></td>
								</tr>
								<tr>
									<td style="font-family: 'PT Sans', Arial, sans-serif; font-size: 14px; color: #6c6c6c; text-align: left; line-height: 20px;" st-content="rightimage-paragraph">

										%CONTENT%

									</td>
								</tr>

								<tr>
									<td width="100%" height="20"></td>
								</tr>


								</td>
								</tr>
								<!-- end of content -->
								<!-- Spacing -->
								<tr>
									<td width="100%" height="10"></td>

								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>

<div class="block">
	<!-- start textbox-with-title -->
	<table width="100%" bgcolor="#f2f2f2" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="fulltext">
		<tbody>
		<tr>
			<td>
				<table bgcolor="#fff" width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit">
					<tbody>

					<tr>
						<td>
							<table width="340" align="center" cellpadding="0" cellspacing="0" border="0" class="devicewidthinner">
								<tbody>



								<!-- button -->
								<tr>
									<td>

										<table height="24" align="center" valign="middle" border="0" cellpadding="0" cellspacing="0" class="tablet-button" st-button="edit">
											<tbody>
											<tr>
												<td width="auto" align="center" valign="middle" height="24" >


													%FOOTER%

												</td>
											</tr>
											</tbody>
										</table>



									</td>
								</tr>
								<!-- /button -->

								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- end of textbox-with-title -->
</div>


%BLOCK%

<div class="block">
	<!-- start of left image -->
	<table width="100%" bgcolor="#f2f2f2" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="leftimage">
		<tbody>
		<tr>
			<td>
				<table bgcolor="#ffffff" width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit">
					<tbody>
					<!-- Spacing -->
					<tr>
						<td height="20"></td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td>
							<table width="540" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
								<tbody>
								<tr>
									<td>
										<!-- start of text content table -->
										<table width="200" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
											<tbody>
											<!-- image -->
											<tr>
												<td width="300" height="80" align="center" style="font-family: 'PT Sans', Arial, sans-serif; font-size: 14px; color: #5b5b5b; padding-left: 30px;">
													<div style="border-top:2px solid #e9e9e9; width:200px;"></div>
													Команда zengram.ru<br/>
													<img src="http://f.imgsurl.com/303365/3logo2.png">
												</td>
											</tr>
											</tbody>
										</table>
										<!-- mobile spacing -->
										<table align="left" border="0" cellpadding="0" cellspacing="0" class="mobilespacing">
											<tbody>
											<tr>
												<td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
											</tr>
											</tbody>
										</table>
										<!-- mobile spacing -->

									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td height="20"></td>
					</tr>
					<!-- Spacing -->
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- end of left image -->
</div>



<div class="block">
	<!-- Start of preheader -->
	<table width="100%" bgcolor="#f2f2f2" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="postfooter">
		<tbody>
		<tr>
			<td width="100%">
				<table width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
					<tbody>
					<!-- Spacing -->
					<tr>
						<td width="100%" height="5"></td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td align="left" valign="middle" style="font-family: 'PT Sans', Arial, sans-serif; font-size: 13px; color: #5b5b5b; padding-left: 50px;" st-content="preheader">
							Это сообщение не требует ответа и сформировано автоматически.<br/>
							Для обращения в службу поддержки zengram.ru  используйте <span style="color:#518ed4;">[онлайн сервис]</span>.<br/>
							<br/>
							© 2014-2015 Zengram. All rights reserved.
						</td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td width="100%" height="5"></td>
					</tr>
					<!-- Spacing -->
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- End of preheader -->
</div>

</body></html>


--SEPARATOR--

<!-- 0. Шаблон сообщения (без отступов)-->
--main-nopaddings--

<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>zengram</title>
	<style type="text/css">
		#outlook a {padding:0;}
		body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
		.ExternalClass {width:100%;}
		.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
		#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
		img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}
		a img {border:none;}
		.image_fix {display:block;}
		p {margin: 0px 0px !important;}

		table td {border-collapse: collapse;}
		table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
		table[class=full] { width: 100%; clear: both; }

		/*################################################*/
		/*IPAD STYLES*/
		/*################################################*/
		@media only screen and (max-width: 640px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				color: #ffffff; /* or whatever your want */
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				color: #ffffff !important;
				pointer-events: auto;
				cursor: default;
			}
			table[class=devicewidth] {width: 440px!important;max-width: 440px!important;text-align:center!important;}
			table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}
			table[class="sthide"]{display: none!important;}
			img[class="bigimage"]{width: 420px!important;height:219px!important;}
			img[class="col2img"]{width: 420px!important;height:258px!important;}
			img[class="image-banner"]{width: 440px!important;height:106px!important;}
			td[class="menu"]{text-align:center !important; padding: 0 0 10px 0 !important;}
			td[class="logo"]{padding:10px 0 5px 0!important;margin: 0 auto !important;}
			img[class="logo"]{padding:0!important;margin: 0 auto !important;}

		}
		/*##############################################*/
		/*IPHONE STYLES*/
		/*##############################################*/
		@media only screen and (max-width: 480px) {
			a[href^="tel"], a[href^="sms"] {
				text-decoration: none;
				color: #ffffff; /* or whatever your want */
				pointer-events: none;
				cursor: default;
			}
			.mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
				text-decoration: default;
				color: #ffffff !important;
				pointer-events: auto;
				cursor: default;
			}
			table[class=devicewidth] {width: 280px!important;max-width: 280px!important;text-align:center!important;}
			table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}
			table[class="sthide"]{display: none!important;}
			img[class="bigimage"]{width: 260px!important;height:136px!important;}
			img[class="col2img"]{width: 260px!important;height:160px!important;}
			img[class="image-banner"]{width: 280px!important;height:68px!important;}

		}
	</style>

	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
<div class="block">
	<!-- start of header -->
	<table width="100%" bgcolor="#ececec" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="header">
		<tbody>
		<tr>
			<td>
				<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" hlitebg="edit" shadow="edit">
					<tbody>
					<tr>
						<td>
							<!-- logo -->
							<table width="100%" cellpadding="0" cellspacing="0" border="0" align="left" class="devicewidth">
								<tbody>
								<tr>
									<td width="30%">&nbsp;</td>
									<td width="40%" class="logo">
										<center><img src="https://web.pechkin-mail.ru/editor/userfiles/303365/2logo.png" alt="&nbsp;" border="0" style="display:block; border:none; outline:none; text-decoration:none; margin-left: auto; margin-right: auto; margin: 30px auto!important;" st-image="edit" class="logo"></center>
									</td>
									<td width="30%">&nbsp;</td>
								</tr>
								</tbody>
							</table>
							<!-- End of logo -->
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- end of header -->
</div><div class="block">
	<!-- image + text -->
	<table width="100%" bgcolor="#ececec" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="bigimage">
		<tbody>
		<tr>
			<td>
				<table bgcolor="#ffffff" width="580" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
					<tbody>
					<tr>
						<td width="100%" height="20"></td>
					</tr>
					<tr>
						<td>
							<table width="500" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
								<tbody>

									<tr>
										<td width="100%" height="20"></td>
									</tr>
									<tr>
										<td style="font-family: 'PT Sans', Arial, sans-serif; font-size: 14px; color: #6c6c6c; text-align: left; line-height: 20px;" st-content="rightimage-paragraph">

											%CONTENT%

										</td>
									</tr>

								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>

%BLOCK%

<div class="block">
	<!-- start of left image -->
	<table width="100%" bgcolor="#ececec" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="leftimage">
		<tbody>
		<tr>
			<td style = "text-align: center;">
				{{banner}}
			</td>
		</tr>
		</tbody>
	</table>
	<!-- end of left image -->
</div>
<div style = "height: 10px; background-color: #ececec; width: 100%;"></div>

<div class="block">
	<!-- start of left image -->
	<table width="100%" bgcolor="#ececec" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="leftimage">
		<tbody>
		<tr>
			<td>
				<table bgcolor="#ffffff" width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit">
					<tbody>
					<!-- Spacing -->
					<tr>
						<td height="20"></td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td>
							<table width="540" align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
								<tbody>
								<tr>
									<td>
										<!-- start of text content table -->
										<table width="100%" align="left" border="0" cellpadding="0" cellspacing="0" class="devicewidthinner">
											<tbody>
											<!-- image -->
											<tr>
												<td width="100%" height="40" align="center" style="font-family: 'PT Sans', Arial, sans-serif; font-size: 14px; color: #5b5b5b;">
													<img src="http://f.imgsurl.com/303365/3logo2.png">
												</td>
											</tr>
											</tbody>
										</table>
										<!-- mobile spacing -->
										<table align="left" border="0" cellpadding="0" cellspacing="0" class="mobilespacing">
											<tbody>
											<tr>
												<td width="100%" height="15" style="font-size:1px; line-height:1px; mso-line-height-rule: exactly;">&nbsp;</td>
											</tr>
											</tbody>
										</table>
										<!-- mobile spacing -->

									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td height="20"></td>
					</tr>
					<!-- Spacing -->
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- end of left image -->
</div>


<div class="block">
	<!-- Start of preheader -->
	<table width="100%" bgcolor="#ececec" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="postfooter">
		<tbody>
		<tr>
			<td width="100%">
				<table width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth">
					<tbody>
					<!-- Spacing -->
					<tr>
						<td width="100%" height="5"></td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td align="left" valign="middle" style="font-family: 'PT Sans', Arial, sans-serif; font-size: 13px; color: #5b5b5b; padding-left: 50px;" st-content="preheader">
							Это сообщение не требует ответа и сформировано автоматически.<br/>

							Для обращения в службу поддержки zengram.ru  используйте <span style="color:#518ed4;"><a href = "http://zengram.ru/?modal=support" style = "color:#174396;">[онлайн сервис]</a></span>.<br/>
							<br><br>
							© 2014-2015 Zengram. All rights reserved.
						</td>
					</tr>
					<!-- Spacing -->
					<tr>
						<td width="100%" height="5"></td>
					</tr>
					<!-- Spacing -->
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
	<!-- End of preheader -->
</div>

</body></html>


--SEPARATOR--

<!-- 1. Шаблон напоминания о сервисе -->
--service-remind-3--


Приветствую!<br/><br/>
Несколько дней назад Вы зарегистрировались в  Zengram – сервисе автоматического продвижения в Instagram.<br/><br/>
Надеюсь, Вы по достоинству оцените возможности нашего сервиса, т.к сможете увидеть результаты уже в первые часы после запуска своего проекта в Zengram.<br/><br/>
Добавьте свои проекты прямо сейчас и получите 3 дня на продвижение в сервисе Zengram бесплатно!

--SEPARATOR--
<!-- 1. Шаблон напоминания о сервисе -->
--service-remind-5--


Приветствую!<br/><br/>
Мы заметили, что несколько дней назад Вы зарегистрировались в  Zengram – сервисе автоматического продвижения в Instagram, но ещё не добавили свой аккаунт.<br/><br/>
Надеюсь, Вы по достоинству оцените возможности нашего сервиса, т.к сможете увидеть результаты уже в первые часы после запуска своего проекта в Zengram.<br/><br/>
Добавьте свои проекты прямо сейчас и получите 3 дня на продвижение в сервисе Zengram бесплатно!

--SEPARATOR--
<!-- 1.1 Шаблон напоминания о сервисе - футер -->
--service-remind-footer--



<a href="https://zengram.ru/page/price"  target="_blank">
	<img src="http://f.imgsurl.com/303365/email52njiol.png"></a>



--SEPARATOR--
<!-- 2. Шаблон напоминания окончания тестового периода -->
--test-period-end-5--


Приветствую!<br/><br/>
Большое спасибо за то, что выбрали наш сервис.<br/>
Несколько дней назад у Вас закончился тестовый период.<br/>
Надеюсь, Вы будете привлекать подписчиков с помощью Zengram.ru<br/><br/>
Так как Вы новый клиент, хочу сделать Вам небольшой подарок -  скидку 10% на любой пакет в сервисе для эффективного продвижения в Instagram!


--SEPARATOR--
<!-- 2. Шаблон напоминания окончания тестового периода -->
--test-period-end-10--


Приветствую!<br/><br/>
Большое спасибо за то, что выбрали наш сервис.<br/>
Несколько дней назад у Вас закончился тестовый период.<br/>
Надеюсь, Вы будете привлекать подписчиков с помощью Zengram.ru<br/><br/>
Так как Вы новый клиент, хочу сделать Вам небольшой подарок -  скидку 20% на любой пакет в сервисе для эффективного продвижения в Instagram!


--SEPARATOR--
<!-- 2.1 Шаблон напоминания окончания тестового периода - футер -->
--test-period-end-footer-5--


<a href="https://zengram.ru/page/price?discount=trigernaya+10%25"  target="_blank">
	<img src="http://f.imgsurl.com/303365/email6.png"></a>

--SEPARATOR--
<!-- 2.1 Шаблон напоминания окончания тестового периода - футер -->
--test-period-end-footer-10--


<a href="https://zengram.ru/page/price?discount=trigernaya+20%25"  target="_blank">
	<img src="http://f.imgsurl.com/303365/email6.png"></a>



--SEPARATOR--
<!-- 3. Шаблон напоминания оплаты -->
--no-money-no-honey-20--



Приветствую!<br/><br/>
Мы заметили, что Вы уже более 7 дней не получаете новых подписчиков на сервисе Zengram.<br/>
Вы можете прямо сейчас пополнить баланс и возобновить работу вашего аккаунта в Instagram.Подписчики ждут тебя!

--SEPARATOR--
<!-- 3. Шаблон напоминания оплаты -->
--no-money-no-honey-40--


Приветствую!<br/><br/>
Большое спасибо за то, что пользуетесь нашим сервисом.<br/>
Мы заметили, что в последнее время Вы не получаете новых подписчиков в Instagram.<br/>
Так как Вы наш клиент, хотим сделать Вам небольшой подарок -  скидку 10% на любой пакет в сервисе для эффективного продвижения в Instagram!




--SEPARATOR--
<!-- 3.1 Шаблон напоминания оплаты - футер -->
--no-money-no-honey-footer-20--



<a href="https://zengram.ru/page/price"  target="_blank">
	<img src="http://f.imgsurl.com/303365/email7.png"></a>


--SEPARATOR--
<!-- 3.1 Шаблон напоминания оплаты - футер -->
--no-money-no-honey-footer-40--

<a href="https://zengram.ru/page/price?discount=trigernaya+10%25"  target="_blank">
	<img src="http://f.imgsurl.com/303365/email6.png"></a>



--SEPARATOR--
<!-- 3.1 Шаблон напоминания оплаты - блок -->
--no-money-no-honey-add-block-20--



<div class="block">
	<!-- image + text -->
	<table width="100%" bgcolor="#f2f2f2" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="bigimage">
		<tbody>
		<tr>
			<td>
				<table bgcolor="#ffffff" width="580" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidth" modulebg="edit">
					<tbody>
					<tr>
						<td width="100%" height="20"></td>
					</tr>
					<tr>
						<td>
							<table width="500" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
								<tbody>

								<tr>
									<td width="100%" height="20"></td>
								</tr>
								<tr>
									<td style="font-family: 'PT Sans', Arial, sans-serif; font-size: 14px; color: #6c6c6c; text-align: left; line-height: 20px;" st-content="rightimage-paragraph">
										Мы думаем, что Вы уже успели оценить работу нашего сервиса. Не время сбавлять темп!<br/><br/>
										Возникли вопросы, проблемы или есть пожелания?<br/>
										Напишите нам в тех.поддержку, воспользовавшись онлайн сервисом, и мы с удовольствием ответим на Ваши вопросы.

									</td>
								</tr>

								</td>
								</tr>
								<!-- end of content -->
								<!-- Spacing -->
								<tr>
									<td width="100%" height="10"></td>

								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
		</tbody>
	</table>
</div>


--SEPARATOR--
<!-- 3.1 Шаблон напоминания оплаты - блок -->
--no-money-no-honey-add-block-40--

<br>


--SEPARATOR--
<!-- 4. Шаблон напоминания об остановленных проектах -->
--no-active-projects--



Приветствую!<br/><br/>
Напоминаю, что в сервисе Zengram у Вас положительный баланс, и Вы можете прямо сейчас получать новых подписчиков.<br/><br/>
Воспользуйтесь нашим сервисом и запустите в работу свои проекты!



--SEPARATOR--
<!-- 4.1 Шаблон напоминания об остановленных проектах - футер -->
--no-active-projects-footer--



<a href="https://zengram.ru/page/price"  target="_blank">
	<img src="http://f.imgsurl.com/303365/email8ll8fgp.png"></a>



--SEPARATOR--

--header-for-stats--
<div style="font-size:16px;text-align:left;">
	<b style = "font-size: 19px;">Привет, {{display_name}}!</b> <br><br>
	Рады сообщить изменения данных по Вашим <br>инстаграм аккаунтам за прошедшие сутки.<br><br>
	<div style="color:rgb(101,120,134);font-size:14px;line-height:21px;text-align:center;background-color:rgb(238,238,238);padding:5px;margin-top:20px;font-weight:bold;text-transform:uppercase;">
		%DATE%
	</div>
</div>
--SEPARATOR--


--header-for-weekly-stats--
<div style="font-size:16px;text-align:left;">
	<b style = "font-size: 19px;">Привет, {{display_name}}!</b> <br><br>
	Рады сообщить изменения данных по Вашим <br>инстаграм аккаунтам за прошедшую неделю.<br><br>
	<div style="color:rgb(101,120,134);font-size:14px;line-height:21px;text-align:center;background-color:rgb(238,238,238);padding:5px;margin-top:20px;font-weight:bold;text-transform:uppercase;">
		%DATE%
	</div>
</div>
--SEPARATOR--

<!-- Шаблон статистики для отдельного аккаунта-->
--instagram-account-stats--
<table width="100%" bgcolor="#f2f2f2" cellpadding="0" cellspacing="0" border="0" id="backgroundTable" st-sortable="fulltext">
	<tbody>
	<tr>
		<td>
<table bgcolor="%BG%" width="580" cellpadding="0" cellspacing="0" border="0" align="center" class="devicewidth" modulebg="edit">
	<tbody>

	<tr>
		<td>
			<table width="500" align="center" cellspacing="0" cellpadding="0" border="0" class="devicewidthinner">
				<tbody>
				<tr>
					<td style="font-family: 'PT Sans', Arial, sans-serif; font-size: 14px; color: #6c6c6c; text-align: left; line-height: 20px;" st-content="rightimage-paragraph">
<br>
			<div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:left;margin-top:20px; margin-bottom:40px;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tbody>
					<tr>
						<td style="vertical-align:top" align="left" width="280">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
								<tr>
									<td align="left" width="60">
										<img src="{{account_avatar-%ITERATION%}}" style="border:none;font-size:13px;font-weight:bold;min-height:60px;line-height:13px;outline:none;text-decoration:none;text-transform:capitalize;display:inline;width:60px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%;margin-right:10px" height="60" width="60">
									</td>
									<td style="vertical-align:top" align="left">
										<span style="font-weight:normal; font-size:21px">{{ul_%ITERATION%}}</span>
										<br>
										<span style="font-weight:normal; font-size:16px">{{name-%ITERATION%}}</span>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
						<td style="vertical-align:middle" align="right">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tbody>
								<tr>
									<td align="left">
										<div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:center">
											<span style="font-size:26px;font-weight:200;color: #31a04a;">{{account_media-%ITERATION%}}</span><br>
											<span>Медиа</span></div>
									</td>
									<td align="left">
										<div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:center">
											<span style="font-size:26px;font-weight:200;color: #31a04a;">{{account_followers-%ITERATION%}}</span><br>
											<span>Подписчиков</span>
										</div>
									</td>
									<td align="left">
										<div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:center">
											<span style="font-size:26px;font-weight:200;color: #31a04a;">{{account_follows-%ITERATION%}}</span><br>
											<span>Подписок</span>
										</div>
									</td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</div>

			<div style="color:rgb(101,120,134);font-family:arial,sans-serif;font-size:18px;line-height:27px;text-align:left;text-transform:uppercase;">Обзор</div>

			<table style="width:540px;margin-top:10px;border-collapse:collapse;color:#424344;font-family:arial,sans-serif;font-size:14px;line-height:21px;">
				<tbody>
				<tr style = "border-bottom: 2px solid rgb(244,244,244);">
					<td style="width:280px;color: #666;font-weight: bold;"></td>
					<td style="width: 85px;color: #666;font-weight: bold;text-align: right;">Вчера</td>
					<td style="width: 85px;color: #666;font-weight: bold;text-align: right;">Позавчера</td>
					<td style="color: #666;font-weight: bold;text-align: right;">Динамика</td>
				</tr>
				<tr>
					<td style="padding-top:10px;padding-bottom:10px;text-align:left">Кол-во подписчиков</td>
					<td style="padding-top:10px;padding-bottom:10px;text-align: right;">{{follower_count-%ITERATION%}}</td>
					<td style="padding-top:10px;padding-bottom:10px;text-align: right;">{{pre_follower_count-%ITERATION%}}</td>
					<td style="padding-top:10px;padding-bottom:10px;color:{{follower_count_color-%ITERATION%}};text-align: right;">{{evolution_follower-%ITERATION%}}</td>
				</tr>
				<tr>
					<td style="padding-bottom:10px;text-align:left">Размещено медиа</td>
					<td style="padding-bottom:10px;text-align: right;">{{published_media-%ITERATION%}}</td>
					<td style="padding-bottom:10px;text-align: right;">{{pre_published_media-%ITERATION%}}</td>
					<td style="padding-bottom:10px;color:{{published_media_color-%ITERATION%}};text-align: right;">{{evolution_published_media-%ITERATION%}}</td>
				</tr>
				<tr>
					<td style="padding-bottom:10px;text-align:left">Получено лайков</td>
					<td style="padding-bottom:10px;text-align: right;">{{received_likes-%ITERATION%}}</td>
					<td style="padding-bottom:10px;text-align: right;">{{pre_received_likes-%ITERATION%}}</td>
					<td style="padding-bottom:10px;color:{{received_likes_color-%ITERATION%}};text-align: right;">{{evolution_received_likes-%ITERATION%}}</td>
				</tr>
				<tr>
					<td style="padding-bottom:10px;text-align:left">Получено комментариев</td>
					<td style="text-align: right;padding-bottom:10px">{{received_comments-%ITERATION%}}</td>
					<td style="text-align: right;padding-bottom:10px">{{pre_received_comments-%ITERATION%}}</td>
					<td style="text-align: right;padding-bottom:10px;color:{{received_comments_color-%ITERATION%}}">{{evolution_received_comments-%ITERATION%}}</td>
				</tr>
				<tr style = "border-bottom: 2px solid rgb(244,244,244);">
					<td style="padding-bottom:10px;text-align:left">Коэффициент вовлеченности</td>
					<td style="text-align: right;padding-bottom:10px">{{engagement-%ITERATION%}}</td>
					<td style="text-align: right;padding-bottom:10px">{{pre_engagement-%ITERATION%}}</td>
					<td style="text-align: right;padding-bottom:10px">{{evolution_engagement-%ITERATION%}}</td>
				</tr>
				</tbody>
			</table>
			<br /><br />
			<div style="color:rgb(101,120,134);font-family:Helvetica;font-size:14px;line-height:21px;text-align:left;padding-bottom:20px">
				<div style="color:rgb(101,120,134);font-family:Helvetica;font-size:18px;line-height:27px;text-align:left;text-transform:uppercase;">Подписчики</div>
				<table style="border-collapse:collapse;text-align:left;width:540px;margin-top:20px">
					<thead><tr>
						<td style="text-transform:uppercase;font-size:10px;font-weight:bold;text-align:center;">Пришло</td>
						<td style="width:40px;"></td>
						<td style="text-transform:uppercase;font-size:10px;font-weight:bold;text-align:center;">Ушло</td>
						<td style="text-transform:uppercase;font-size:10px;font-weight:bold;padding-right:10px" align="right">
							Популярные подписчики
						</td>
					</tr>
					</thead><tbody>
					<tr>
						<td style="font-size:30px;padding-top:10px;padding-bottom:10px;text-align:left;"><span style="color:rgb(74,186,69)">+ {{came-%ITERATION%}}</span></td>
						<td></td>
						<td style="font-size:30px;padding-top:10px;padding-bottom:10px;text-align:left;"><span style="color:rgb(219,6,69)">- {{gone-%ITERATION%}}</span></td>
						<td style="font-size:30px;padding-top:10px;padding-bottom:10px" align="right">
							<table border="0" cellpadding="5" cellspacing="0">
								<tbody>
								<tr>
									<td style="font-size:30px;padding-top:10px;padding-bottom:10px" align="right" width="40"><a href = "{{top-follower-link1-%ITERATION%}}"><img src="{{top-follower1-%ITERATION%}}" style="border:none;font-size:13px;font-weight:bold;min-height:40px;line-height:13px;outline:none;text-decoration:none;text-transform:capitalize;display:inline;width:40px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%" height="40" width="40"></a></td>
									<td style="font-size:30px;padding-top:10px;padding-bottom:10px" align="right" width="40"><a href = "{{top-follower-link2-%ITERATION%}}"><img src="{{top-follower2-%ITERATION%}}" style="border:none;font-size:13px;font-weight:bold;min-height:40px;line-height:13px;outline:none;text-decoration:none;text-transform:capitalize;display:inline;width:40px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%" height="40" width="40"></a></td>
									<td style="font-size:30px;padding-top:10px;padding-bottom:10px" align="right" width="40"><a href = "{{top-follower-link3-%ITERATION%}}"><img src="{{top-follower3-%ITERATION%}}" style="border:none;font-size:13px;font-weight:bold;min-height:40px;line-height:13px;outline:none;text-decoration:none;text-transform:capitalize;display:inline;width:40px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%" height="40" width="40"></a></td>
									<td style="font-size:30px;padding-top:10px;padding-bottom:10px" align="right" width="40"><a href = "{{top-follower-link4-%ITERATION%}}"><img src="{{top-follower4-%ITERATION%}}" style="border:none;font-size:13px;font-weight:bold;min-height:40px;line-height:13px;outline:none;text-decoration:none;text-transform:capitalize;display:inline;width:40px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%" height="40" width="40"></a></td>
									<td style="font-size:30px;padding-top:10px;padding-bottom:10px" align="right" width="40"><a href = "{{top-follower-link5-%ITERATION%}}"><img src="{{top-follower5-%ITERATION%}}" style="border:none;font-size:13px;font-weight:bold;min-height:40px;line-height:13px;outline:none;text-decoration:none;text-transform:capitalize;display:inline;width:40px;border-top-left-radius:100%;border-top-right-radius:100%;border-bottom-right-radius:100%;border-bottom-left-radius:100%" height="40" width="40"></a></td>
								</tr>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
				</table>
			</div>
		</td>
	</tr>
	</tbody>
</table>

		</td>
	</tr>
	</tbody>
</table>
		</td>
	</tr>
	</tbody>
</table>
--SEPARATOR--

<!-- Шаблон статистики для отдельного аккаунта-->
--stats-block--

Блок <br />

--SEPARATOR--