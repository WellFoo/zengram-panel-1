<?php
use app\components\Counters;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>


<?php if (!Yii::$app->user->isGuest): ?>
	<!-- Modal User password change -->
	<div class="modal zen-modal fade" id="user-settings" tabindex="-1" role="dialog" aria-labelledby="user-settings" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-body">
					<h2 class="modal-title"><?= Yii::t('app', 'Settings of the account') ?></h2>
					<?php
					$formModel = new \app\forms\UserPasswordChangeForm();
					$form = ActiveForm::begin([
						'action' => '/site/change-password',
						'enableAjaxValidation' => true,
						'validationUrl' => Url::to(['site/change-password', 'validate' => 1]),
						'fieldConfig' => [
							'template' => '<div class="col-sm-12">{label}{input}{error}</div>',
							'labelOptions' => ['class' => ''],
						]
					]);
					?>

					<div class="form-horizontal">
						<div id="error" class="alert alert-danger" style="display: none"></div>
						<div id="message" class="alert alert-success" style="display: none"></div>
						<?= $form->field($formModel, 'email')->textInput([
							'class' => 'form-control',
							'placeholder' => '',
							'required' => 'required'
						]) ?>
						<?= $form->field($formModel, 'old_password')->passwordInput([
							'class' => 'form-control',
							'placeholder' => '',
							'required' => 'required'
						]) ?>
						<?= $form->field($formModel, 'new_password')->passwordInput([
							'class' => 'form-control',
							'placeholder' => ''
						]) ?>
						<div class="row">
							<div class="col-xs-6">
								<?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success', 'name' => 'contact-button']) ?>
							</div>
							<div class="col-xs-6">
								<?= Html::a(Yii::t('app', 'Cancel'), '#', ['class' => 'btn btn-danger', 'name' => 'contact-button', 'data' => ['dismiss' => 'modal']]) ?>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

					<?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal User password change end -->
<?php endif; ?>

<!-- Modal alert -->
<div id="alertModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
	<div class="modal-dialog modal-lg accountErrorModal">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<p id="alertModalContent" class="text-center"
			   style="color: #ff6666; margin: 25px auto 45px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;"></p>
		</div>
	</div>
</div>
<!-- Modal alert end -->

<?php if (Yii::$app->language == 'ru'): ?>
	<!-- Modal rek -->
	<div id="rekModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
	     aria-labelledby="myLargeModalLabel"
	     aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="modal-body">
					<h2 class="modal-title">Реквизиты для приема платежей</h2>
					<p id="alertModalContent" class="text-left"
					   style="color: #ff6666; margin: 25px auto 45px auto; font-size: 18px; line-height: 25px; font-family: 'Open Sans', sans-serif;">
						Прием платежей в сервисе zengram.ru через систему Paypal
						осуществляется на счет maxim.parschin2016@yandex.ru.
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal rek end -->
<?php endif; ?>

<!-- Modal Support -->
<div class="modal zen-modal fade" id="support" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?php $form = ActiveForm::begin(['id' => 'contact-form', 'action' => '/site/contact']); ?>
			<div class="modal-body">
				<h4 class="modal-title"><?= Yii::t('main', 'Support') ?></h4>

				<div class="form-horizontal">
					<div id="error" class="alert alert-danger" style="display: none"></div>
					<div id="message" class="alert alert-success" style="display: none"></div>
					<div class="form-group">
						<div class="col-sm-12">
							<label><?= Yii::t('main', 'Subject') ?></label>
							<?= Html::dropDownList('subject', null, \app\models\ContactForm::getSubjects(), [
								'class' => 'form-control subj',
								'prompt' => Yii::t('app', 'Choose one'),
								'required' => 'required',
								'encode' => false
							]) ?>
						</div>
					</div>
					<div class="form-group insa_acc">
						<div class="col-sm-12">
							<label><?= Yii::t('main', 'Instagram Account') ?></label>
							<?= Html::dropDownList('instagram_account', null, \app\models\ContactForm::getAccounts(), [
								'class' => 'form-control',
								'prompt' => Yii::t('app', 'Choose one'),
								'required' => 'required',
								'encode' => false
							]) ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<label><?= Yii::t('main', 'Message') ?></label>
							<?= Html::textarea('text', '', [
								'class' => 'form-control', 'placeholder' => '',
								'required' => 'required', 'rows' => 5
							]) ?>
						</div>
					</div>
					<?= Html::submitButton(Yii::t('app', 'Send'), ['class' => 'btn', 'name' => 'contact-button']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
<!-- Modal Support end -->

<script>
	// js обработчик для формы саппорта. Управляет отображением селекта выбора акка
	$(document).ready(function () {
		$('.insa_acc').hide();
		$('.subj').change(function(){
			var subject_id = $(this).val();
			console.log(subject_id);
			if (subject_id == 1 || subject_id == 7){
				$('.insa_acc').show();
			}else{
				$('.insa_acc').hide();
			}
		});

		<?php if (Yii::$app->request->get('modal', null) == 'support') { ?>
		<?php if (Yii::$app->user->isGuest) { ?>
		$("#loginModal").modal('show');
		<?php } else { ?>
		$("#support").modal('show');
		<?php } ?>
		<?php } ?>

	});
</script>

<!-- Modal Support -->
<div class="modal zen-modal fade" id="support-success" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="modal-body">
				<h4 class="modal-title"><?= Yii::t('main', 'Thanks for feedback!') ?></h4>
				<p class="text-center" style="margin: 15px;"><?= Yii::t('main', 'Your message has been sent to the technical support. We will consider it and respond to you within 24 hours.') ?></p>
				<?= Html::a(Yii::t('app', 'Done'), '#', ['class' => 'btn btn-success', 'name' => 'contact-button', 'data' => ['dismiss' => 'modal']]) ?>
			</div>
		</div>
	</div>
</div>
<!-- Modal Support end -->

<!-- Modal Password Change -->
<div class="modal zen-modal fade" id="password-changed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<div class="modal-body">
				<h4 class="modal-title"><?= Yii::t('main', 'Settings saved!') ?></h4>
				<?= Html::a(Yii::t('app', 'Done'), '#', ['class' => 'btn btn-success', 'name' => 'contact-button', 'data' => ['dismiss' => 'modal']]) ?>
			</div>
		</div>
	</div>
</div>
<!-- Modal Support end -->

<!-- Modal Registration -->
<div id="regModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

			<?php $form = ActiveForm::begin([
				'id' => 'reg-form',
				'options' => [
					'class' => 'reg-form',
					'onSubmit' => Counters::renderAction('register')
				],
				'fieldConfig' => [
					'template' => "{input}{error}",
				],
				'enableAjaxValidation' => true,
				'validationUrl' => ['/site/ajaxregvalidate']
			]);
			$regForm = new \app\models\RegisterForm();
			?>

			<h2 class="reg-title"><?= Yii::t('app', 'Sign up') ?></h2>

			<p class="reg-text"><?= Yii::t('main', 'Is it your first visit?') ?></p>

			<p class="alert alert-danger" style="display: none"></p>

			<?= $form->field($regForm, 'mail')->input('text', ['placeholder' => Yii::t('main', 'Input the email*')]) ?>
			<input type="text" class="hidden">
			<?= $form->field($regForm, 'password')->passwordInput(['placeholder' => Yii::t('main', 'Create the password')]) ?>

			<p class="reg-descr">
				<?= Yii::t('main', '*It is necessary to input the real email. All notifications will be send to it. You can renew the password of your account only with your email.') ?>
			</p>

			<?= Html::submitButton(Yii::t('app', 'Sign up')) ?>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<!-- Modal Registration end -->

<!-- Modal Login -->
<div id="loginModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

			<?php $form = ActiveForm::begin([
				'id' => 'login-form',
				'options' => ['class' => 'login-form'],
				'action' => ['site/login'],
				'fieldConfig' => [
					'template' => "{input}{error}",
				],
				'enableAjaxValidation' => true,
				'validationUrl' => ['/site/ajaxvalidate']
			]);
			$loginModel = new \app\models\LoginForm();
			?>
			<h2 class="login-title"><?= Yii::t('app', 'Log in') ?></h2>

			<p class="login-text"><?= Yii::t('main', 'Input username and password <br>for an access for the control panel') ?></p>

			<p class="alert alert-danger" style="display: none"></p>

			<?= $form->field($loginModel, 'mail')->input('email', ['placeholder' => Yii::t('main', 'Input the username *')]) ?>
			<?= $form->field($loginModel, 'password')->passwordInput(['placeholder' => Yii::t('main', 'Input the password')]) ?>

			<p class="descr-text"><?= Yii::t('main', '*Use your email as a username') ?></p>
			<?= Html::submitButton(Yii::t('app', 'Log in'), ['class' => 'btn btn-success']) ?>
			<?php ActiveForm::end(); ?>
			<a onClick="showPassword()" class="reset-password pull-right" data-toggle="modal" data-target="#passwordModal" title="<?= Yii::t('main', 'forgot password') ?>">
				<i class="fa fa-lock"></i> <span><?= Yii::t('main', 'forgot password') ?></span>
			</a>
		</div>
	</div>
</div>
<!-- Modal Login end -->

<!-- Modal Reset password -->
<div id="passwordModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

			<?php $form = ActiveForm::begin([
				'id' => 'password-form',
				'options' => ['class' => 'login-form'],
			]);
			?>
			<h2 class="login-title"><?= Yii::t('main', 'Password assistance') ?></h2>

			<p class="login-text"><?= Yii::t('main', 'Rest password <br> and send a new one to the specified email') ?></p>

			<p class="alert alert-danger" style="display: none"></p>

			<?= Html::input('email', 'email', '', ['class' => 'form-control', 'placeholder' => 'Email']); ?>

			<div class="row">
				<div class="col-xs-6">
					<button type="submit" class="btn btn-success"><?= Yii::t('app', 'Send') ?></button>
				</div>
				<div class="col-xs-6">
					<button onClick="cancelRemind()" class="btn btn-danger"><?= Yii::t('app', 'Cancel') ?></button>
				</div>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<!-- Modal Reset password end -->
<!-- Modal Success Reset password -->
<div id="passwordSucessModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

			<?php $form = ActiveForm::begin([
				'options' => ['class' => 'login-form'],
			]);
			?>
			<h2 class="login-title"><?= Yii::t('main', 'Password assistance') ?></h2>

			<p class="login-text"><?= Yii::t('main', 'Email with new password sent to <span class="email-result">specified email</span>. If you don&#39;t see this email in your inbox within 15 minutes, look for it in your junk mail folder. If you find it there, please mark it as "Not Spam".') ?></p>
			<button onClick="cancelRemind()" class="btn btn-success" data-dismiss="modal"><?= Yii::t('app', 'Close') ?></button>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>
<!-- Modal Reset password end -->
<script type="text/javascript">
	$(document).ready(function(){
		var anc = window.location.hash.replace("#", "");
		if(anc == "loginModal"){
			$("#loginModal").modal('show');
		}
		$('.header .user-name button').dotdotdot({wrap: 'letter', height: '40px'});
		<?php $show = Yii::$app->request->get('show', null);
		if (!Yii::$app->user->isGuest && $show === 'support') : ?>
		$('#support').modal();
		<?php endif; ?>
		<?php if (!Yii::$app->user->isGuest && $show === 'changed') : ?>
		$('#password-changed').modal();
		<?php endif; ?>
	});
</script>

<footer class="footer">

	<section class="header container">
		<div class="row">

			<i class="toggle-menu fa fa-bars col-xs-3 visible-xs"></i>

			<p class="logo col-xs-3 col-sm-3 col-md-2 col-lg-3">
				<a href="/" title="<?= Yii::t('app', 'Zengram')?>">
					<img src="/img/logo.png" alt="<?=Yii::t('app', 'Zengram')?>"/><?= Html::encode(Yii::$app->params['company']) ?>
				</a>
			</p>

			<?php if (!Yii::$app->user->isGuest) {?>
			<nav class="nav-top col-xs-12 col-sm-8 col-md-7 col-lg-6 hidden-xs">
				<?php }else{ ?>
				<nav class="nav-top col-xs-9 col-sm-7 col-md-7 col-lg-6 hidden-xs">
					<?php }?>
					<?php
					foreach ($pages as $page) {
						/** @var \app\models\Page $page */
						echo empty($page->linkTitle) ? Html::a(Yii::t('app', $page->title), ['/page/' . $page->alias]):
							Html::a(Yii::t('app', $page->title), ['/page/' . $page->alias], ['title' => $page->linkTitle]);
					}
					echo Yii::$app->language !== 'en' ? Html::a(Yii::t('app', 'Blog'), ['/site/blog']) :
						Html::a(Yii::t('app', 'Blog'), ['/site/blog'], ['title' => Yii::t('app', 'Our Blog')]);
					if (!Yii::$app->user->isGuest) {
						echo Html::a(Yii::t('app', 'Support'), ['#support'], [
							'data-toggle' => 'modal', 'data-target' => '#support'
						]);
					} else {
						echo Html::a(Yii::t('app', 'Contacts'), ['/page/contacts'], ['title' => Yii::t('app', 'Contacts')]);
					}
					?>

					<?php if (Yii::$app->language === 'ru') { ?>
						<div class="site-service">
							<div class="service-dropdown">
								<a href="/instashpion">Кого лайкал твой друг</a>
								<a href="/instagram">Привлечение подписчиков</a>
							</div>
							Услуги
						</div>
					<?php } ?>

					<?php if (Yii::$app->language == 'ru'): ?>
						<p style="color:#000;line-height:16px;margin:10px 0px;">г. Москва, ул.Бауманская д.11, оф.6. Тел. +7 (499) 703-17-16</p>
					<?php endif; ?>

					<p style="color:#000;line-height:16px;"><?= Html::mailto(Yii::$app->params['adminEmail'], Yii::$app->params['adminEmail'], ['style' => 'color: #000;']) ?></p>
					<?php if (Yii::$app->language == 'ru'): ?>
						<!-- <a href="#oferta">Оферта</a>-->
						<!-- <a href="#rek" data-toggle="modal" data-target="#rekModal">Реквизиты</a> -->
						<a href="/download/Реквизиты.pdf" target="_blank">Реквизиты</a>
						<a href="/download/Инструкция по раскрутке в Instagram.pdf" target="_blank">Инструкция</a>
					<?php endif; ?>
					<div class="clearfix"></div>
					<a href="#" style="font-family: 'Open Sans',Helvetica,Arial,sans-serif;color: #a6a6a4;font-weight: 400; padding-right:0px;">© 2014-<?= date('Y') ?> <?=Yii::t('app', 'Zengram')?>. All rights reserved.</a> &nbsp;|&nbsp;
					<?= Html::a(Yii::t('views', 'Sitemap'), ['/page/sitemap'], [
						'title' => Yii::t('views', 'Sitemap'),
						'style' => "font-family: 'Open Sans',Helvetica,Arial,sans-serif;color: #a6a6a4;font-weight: 400;"
					])?>
					<?= Yii::$app->language === 'en' ? Html::a('|&nbsp;' . Yii::t('views', 'Contacts'), ['/page/contacts'], ['style' => 'color: #DDD; margin-left: -10px;']) : '' ?>
				</nav>
				<nav class="navbar-right nav-top col-lg-3 hidden-xs socialfoot" style="text-align: left;">
					<?php if (Yii::$app->language === 'ru') { ?>
						<div class="pull-left mysocicons">

							<a style="padding-right:0; padding-left:0px; !important;" href="https://vk.com/zengram" target="_blank" title="vk.com">
								<img src="/img/icons/vkico.png" alt="vk">
							</a>
							<a style="padding-right:4px; padding-left:0px; !important;" href="https://www.facebook.com/zengramru/" target="_blank" title="Facebook">
								<img src="/img/icons/fbico.png" alt="Facebook">
							</a>
							<div class="clearfix"></div>

							<img src="/images/pay/pay.jpg" alt="Pay" style="display: block; padding-top:15px;" />
						</div>
					<?php } ?>

					<?= '' /* Html::mailto(Yii::$app->params['adminEmail']) */ ?>
				</nav>
				<?php if (!Yii::$app->user->isGuest) {?>
				<nav class="col-xs-12 col-sm-8 col-md-9 col-lg-6 hidden-xs" style="text-align: center; line-height: 34px; margin-top: 0; color: #a6a6a4;">
					<?php }else{ ?>
					<nav class="col-xs-9 col-sm-7 col-md-8 col-lg-6 hidden-xs" style="text-align: center; line-height: 34px; margin-top: 0; color: #a6a6a4;">
						<?php }?>
						<!--
					<a href="#" style="font-family: 'Open Sans',Helvetica,Arial,sans-serif;color: #a6a6a4;font-weight: 400;">© 2014-<?= date('Y') ?> Zengram. All rights reserved.</a>
					|
					<?= Html::a(Yii::t('views', 'Sitemap'), ['/page/sitemap'], [
							'title' => Yii::t('views', 'Sitemap'),
							'style' => "font-family: 'Open Sans',Helvetica,Arial,sans-serif;color: #a6a6a4;font-weight: 400;"
						])?>
					-->
					</nav>
		</div>
	</section>
</footer>