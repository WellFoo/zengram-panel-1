<?php
/**
 * @var \yii\web\View $this
 * @var \app\models\Post $post
 */
use yii\helpers\Html;

$this->title = $post->title;
$this->registerMetaTag([
	'name' => 'description',
	'content' => $post->title
], 'description');
?>
<style>
	.blog-content {
		line-height: 1.5em;
	}
	.blog-content p {
		margin: 10px 0;
	}
	.blog-content ul,
	.blog-content ol {
		font-size: 18px;
		list-style: disc;
		margin-left: 30px;
	}
	.blog-content ol {
		list-style: decimal;
	}
	.blog-content li {
		padding: 5px;
	}
</style>

<div class="blog">
	<h1><?= $post->title ?></h1>

	<div class="row blog-wrap">
		<div class="col-sm-2 col-md-1">
			<div class="blog-date">
				<div class="day"><?= \Yii::$app->formatter->asDate($post->date, 'dd'); ?></div>
				<div class="month"><?= \Yii::$app->formatter->asDate($post->date, 'MMM'); ?></div>
				<div class="year"><?= \Yii::$app->formatter->asDate($post->date, 'yyyy'); ?></div>
			</div>
		</div>
		<div class="col-sm-10 col-md-11">
			<div class="col-sm-10 col-md-11">
				<div class="blog-post">
					<div class="blog-content">
						<?php if ($post->image): ?>
							<div class="blog-image col-md-12">
								<?= Html::img($post->image, ['alt' => $post->title]) ?>
							</div>
						<?php endif; ?>
						<?= $post->text ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
