<?php
/**
 * @var \yii\web\View $this
 * @var string        $file
 */

Yii::$app->response->format = \yii\web\Response::FORMAT_RAW;
Yii::$app->response->headers->set('Content-Type', 'text/javascript');

$messages = [];
if (Yii::$app->language !== Yii::$app->sourceLanguage) {
	try {
		$messages = include $file;
	} catch (Exception $e) {}
}
?>
window.Zengram = {
	t: function (string, replacements) {
		var result;

		string = String(string);

		if (this.messages.hasOwnProperty(string)) {
			result = this.messages[string];
		} else {
			result = string;
		}

		if (replacements instanceof Object) {
			for (var key in replacements) {
				if (replacements.hasOwnProperty(key)) {
					result = result.replace('{' + key + '}', replacements[key]);
				}
			}
		}
		return result;
	},
	messages: <?= json_encode($messages) ?>
};