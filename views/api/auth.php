<?php
/**
 * @var $this \yii\web\View
 * @var $is_logged_in boolean
 * @var $user_id integer
 * @var $mail string
 * @var $token string
 */
use yii\helpers\Url;

?>
window.__zengram_user = {
<?php if ($is_logged_in) { ?>
	user_id: <?= $user_id ?>,
	mail: '<?= $mail ?>',
	token: '<?= $token ?>',
<?php } ?>
	is_logged_in: <?= (($is_logged_in) ? 'true' : 'false') ?>

};

var Zengram = {
	isLoggedIn: function() {
		return window.__zengram_user.is_logged_in;
	},

	isGuest: function() {
		return (!this.isLoggedIn());
	},

	getInfo: function () {
		return {
			id: window.__zengram_user.user_id,
			mail: window.__zengram_user.mail
		}
	},

	getToken: function () {
		return window.__zengram_user.token;
	},

	login: function(login, password, callbacks) {
		$.post('<?= Url::to(['/user/auth'], true) ?>', {
			login: login,
			password: password
		}, function (response) {
			if (response.status == 'ok') {
				var script = document.createElement('script');
				script.src = '<?= Url::to(['/site/login'], true) ?>?mail=' + login + '&password=' + password + '&noredir=1';
				if (callbacks && callbacks.success) {
					script.onload = function () {
						callbacks.success(response.user);
					};
				}
				document.getElementsByTagName('head')[0].appendChild(script);

				window.__zengram_user = {
					is_logged_in: true,
					user_id: response.user.id,
					mail: response.user.mail,
					token: response.token
				};
			} else {
				if (callbacks && callbacks.error) {
					callbacks.error(response.message);
				}
			}
			if (callbacks && callbacks.always) {
				callbacks.always(response.message);
			}
		}, 'JSON');
	},

	register: function(login, password, callbacks) {
		$.post('<?= Url::to(['/user/register'], true) ?>', {
			login: login,
			password: password
		}, function (response) {
			if (response.status == 'ok') {
				Zengram.login(login, password, callbacks);
			} else {
				if (callbacks && callbacks.error) {
					callbacks.error(response.message);
				}
			}
			if (callbacks && callbacks.always) {
				callbacks.always(response.message);
			}
		}, 'JSON');
	},

	logout: function(callback) {
		var script = document.createElement('script');
		script.src = '<?= Url::to(['/site/logout'], true) ?>?noredir=1';
		script.onload = function () {
			callback();
		};
		document.getElementsByTagName('head')[0].appendChild(script);
	}
}

