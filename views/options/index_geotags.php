<?php
/**
 * @var $this yii\web\View
 * @var $account app\models\Account
 * @var $form yii\bootstrap\ActiveForm
 */

use app\models\Options;
use yii\helpers\Url;
use yii\web\View;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->registerJsFile('@web/js/lib/can.custom.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/bs3paginator.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/bs3radio.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/lib/bootstrap3-typeahead.min.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/account.js', ['position' => View::POS_BEGIN]);

$this->registerJsFile(
	'//maps.googleapis.com/maps/api/js?libraries=geometry&key=' . Yii::$app->params['google_api_key'],
	['position' => View::POS_BEGIN]
);
$this->registerJsFile('@web/js/markerclusterer.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/place.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/geotags.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/options-geotags.js', ['position' => View::POS_BEGIN]);

$this->registerJsFile('@web/js/place.js', ['position' => View::POS_BEGIN]);
$this->registerJsFile('@web/js/jquery.autosize.js', ['position' => View::POS_BEGIN]);

$this->registerJsFile('@web/js/lib/enjoyhint.min.js', ['position' => View::POS_BEGIN]);
$this->registerCssFile('@web/css/enjoyhint.css', ['position' => View::POS_BEGIN]);

$this->title = Yii::t('views', 'Account {login} options', ['login' => $account->login]);
//$this->params['breadcrumbs'][] = $this->title;

$status = $account->getStatus();

$speeds = [Yii::t('views', 'slow'), Yii::t('views', 'medium'), Yii::t('views', 'fast')];
?>
<script type="text/javascript">
	var mapsApiKey = '<?= Yii::$app->params['yandex_maps_api_key'] ?>';
	var googleApiKey = '<?= Yii::$app->params['google_api_key'] ?>';
	$(function(){
		$('.animated').autosize();
	});
</script>

<div class="settings-container">

	<script>speeds = ['<?= implode("','", $speeds) ?>'];</script>

	<!-- Setting panels list -->
	<section class="row">

		<?= $this->render('//account/_account_inside', [
			'speeds'  => $speeds,
			'account' => $account,
			'status'  => $status,
		]); ?>

		<div class="hidden-xs hidden-sm col-md-5">
			<p class="set-title" style="padding-left: 10px;"><?= Yii::t('views', 'Main settings') ?></p>
		</div>

		<div class="col-xs-12 col-md-7 group-action-btn visible-lg account-controls" style="margin-top: 25px;">
			<?= Html::a(Yii::t('views', 'Delete account'), ['options/delete', 'id' => $account->id], [
				'class' => 'btn btn-danger pull-right',
				'onclick' => '$(this).addClass(\'disabled\').addClass(\'loading\');'
			]); ?>
			<?= Html::a(Yii::t('views', 'Reset settings'), ['options/reset', 'id' => $account->id], ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 10px;']); ?>
			<?= Html::a(Yii::t('views', 'Save settings'), ['/'], ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 10px;']); ?>
		</div>

		<div class="clearfix"></div>

		<?php
		$form = ActiveForm::begin([
			'id'          => 'options-form',
			//'options' => ['class' => 'form-horizontal'],
			'fieldConfig' => [
				//'template' => "<div class=\"col-xs-6\">{label}</div><div class=\"col-xs-2\">{input}</div><div class=\"col-lg-8\">{error}</div>",
				//'labelOptions' => ['class' => 'control-label', 'style' => 'text-align: left;'],
			],
		]); ?>

		<!-- New block -->
		<div class="panel panel-settings">

			<div class="panel-header container-fluid">
				<div class="row">
					<div class="col-xs-3 col-sm-2"><?= Yii::t('views', 'Actions') ?></div>
					<div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							<?= Yii::t('views', 'Select the actions to be performed by Zengram') ?>
						</span>
						<span class="hidden-md hidden-lg">
							<?= Yii::t('views', 'Possible actions') ?>
							<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Possible actions') ?>"
							   data-content="<?= Yii::t('views', 'Select the actions to be performed by Zengram') ?>"></i>
						</span>
					</div>
				</div>
			</div>

			<div class="panel-body" id="actions">

				<div class="row options-row">
					<!-- Likes checkbox group -->
					<div class="col-sm-4">

						<div class="panel panel-sub<?= !$account->options->likes ? ' disable' : '' ?>">
							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-likes" class="projectOption"
											<?= $account->options->likes ? 'checked=""' : '' ?>
											   data-account="<?= $account->id; ?>" data-option="likes"
											   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-heart&quot;></i>"
											   data-off="<i class=&quot;fa fa-heart&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-likes"><?= Yii::t('views', 'Put likes') ?></label>
										<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Put likes') ?>"
										   data-content="<?= Yii::t('views', 'If this option is enabled zengram will put likes for accounts that matches the search criteria') ?>"></i>
									</div>
								</div>
							</div>

							<div class="panel-body" style="padding: 5px 15px;">
								<?= $form->field($account->options, 'likeMutuals', [
									'checkboxTemplate' => '{input} {beginLabel}{labelTitle} {beginWrapper}{endWrapper}{endLabel}',
									'enableError'      => false,
									'wrapperOptions'   => [
										'tag'   => 'i',
										'class' => 'fa fa-question-circle description',
										'data'  => [
											'toggle'  => 'popover',
											'title'   => Yii::t('views', 'Mutual likes'),
											'content' => Yii::t('views', 'If someone likes your account zengram will put like back one of the last photo of that account')
										]
									]
								])->checkbox([
									'id'    => 'option-like-some',
									'class' => 'projectOption',
									'data'  => ['account' => $account->id, 'option' => 'likeMutuals'],
									'label' => Yii::t('views', 'Mutual likes')
								]); ?>

								<?= $form->field($account->options, 'likeFollowers', [
									'checkboxTemplate' => '{input} {beginLabel}{labelTitle} {beginWrapper}{endWrapper}{endLabel}',
									'enableError'      => false,
									'wrapperOptions'   => [
										'tag'   => 'i',
										'class' => 'fa fa-question-circle description',
										'data'  => [
											'toggle'  => 'popover',
											'title'   => Yii::t('views', 'Like own followers'),
											'content' => Yii::t('views', 'Zengram will put likes to the new photos of your followers')
										]
									]
								])->checkbox([
									'id'    => 'option-like-another',
									'class' => 'projectOption',
									'data'  => ['account' => $account->id, 'option' => 'likeFollowers'],
									'label' => Yii::t('views', 'Like own followers')
								]); ?>
							</div>
						</div>
					</div>
					<!-- Likes checkbox group end -->

					<div class="col-sm-4">
						<!-- Follow -->
						<div class="panel panel-sub<?= !$account->options->follow ? ' disable' : '' ?>">
							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-follow" class="projectOption"
										       data-account="<?= $account->id; ?>" data-option="follow"
											<?= $account->options->follow ? 'checked=""' : '' ?>
											   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-check&quot;></i>"
											   data-off="<i class=&quot;fa fa-check&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-follow"><?= Yii::t('views', 'Follows') ?></label>
										<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Follows') ?>"
										   data-content="<?= Yii::t('views', 'If this option is enabled zengram will follows accounts that matches the search criteria') ?>"></i>
									</div>
								</div>
							</div>
						</div>
						<!-- Follow end -->

						<div class="hidden-xs" style="height: 20px;"></div>

						<!-- Comments -->
						<div class="panel panel-sub<?= !$account->options->comment ? ' disable' : '' ?>">
							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-comment" class="projectOption"
										       data-account="<?= $account->id; ?>" data-option="comment"
											<?= $account->options->comment ? 'checked=""' : '' ?>
											   data-toggle="toggle" data-style="ios"
											   data-on="<i class=&quot;fa fa-comment&quot;></i>"
											   data-off="<i class=&quot;fa fa-comment&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-comment"><?= Yii::t('views', 'Put comments') ?></label>
										<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Put comments') ?>"
										   data-content="<?= Yii::t('views', 'If this option is enabled zengram will put comments to accounts that matches the search criteria. You can edit list of available comments below.') ?>"></i>
									</div>
								</div>
							</div>
						</div>
						<!-- Comments end -->
					</div>

					<!-- Unfollow checkbox group -->
					<div class="col-sm-4">
						<div class="panel panel-sub<?= !$account->options->unfollow ? ' disable' : '' ?>">

							<div class="panel-header container-fluid">
								<div class="row">
									<div class="option-switch">
										<input type="checkbox" id="options-unfollow" class="projectOption"
										       data-account="<?= $account->id; ?>" data-option="unfollow"
											<?= $account->options->unfollow ? 'checked=""' : '' ?>
											   data-toggle="toggle" data-style="ios" data-on="<i class=&quot;fa fa-times&quot;></i>"
											   data-off="<i class=&quot;fa fa-times&quot;></i>" data-onstyle="setting-on">
									</div>
									<div class="option-label">
										<label for="options-unfollow"><?= Yii::t('views', 'Unfollows') ?></label>
										<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Unfollows') ?>"
										   data-content="<?= Yii::t('views', 'If this option is enabled zengram will unfollow from accounts that you follow') ?>"></i>
									</div>
								</div>
							</div>

							<div class="panel-body" style="padding: 5px 15px;">
								<?= $form->field($account->options, 'mutual', [
									'checkboxTemplate' => '{input} {beginLabel}{labelTitle} {beginWrapper}{endWrapper}{endLabel}',
									'enableError'      => false,
									'wrapperOptions'   => [
										'tag'   => 'i',
										'class' => 'fa fa-question-circle description',
										'data'  => [
											'toggle'  => 'popover',
											'title'   => Yii::t('views', 'Keep mutual'),
											'content' => $account->options->getAttributeLabel('mutual')
										]
									]
								])->checkbox([
									'id'    => 'option-unfollow',
									'class' => 'projectOption',
									'data'  => ['account' => $account->id, 'option' => 'mutual'],
									'label' => Yii::t('views', 'Keep mutual')
								]); ?>

								<?= $form->field($account->options, 'autounfollow', [
									'checkboxTemplate' => '{input} {beginLabel}{labelTitle} {beginWrapper}{endWrapper}{endLabel}',
									'enableError'      => false,
									'wrapperOptions'   => [
										'tag'   => 'i',
										'class' => 'fa fa-question-circle description',
										'data'  => [
											'toggle'  => 'popover',
											'title'   => Yii::t('views', 'Autounfollows'),
											'content' => $account->options->getAttributeLabel('autounfollow')
										]
									]
								])->checkbox([
									'id'    => 'option-autounfollow',
									'class' => 'projectOption',
									'data'  => ['account' => $account->id, 'option' => 'autounfollow'],
									'label' => Yii::t('views', 'Autounfollows')
								]); ?>
							</div>
						</div>
					</div>
					<!-- Unfollow checkbox group end -->
				</div>

			</div><!-- End of .panel-body -->

		</div><!-- End of .panel-settings -->

		<div class="panel panel-settings" id="targeting-panel" data-url="<?=Url::to(['options/search-by/' . $account->id])?>">

			<div class="panel-header container-fluid">
				<div class="row">
					<div class="col-xs-3 col-sm-2"><?= Yii::t('views', 'Targeting') ?></div>
					<div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							<?= Yii::t('views', 'Select how and where zengram will search new followers') ?>
						</span>
						<span class="hidden-md hidden-lg">
							<?= Yii::t('views', 'Followers search') ?>
							<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Targeting') ?>"
							   data-content="<?= Yii::t('views', 'Select how and where zengram will search new followers') ?>"></i>
						</span>
					</div>
				</div>
			</div>

			<div id="tergeting-panel-container" class="panel-body <?= $account->options->use_geotags ? 'extended' : '' ?>">
				<div class="row">
					<?= $this->render('targeting-geotags', [
						'account' => $account,
						'form' => $form
					]); ?>
				</div>
			</div>
		</div><!-- End of .panel-settings -->

		<?php if ($account->options->auto_fill) {
			/* Настройка прокачки аккаунта */
			echo $this->render('auto_fill', ['account' => $account]);
		} ?>

		<!-- Comments panel -->
		<div class="panel panel-settings">

			<div class="panel-header container-fluid">
				<div class="row">
					<div class="col-xs-3 col-sm-2"><?= Yii::t('views', 'Comments') ?></div>
					<div class="col-xs-9 col-sm-10">
						<span class="visible-md visible-lg">
							<?= Yii::t('views', 'Select comments that zengram will put, or add your own') ?>
						</span>
						<span class="hidden-md hidden-lg">
							<?= Yii::t('views', 'Comments texts') ?>
								<i class="fa fa-question-circle description" data-toggle="popover" data-title="<?= Yii::t('views', 'Comments texts') ?>"
								   data-content="<?= Yii::t('views', 'Select comments that zengram will put, or add your own') ?>"></i>
						</span>
					</div>
				</div>
			</div>

			<div class="panel-body">

				<div class="acc-tog-set" style="margin: -10px 0 -15px 20px; padding: 0; border: none;">
					<label style="font-size: 16px;">
						<?= $form->field($account->options, 'firmcomments')->checkbox([
							'checked'      => $account->options->firmcomments,
							'id'           => 'firmCommentsCheckbox',
							'data-toggle'  => 'toggle',
							'data-style'   => 'ios',
							'data-on'      => '<i class="fa fa-check"></i>',
							'data-off'     => '<i class="fa fa-check"></i>',
							'data-onstyle' => 'success',
						]); ?>
					</label>
				</div>

				<div class="clearfix divide"></div>

				<div class="row">
					<label for="commentText" class="panel-sub-header col-md-2"><?= Yii::t('views', 'Comments') ?>:</label>

					<div class="col-md-10">

						<input id="commentId" type="hidden">
						<div id="commentInputHolder" class="input-group">
							<textarea id="commentText" type="search" class="form-control animated"
							       data-url="<?=Url::to(['options/comment/' . $account->id])?>"></textarea>
							<input id="commentId" type="hidden">

							<span class="input-group-btn">
								<button id="commentEditBtn" class="btn btn-warning" style="display: none;">
									<span class="glyphicon glyphicon-edit" title="<?= Yii::t('app', 'Edit') ?>"></span>
								</button>
								<button id="commentAddBtn" class="btn btn-success">
									<span class="glyphicon glyphicon-plus" title="<?= Yii::t('app', 'Add') ?>"></span>
								</button>
								<button id="commentClearBtn" class="btn btn-default">
									<span class="glyphicon glyphicon-ban-circle" title="<?= Yii::t('app', 'Cancel') ?>"></span>
								</button>
							</span>

						</div>
					</div>
				</div>

				<div class="row">
					<div class="panel-sub-header fs-16 col-md-2"><?= Yii::t('views', 'Leave the desired<br/>or add your own:') ?></div>
					<div class="col-md-10 comments-list" id="commentsList"></div>
					<div id="commentTemplate" class="balloon" style="display: none;">
							<span class="button-holder">
								<?php /* <span class="edit glyphicon glyphicon-pencil" title="<?= Yii::t('views', 'Edit') ?>"></span> */ ?>
								<span class="delete glyphicon glyphicon-trash" title="<?= Yii::t('views', 'Remove') ?>"></span>
							</span>
						<span class="content"></span>
					</div>
				</div>

				<div class="row hidden-xs" style="margin-top: 15px;">
					<div class="col-md-10 col-md-offset-2">
						<div class="pull-right">
							<span><?= Yii::t('views', 'Show by') ?>: </span><span id="commentsCountSwitch"></span>
						</div>
						<div id="commentsPagination" data-count="<?=$account->getCommentsCount();?>"></div>
					</div>
				</div>

				<div class="group-action-btn text-center visible-xs">
					<button type="button" id="commentsShowMore" class="btn btn-warning btn-grey"><?= Yii::t('views', 'Show next 3') ?></button>
				</div>
				<span class="clearfix"></span>

				<?php
				/** @var \app\models\Users $user */
				$user = Yii::$app->user->identity;
				if ($user->comment_popular) {
					echo Html::a(Yii::t('views', 'Propagate comments'), [
						'options/propagate-comments', 'id' => $account->id
					], [
						'class' => 'btn btn-primary',
						'style' => 'margin-top: 10px;'
					]);
				}
				?>

			</div><!-- End of .panel-body -->
		</div><!-- End of .panel-settings -->
		<!-- Comments panel end -->

		<div class="group-action-btn hidden-xs account-controls">
			<?= Html::a(Yii::t('views', 'Delete account'), ['options/delete', 'id' => $account->id], ['class' => 'btn btn-danger pull-right']); ?>
			<?= Html::a(Yii::t('views', 'Reset settings'), ['options/reset', 'id' => $account->id], ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 10px;']); ?>
			<?= Html::a(Yii::t('views', 'Save settings'), ['/'], ['class' => 'btn btn-success pull-right', 'style' => 'margin-right: 10px;']); ?>
		</div>

		<div class="group-action-btn visible-xs">
			<div class="col-xs-4">
				<div class="row">
					<?= Html::a(Yii::t('views', 'Save settings'), ['/'], ['class' => 'btn btn-success']) ?>
				</div>
			</div>
			<div class="col-xs-4 p05">
				<?= Html::a(Yii::t('views', 'Reset settings'), ['options/reset', 'id' => $account->id], ['class' => 'btn btn-warning']) ?>
			</div>
			<div class="col-xs-4">
				<div class="row">
					<?= Html::a(Yii::t('views', 'Delete account'), ['options/delete', 'id' => $account->id], ['class' => 'btn btn-danger']) ?>
				</div>
			</div>
		</div>

		<?php ActiveForm::end(); ?>
	</section>    <!-- Setting panels list end -->
	<script>
		$('input[name="Options[unfollow]"]').change(function () {
			$('.function-status').toggleClass('disable');
		});
		$(document).ready(function () {
			$('[data-toggle="popover"]').popover(
				{
					placement: 'top',
					trigger: 'hover',
					container: 'body'
				}
			);

			$('.helper').popover({placement: 'top'}).unbind('click').on('click', function (e) {
				$(this).popover('toggle');
			});
			<?php if ($account->options->getFirstErrors()): ?>
			$('#modalAlert').modal('show');
			<?php endif; ?>
		});
	</script>


	<?php
	echo $this->render('@app/views/account/_modals.php');
	?>


<?php if ($account->options->getFirstError('comments')): ?>
	<div class="modal fade" id="modalAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	     aria-hidden="true">
		<div class="modal-dialog modal-lg accountErrorModal">
			<div class="modal-content">
				<?php foreach ($account->options->getFirstErrors() as $attributeError): ?>
					<h2 class="text-center" style="margin: 0; line-height: 1.428571429; font-size: 18px !important; color: #ff6666;">
						<?= $attributeError ?></h2>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>
</div>