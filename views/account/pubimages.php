<?php
/* @var $queues array */
use yii\helpers\Html;

/* @var $projects \app\models\Account[] */
?>
<div class="clear"></div>
<div class="site-admin">
	<table class="table">
		<thead>
		<tr>
			<th>ID</th>
			<th><?= Yii::t('views', 'Photo') ?></th>
			<th><?= Yii::t('views', 'Status') ?></th>
			<th><?= Yii::t('views', 'Description') ?></th>
			<th><?= Yii::t('views', 'Log') ?></th>
			<th><?= Yii::t('views', 'Pauses') ?></th>
			<th><?= Yii::t('views', 'Add date') ?></th>
			<th><?= Yii::t('views', 'Number of projects') ?></th>
			<th><?= Yii::t('views', 'Error projects') ?></th>
			<th><?= Yii::t('views', 'Success projects') ?></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($queues as $queue): ?>
			<?php $users = json_decode($queue['users'], true); ?>
			<tr>
				<td><?= $queue['id']; ?></td>
				<td><img style="max-width: 250px;" class="img img-thumbnail"
				         src="data:image/jpg;base64,<?= ($queue['image']) ?>"></td>
				<td><?= $queue['status']; ?></td>
				<td><?= $queue['comment']; ?></td>
				<td><?= Html::a('Лог', [
						'account/pubimages', 'id' => $queue['id']
					], ['class' => 'btnew btn btn-gray col-md-6 col-sm-6 col-xs-6 btn-comment']
					) ?></td>
				<td><?= $queue['pause_from'].' - '. $queue['pause_to']; ?></td>
				<td><?= $queue['added']; ?></td>
				<td><?= count($users) ?></td>
				<td><?= $queue['fails'] ?></td>
				<td><?= $queue['success'] ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
