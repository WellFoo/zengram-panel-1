<?php
/* @var $queues array */
use yii\helpers\Html;

/* @var $projects \app\models\Account[] */
/* @var $logs [] */
?>
<div class="clear"></div>
<div class="site-admin">
	<table class="table">
		<thead>
		<tr>
			<th>ID</th>
			<th><?= Yii::t('views', 'Account') ?></th>
			<th><?= Yii::t('views', 'Text') ?></th>
			<th><?= Yii::t('views', 'Error?') ?></th>
		</tr>
		</thead>
		<tbody>
		<?php foreach ($logs as $log): ?>
			<tr>
				<td><?= $log['id'] ?></td>
				<td><?= $log['account_id'] ?></td>
				<td><?= $log['text'] ?></td>
				<td><?= $log['stop'] ? Yii::t('views', 'Yes') : Yii::t('views', 'No') ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
</div>
<?php echo Html::a(
	Yii::t('views', 'Back'),
	['account/pubimages'],
	['class' => 'btn btn-success', ]
); ?>
