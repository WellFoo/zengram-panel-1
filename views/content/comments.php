<?php
/* @var app\models\Account $account */
/* @var string $media_id */
?>
<?php if (!empty($account->medias['max_id'])) : ?>
	<div class="panel-more text-center col-md-12 col-sm-12 col-xs-12">
		<button class="btn btn-success" onclick="loadMoreComments(<?= $account->id; ?>,'<?= $account->medias['data'] ?>','<?= $media_id ?>')">
			<?= Yii::t('views', 'Load more') ?>
		</button>
	</div>
<?php endif; ?>
<?php if (!empty($account->mediaComments['data'])) : ?>
	<?php if (!empty($account->mediaComments['more'])) : ?>
		<div class="panel-more-comments text-center modal-content-comment instagram-modal-comment">
			<button class="btn btn-success"
			        onclick="loadMoreComments('<?= $account->id; ?>', '<?= $media_id ?>','<?= $account->mediaComments['max_id']; ?>')">
				<?= Yii::t('views', 'Load more') ?>
			</button>
			<p style="display: none" class="text-center loading-more-comments"><img
					src="/img/ajax-loader.gif"></p>
			<div class="clearfix"></div>
		</div>
	<?php endif; ?>
	<?php foreach ($account->mediaComments['data'] as $comment): ?>
		<div class="modal-content-comment instagram-modal-comment" data-comment-id="<?= $comment['pk'] ?>">
			<div class="delete">
				<span class="glyphicon glyphicon-trash" aria-hidden="true" title="<?= Yii::t('views', 'Remove comment') ?>"></span>
			</div>
			<div class="comment-user col-xs-3 col-md-3 col-sm-3">
				<img class="img img-rounded" src="<?= str_replace('http://scontent.cdninstagram.com', 'https://scontent.cdninstagram.com', $comment['user']['profile_pic_url']) ?>">
				<h5 class="cursor-pointer commenter-login" title="@<?= $comment['user']['username'] ?>"
				    onclick="quoteUser('<?= $media_id ?>', '@<?= $comment['user']['username'] ?>')">
					@<?= $comment['user']['username'] ?></h5>
			</div>
			<div class="comment-text col-xs-9 col-md-9 col-sm-9"><?= $comment['text'] ?></div>
			<div class="clearfix"></div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>

<?php if (!empty($account->medias['error'])) : ?>
	<div class="alert alert-danger"><?= Yii::t('views', '') ?> <?= $account->medias['error'] ?></div>
<?php endif; ?>
