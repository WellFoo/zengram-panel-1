<?php
use app\components\Counters;
?>
<div id="currencies">
	<div class="clearfix"></div>

	<button type="button" class="pay-button paypal-submit"><img src="/img/currencies/Paypal.gif"></button>
	<button type="submit" class="pay-button 2co-submit"><img src="/img/currencies/BankCard.png"></button>
</div>
<script>
$(document).ready(function () {
	var $refundPolicyModal = $('#refund-policy');
	var refundPolicyAccepted = true || <?= (isset(Yii::$app->user->id) && Yii::$app->user->identity->info->refund_policy_accepted) ? 'true' : 'false' ?>;

	$('.2co-submit').click(function (event)
	{
		event.preventDefault();
		try {
			<?= Counters::renderAction('invoice_create') ?>
		} catch (e) {}

		if (refundPolicyAccepted) {
			$(this).closest('form').attr('action', '/payment/2co-invoice').submit();
		} else {
			$refundPolicyModal.data('target', this);
			$refundPolicyModal.modal();
		}

		return false;
	});



	$('.paypal-submit').click(function (event) {
		<?php
		if (Yii::$app->user->isGuest){
			echo '
			$("#loginModal").modal();
			return false;';
		}
		?>
		event.preventDefault();
		try {
			<?= Counters::renderAction('invoice_create') ?>
		} catch (e) {}
		$(this).closest('form').attr('action', '/payment/paypal').submit();
		return false;
	});
	
	$refundPolicyModal.find('.policy-accept').on('click', function(event) {
		event.preventDefault();
		$.post(
			'<?= \yii\helpers\Url::to(['account/refund-policy-accept']) ?>',
			{accept: 1},
			function(data) {
				refundPolicyAccepted = (data.status && data.status === 'ok');
				$refundPolicyModal.modal('hide');
				$refundPolicyModal.data('target').click();
			}
		);
	});
});
</script>

<div id="refund-policy" class="modal zen-modal fade" tabindex="-1" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><?= Yii::t('views', 'Dear Customer!') ?></h4>
			</div>
			<div class="modal-body">
				<p><?= Yii::t('views', 'By paying you acknowledge that you have used your free trial on zen-promo.com service, during which you have become fully acquainted with the service functionality. Therefore all the service functions are fully consistent with the description and meet your expectations.') ?></p>
				<p><?= Yii::t('views', 'Purchasing the services you are notified that you can initiate dispute in order to return your unused funds in the following cases:') ?></p>
				<ul>
					<li><?= Yii::t('views', 'if the service is not available in total more than 72 hours a month (except for force majeure)') ?></li>
					<li><?= Yii::t('views', 'in case there were balance write-offs with no actual actions on instagram account during that time period') ?></li>
					<li><?= Yii::t('views', 'if the payment is actually performed, but the balance has not increased, that is goods are actually not delivered') ?></li>
				</ul>
				<p><?= Yii::t('views', 'These conditions apply after you approached the support team and your compensation has not been added to your account balance.') ?></p>
			</div>
			<div class="modal-footer">
				<div class="col-sm-6">
					<button type="button" class="btn btn-red" data-dismiss="modal"><?= Yii::t('views', 'Decline') ?></button>
				</div>
				<div class="col-sm-6">
					<button type="button" class="btn btn-green policy-accept"><?= Yii::t('views', 'Accept') ?></button>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->