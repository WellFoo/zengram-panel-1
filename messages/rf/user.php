<?php return [
	'Email Notifications' => 'Мои подписки',
	'Select options to get emails about:' => 'Отметьте пункты, информацию о которых вы хотите получать в рассылке:',
	'Special offers' => 'Специальные предложения',
	'Service news' => 'Информацию о новых возможностях системы',
	'Payment notification' => 'Уведомления об платежах',
	'Notifications have been saved' => 'Оповещения успешно изменены',
	'You will receive notifications about our sepecial offers' => 'Вы будите получать уведомления о наших специальных предложениях',
	'You will receive notifications about service updates' => 'Вы будите получать оповещения об обновлениях сервиса',
	'You will receive notifications about successfully proceeded payments' => 'Вы будите получать оповещения об успешно проведенных платежах',
	'Uncheck all' => 'Отключить все'
];