<?php return [
	'Your version of Instagram is out of date. Please upgrade your app to log in to Instagram.' => 'Failed to add account. Please try again in few minutes.',
    'Zengram' => 'Zen-promo',
    'Balance recharge on Zengram.net for #{user}' => 'Balance recharge on Zen-promo.com for #{user}',
    'Zengram App' => 'Zen-promo App',
    'Use recommended, branded Zengram comments' => 'Use recommended, branded Zen-promo comments',
    '{value} days on Zengram.net for #{user}' => '{value} days on Zen-promo.com for #{user}',

	'I am promoting my account in Instagram using zengram.net. I was glad for 3 free days, which is presenting to everyone' => 'I am promoting my account in Instagram using zen-promo.com. I was glad for 3 free days, which is presenting to everyone',

];