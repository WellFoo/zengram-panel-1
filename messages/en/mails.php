<?php return [
    'You successfully sign up at Zengram.net.' => 'You successfully sign up at Zen-promo.com',
    'Zengram.net. Need your attention' => 'Zen-promo.com. Need your attention',
    'Zengram. Balance come to the end' => 'Zen-promo. Balance come to the end',
    'Zengram. Not enough funds' => 'Zen-promo. Not enough funds',
    'On your account for service zengram.net not enough funds. Your projects are temporary stopped.' => 'On your account for service zen-promo.com not enough funds. Your projects are temporary stopped.',
    'On your account at the zengram.net service means come to the end. We recommend you to %s the balance right now.' => 'On your account at the zen-promo.com service means come to the end. We recommend you to fill up the balance right now',
];