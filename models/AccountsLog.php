<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "accounts_log".
 *
 * @property integer $id
 * @property string $instagram_id
 * @property string $clientmail
 * @property string $datetime
 * @property string $data
 */

class AccountsLog extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'accounts_log';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['instagram_id', 'clientmail', 'data'], 'required'],
			[['datetime'], 'safe'],
			[['clientmail'], 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => Yii::t('app', 'ID'),
			'instagram_id' => Yii::t('app', 'Instagram ID'),
			'clientmail' => Yii::t('app', 'Clientmail'),
			'datetime' => Yii::t('app', 'Datetime'),
			'data' => Yii::t('app', 'Data')
		];
	}
}
