<?php namespace app\models;


class DemoAccount extends Account
{
	public $options;
	public $user;

	public function __construct($config = [])
	{
		parent::__construct($config);

		$this->id = self::DEMO_ID;
		$this->demo = true;
		$this->login = \Yii::t('views', 'Demo project');
		$this->account_avatar = '/img/demo.jpg';
		$this->account_media = 44;
		$this->account_followers = 1200;
		$this->account_follows = 800;
		$this->lastfollowers = 250;
		$this->lastfollows = 40;
		$this->comments = 12;
		$this->likes = 40;
		$this->unfollow = 34;
		$this->follow = 33;

		$this->user = \Yii::$app->user->identity;

		$this->options = new Options([
			'likes'        => 1,
			'speed'        => 2,
			'comment'      => 1,
			'follow'       => 1,
			'unfollow'     => 0,
			'mutual'       => 1,
			'autounfollow' => 1,
		]);
	}

	public function save($runValidation = true, $attributeNames = null)
	{
		return true;
	}

	public function getOptions()
	{
		return $this->options;
	}

	public function getUser()
	{
		return $this->user;
	}
}