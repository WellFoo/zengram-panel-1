<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "balance_flow".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $value
 * @property string $description
 * @property string $type
 * @property string $date
 */
class BalanceFlow extends ActiveRecord
{
	const ADD_BALANCE = 'add';
	const SUB_BALANCE = 'sub';

	public static function tableName()
	{
		return 'balance_flow';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
				[['user_id', 'value', 'type'], 'required'],
				['description', 'default', 'value' => 'Reason unknown'],
		];
	}
}