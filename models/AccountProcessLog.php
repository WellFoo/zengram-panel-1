<?php

namespace app\models;

use core\logger\Logger;
use yii\db\ActiveRecord;

/**
 * @property integer $id
 * @property integer $worker_ip
 * @property integer $instagram_id
 * @property integer $user_id
 * @property integer $level
 * @property integer $code
 * @property integer $date
 * @property integer $environment
 * @property string  $description
 *
 * This is the model class for table "account_process_log".
 */

class AccountProcessLog extends ActiveRecord
{
	public static function tableName()
	{
		return 'account_process_log';
	}

	public static function getDb()
	{
		return \Yii::$app->log_db;
	}

	public function rules()
	{
		return [
			[['id', 'worker_ip', 'instagram_id', 'user_id', 'level', 'code', 'date', 'environment'], 'integer'],
			[['description'], 'string']
		];
	}

	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'worker_ip' => 'IP',
			'instagram_id' => 'Instagram ID',
			'user_id' => 'User',
			'level' => 'Приоритет',
			'date' => 'Дата',
			'description' => 'Описание',
			'environment' => 'Окружение',
		];
	}

	public static function log($data)
	{
		$log = new AccountProcessLog($data);
		$log->save();
		return $log;
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			if (empty($this->date)) {
				$this->date = time();
			}

			if (!empty($this->code) && empty($this->level)) {
				$this->level = Logger::getCodePriority($this->code);
			}

			return true;
		}
		return false;
	}
}