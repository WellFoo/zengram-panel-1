<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
	static function getSubjects() {
		return [
			Yii::t('app', 'Can&#39;t add account'),
			Yii::t('app', 'Can&#39;t start account'),
			Yii::t('app', 'Problems with counter actions'),
			Yii::t('app', 'Problems with payments'),
			Yii::t('app', 'Problems with password change'),
			Yii::t('app', 'Problems with making comments'),
			Yii::t('app', 'Problems with balance cancellation'),
			Yii::t('app', 'Problem with setup project'),
			'default' => Yii::t('app', 'Other')
		];
	}

	static function getAccounts(){
		if (!isset(Yii::$app->user->identity->accounts)){
			return [];
		}
		$accounts = [];
		foreach(Yii::$app->user->identity->accounts as $account){
			$accounts[] = $account->login;
		}
		sort($accounts);
		return $accounts;
	}
	public $subject;
	public $instagram_account;
	public $body;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			// name, email, subject and body are required
			[['subject', 'body', 'instagram_account'], 'required'],
			//['instagram_account', 'required', 'when' => function($model) {
			//	return $model->subject->value == 1;
			//}]
			//['account_login' ]
		];
	}

	/**
	 * @return array customized attribute labels
	 */
	public function attributeLabels()
	{
		return [
		];
	}

	/**
	 * Sends an email to the specified email address using the information collected by this model.
	 * @param  string  $email the target email address
	 * @return boolean whether the model passes validation
	 */
	public function contact($email)
	{
		if ($this->validate()) {
			Yii::$app->mailer->compose()
				->setTo($email)
				->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
				->setSubject($this->subject)
				->setTextBody($this->body)
				->send();
			return true;
		} else {
			return false;
		}
	}
}
