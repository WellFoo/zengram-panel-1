<?php
namespace app\models;

class Enotify
{
	const ENOTIFY_TYPE_SPECIAl = 1;
	const ENOTIFY_TYPE_NEWS = 2;
	const ENOTIFY_TYPE_PAYMENTS = 3;
	public $table = 'enotify';
	/** @var $db \mysqli */
	private $db;
	private $host;
	private $user;
	private $password;
	private $db_name;

	function __construct($host, $user, $password, $db_name)
	{
		$this->host = $host;
		$this->user = $user;
		$this->password = $password;
		$this->db_name = $db_name;

		$this->db = new \mysqli();
		$this->connect();
	}

	private function connect()
	{
		$this->db->connect($this->host, $this->user, $this->password, $this->db_name);
	}

	function __destruct()
	{
		$this->db->close();
	}

	public function getError()
	{
		return 'SQL error ' . $this->db->errno . ': ' . $this->db->error;
	}

	private function query($query)
	{
		if ($this->db->stat() === false) {
			$this->connect();
		}
		while ($this->db->more_results() && $this->db->next_result()) {
			$this->db->store_result();
		}
		return $this->db->query($query);
	}

	private function queryMultiple($queries)
	{
		if ($this->db->stat() === false) {
			$this->connect();
		}
		while ($this->db->more_results() && $this->db->next_result()) {
			$this->db->store_result();
		}
		foreach ($queries as $idx => $query) {
			$queries[$idx] = trim($query, ';');
		}
		return $this->db->multi_query(implode(';', $queries) . ';');
	}

	public function checkNotify($user_id, $notify)
	{
		$result = $this->query(
			sprintf('SELECT checked FROM %s WHERE user_id=\'%s\' AND notify=\'%s\'',
				$this->table, $user_id, $this->db->escape_string($notify)));
		if (!$result || $result->num_rows < 1) {
			return true;
		}
		$row = $result->fetch_assoc();
		return $row['checked'];
	}

	public function checkNotifies($user_id, $notifies)
	{
		foreach ($notifies as &$notify) {
			$notify = $this->db->escape_string($notify);
		}
		$result = $this->query(
			sprintf('SELECT notify, checked FROM %s WHERE user_id=\'%s\' AND notify IN (\'%s\')',
				$this->table, $user_id, implode('\',\'', $notifies)));
		$results = [];
		if ($result){
			while ($row = $result->fetch_assoc()){
				$results[$row['notify']] = $row['checked'];
				if(($key = array_search($row['notify'], $notifies)) !== false) {
					unset($notifies[$key]);
				}
			}
		}
		foreach ($notifies as $notif) {
			$results[$notif] = true;
		}
		return $results;
	}

	public function changeNotifies($user_id, $notifies)
	{
		$queries = [];
		foreach ($notifies as $notify => $new_value) {
			$queries[] = sprintf(
				'INSERT INTO %s(user_id, notify, checked) VALUES(\'%s\',\'%s\',\'%s\') ON CONFLICT (user_id, notify) DO UPDATE SET checked = EXCLUDED.checked',
				$this->table,
				$user_id,
				$notify,
				$new_value ? '1' : '0'
			);
		}
		return $this->queryMultiple($queries);
	}

	public function changeNotify($user_id, $notify, $new_value)
	{
		return $this->db->query(sprintf(
			'INSERT INTO %s(user_id, notify, checked) VALUES(\'%s\',\'%s\',\'%s\')  ON CONFLICT (user_id, notify) DO UPDATE SET checked = EXCLUDED.checked',
			$this->table,
			$user_id,
			$notify,
			$new_value ? '1' : '0'
		));
	}
}