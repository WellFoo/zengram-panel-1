<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "comments".
 *
 * @property integer $id
 * @property integer $account_id
 * @property string  $text
 * @property integer $is_firm
 * @property integer $spam
 */
class AccountComments extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'account_comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'text'], 'required'],
            [['is_firm', 'spam'], 'default', 'value' => 0],
            [['text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
        ];
    }

	public static function getRandomComment($login)
	{
		
		$query = new Query();
		$query->from('accounts');
		$query->select('id');
		$query->where([
			'login' => $login
		]);

		$account = $query->one();

		$count = self::find()->where([
			'account_id' => $account['id']
		])->count();

		if ($count == 0) {
			return false;
		}

		$count = mt_rand(0, --$count);

		/* @var $comment AccountComments */
		$comment = self::find()->where([
			'account_id' => $account['id']
		])->offset($count)->one();

		return self::parseComment(html_entity_decode($comment->text, ENT_QUOTES));

	}
	
	public static function getRandomComments($login, $limit = 1, $obfuscation = false)
	{

		$query = new Query();
		$query->from('accounts')
			->select('id')
			->where([
				'login' => $login
			]);

		$account = $query->one();

		$count = self::find()->where([
			'account_id' => $account['id']
		])->count();

		if ($count == 0) {
			return false;
		}

		if ($count < $limit){
			$limit = $count;
		}
		$offset = mt_rand(0, $count - $limit);

		if ($offset < 0){
			$offset = 0;
		}

		/* @var $comments[] AccountComments */
		$comments = self::find()->where([
			'account_id' => $account['id']
		])->offset($offset)->limit($limit)->all();

		foreach($comments as $id => $comment){
			$comments[$id] = self::parseComment($comment->text);

			if ($obfuscation) {
				$comments[$id] = self::obfuscateComment($comments[$id]);
			}

		}

		return $comments;
	}

	public static function obfuscateComment($text, $all = false)
	{

		$replaces = [
			'е' => 'e',
			'о' => 'o',
			'р' => 'p',
			'а' => 'a',
			'с' => 'c',
		];

		$length = mb_strlen($text);

		$result = '';

		for ($i = 0; $i < $length; $i++) {

			$char = mb_substr($text, $i, 1, 'UTF-8');

			if (!empty($replaces[$char]) && ($all || mt_rand(0, 1))) {
				
				$char = $replaces[$char];

			}

			$result .= $char;

		}

		return $result;

	}

	public static function parseComment($comment)
	{

		preg_match('#{(.+?)}#is', $comment, $matches);
		while ($matches) {
			$match = $matches[1];
			if (strpos($match, '{') !== false) {
				$match = substr($match, strrpos($match, '{') + 1);
			}
			$parts = explode("|", $match);
			$comment = preg_replace("+{" . preg_quote($match) . "}+is", $parts[array_rand($parts)], $comment, 1);
			preg_match('#{(.+?)}#is', $comment, $matches);
		}
		return $comment;

	}
}
