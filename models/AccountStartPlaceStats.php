<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "account_start_place_stats".
 *
 * @property integer $account_id
 * @property integer $user_id
 * @property integer $instagram_id
 * @property string $date
 * @property string $type
 * @property integer $type_id
 */
class AccountStartPlaceStats extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'account_start_place_stats';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['account_id', 'user_id', 'instagram_id', 'date', 'type_id'], 'required'],
			[['account_id', 'user_id', 'instagram_id', 'type_id'], 'integer'],
			[['date'], 'safe'],
			[['type'], 'string'],
			[['account_id', 'date', 'type_id'], 'unique', 'targetAttribute' => ['account_id', 'date', 'type_id'],
			                                               'message' => 'The combination of Account ID, Date and Place ID has already been taken.']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'account_id' => 'Account ID',
			'user_id' => 'User ID',
			'instagram_id' => 'Instagram ID',
			'date' => 'Date',
			'type_id' => 'Type ID',
		];
	}

	public function getPlace()
	{
		return $this->hasOne(Place::className(), ['id' => 'type_id']);
	}

	public function getRegion()
	{
		return $this->hasOne(Regions::className(), ['id' => 'type_id']);
	}
}