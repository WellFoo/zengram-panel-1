<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "account_stats".
 *
 * @property integer $account_id
 * @property string  $date
 * @property integer $speed
 * @property integer $likes
 * @property integer $comments
 * @property integer $follows
 * @property integer $unfollows
 * @property integer $photos
 * @property integer $manual_comments
 * @property integer $password_resets
 * @property integer $proxy_id
 * @property boolean $trial
 * @property string  $instagram_id
 * @property integer $came
 * @property integer $gone
 * @property integer $user_id
 * @property integer $likes_time
 * @property integer $comments_time
 * @property integer $follows_time
 * @property integer $unfollows_time
 *
 * @property Account account
 * @property Users user
 */
class TestDeliveryAccount extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'delivery_test_accounts';
	}

	public static function primaryKey() {
		return ['id'];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'email', 'instagram_id','user_id'
				],
				'required'
			]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'account_id' => 'Account ID',
			'instagram_id' => 'Instagram ID'
		];
	}

	public function getAccount()
	{
		return self::hasOne(Account::className(), ['id' => 'account_id']);
	}

	public function getUser()
	{
		return self::hasOne(Users::className(), ['id' => 'user_id']);
	}
}
