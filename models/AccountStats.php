<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "account_stats".
 *
 * @property integer $account_id
 * @property string  $date
 * @property integer $speed
 * @property integer $likes
 * @property integer $comments
 * @property integer $follows
 * @property integer $unfollows
 * @property integer $photos
 * @property integer $manual_comments
 * @property integer $password_resets
 * @property integer $proxy_id
 * @property boolean $trial
 * @property string  $instagram_id
 * @property integer $came
 * @property integer $gone
 * @property integer $user_id
 * @property integer $likes_time
 * @property integer $comments_time
 * @property integer $follows_time
 * @property integer $unfollows_time
 * @property integer $usedlist_reseted
 * @property integer follows_private
 * @property integer $mongo_came
 * @property integer $mongo_gone
 *
 * @property Account account
 * @property Users user
 */
class AccountStats extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'account_stats';
	}

	public static function primaryKey() {
		return ['account_id'];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[
				[
					'account_id', 'instagram_id','user_id', 'speed', 'likes', 'comments', 'follows', 'unfollows',
					'photos', 'manual_comments', 'password_resets'
				],
				'required'
			],
			[
				[
					'account_id', 'instagram_id', 'user_id', 'speed', 'likes', 'comments', 'follows', 'unfollows',
					'photos', 'manual_comments', 'password_resets', 'proxy_id', 'came', 'gone', 'mongo_came', 'mongo_gone',
					'likes_time', 'comments_time', 'follows_time', 'unfollows_time', 'usedlist_reseted', 'follows_private'
				],
				'integer'
			],
			[['date'], 'safe'],
			[['trial'], 'boolean'],
			[['instagram_id', 'came', 'gone', 'mongo_came', 'mongo_gone'], 'default', 'value' => 0],
			[['account_id', 'date'], 'unique', 'targetAttribute' => ['account_id', 'date']]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'account_id' => 'Account ID',
			'date' => 'Дата',
			'speed' => 'Скорость',
			'likes' => 'Лайки',
			'comments' => 'Комментарии',
			'follows' => 'Подписки',
			'unfollows' => 'Отписки',
			'photos' => 'Фото',
			'manual_comments' => 'Комментарии (ручные)',
			'password_resets' => 'Сбросы пароля',
			'trial' => 'Демо',
			'came' => 'Пришло',
			'gone' => 'Ушло',
			'usedlist_reseted' => 'Сбросы usedlist',
			'follows_private' => 'Из них(follows) приватных'
		];
	}

	public function getAccount()
	{
		return self::hasOne(Account::className(), ['id' => 'account_id']);
	}

	public function getUser()
	{
		return self::hasOne(Users::className(), ['id' => 'user_id']);
	}

	public function getProxy()
	{
		return self::hasOne(Proxy::className(), ['id' => 'proxy_id']);
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			if (empty($this->date)) {
				$this->date = date('Y-m-d');
			}

			return true;
		}
		return false;
	}
}
