<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "mail_user_triggers".
 *
 * @property integer $id
 * @property integer $action
 * @property integer $user_id
 * @property integer $date
 */
class MailUserTriggers extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'mail_user_triggers';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['action', 'user_id', 'date'], 'integer'],
		];
	}

	public static function sended($data)
	{
		$row = new MailUserTriggers($data);
		$row->save();
		return $row;
	}
	
	public static function reset($user_id, $action)
	{
		return self::findOne([
			'user_id' => $user_id,
			'action' => $action
		])->delete();
	}
}
