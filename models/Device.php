<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "devices".
 *
 * @property integer $id
 * @property string  $name
 * @property integer $android_api
 * @property string  $android_version
 * @property integer $screen_x
 * @property integer $screen_y
 * @property integer $dpi
 * @property string  $vendor
 * @property string  $model
 * @property string  $platform
 * @property string  $cpu
 */
class Device extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'devices';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['name', 'android_api', 'android_version', 'screen_x', 'screen_y', 'dpi', 'vendor', 'model', 'platform', 'cpu'], 'required'],
			[['android_api', 'screen_x', 'screen_y', 'dpi'], 'integer'],
			[['name', 'vendor', 'model', 'platform', 'cpu'], 'string', 'max' => 255],
			[['android_version'], 'string', 'max' => 10]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'name' => 'Name',
			'android_api' => 'Android Api',
			'android_version' => 'Android Version',
			'screen_x' => 'Screen X',
			'screen_y' => 'Screen Y',
			'dpi' => 'Dpi',
			'vendor' => 'Vendor',
			'model' => 'Model',
			'platform' => 'Platform',
			'cpu' => 'Cpu',
		];
	}

	static function getRandom()
	{
		$devices = self::find()->asArray()->all();

		$random_device = array_rand($devices);

		return $devices[$random_device];
	}
}
