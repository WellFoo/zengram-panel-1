<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
* This is the model class for table "statistics".
*
	* @property integer $id
	* @property integer $account_id
	* @property string $datetime
	* @property integer $likes
	* @property integer $comments
	* @property integer $follow
	* @property integer $unfollow
*/
class Statistics extends ActiveRecord
{
/**
* @inheritdoc
*/
public static function tableName()
{
return 'statistics';
}

/**
* @inheritdoc
*/
public function rules()
{
return [
            [['account_id', 'likes', 'comments', 'follow', 'unfollow'], 'required'],
            [['account_id', 'likes', 'comments', 'follow', 'unfollow'], 'integer'],
            [['datetime'], 'safe']
        ];
}

/**
* @inheritdoc
*/
public function attributeLabels()
{
return [
	'id' => Yii::t('app', 'ID'),
	'account_id' => Yii::t('app', 'Account ID'),
	'datetime' => Yii::t('app', 'Datetime'),
	'likes' => Yii::t('app', 'Likes'),
	'comments' => Yii::t('app', 'Comments'),
	'follow' => Yii::t('app', 'Follow'),
	'unfollow' => Yii::t('app', 'Unfollow'),
];
}
}
