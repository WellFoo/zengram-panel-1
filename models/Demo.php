<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "demo".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $login
 * @property string $date
 * @property string $tryed
 */

class Demo extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id', 'date', 'tryed'], 'safe'],
			[['user_id', 'login'], 'required'],
			[['login', 'date'], 'string']
		];
	}


	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'demo';
	}

}