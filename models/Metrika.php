<?php

/*
    http://webfilin.ru/notes/server_yametrika/
    
    Author: Seleznev Denis, hcodes@yandex.ru
    Description: Серверная отправка хитов с помощью PHP в Яндекс.Метрику
    Version: 1.0
    License: MIT, GNU PL

    Примеры использования:
    ======================
    
    $counter = new YaMetrika(123456); // номер счётчика Метрики
    $counter->hit(); // Значение URL и referer берутся по умолчанию из $_SERVER
    
    // Отправка хита
    $counter->hit('http://example.ru', 'Main page', 'http://ya.ru');
    $counter->hit('/index.html', 'Main page', '/back.html');

    // Отправка хита вместе с пользовательскими параметрами
    $counter->hit('http://example.ru', 'Main page', 'http://ya.ru', $myParams);

    // Отправка хита вместе с параметрами визитов и с запретом на индексацию
    $counter->hit('http://example.ru', 'Main page', 'http://ya.ru', $myParams, 'noindex');

    // Достижение цели
    $counter->reachGoal('back');
    
    // Внешняя ссылка - отчёт "Внешние ссылки"
    $counter->extLink('http://yandex.ru');
    
    // Загрузка файла - отчёт "Загрузка файлов"
    $counter->file('http://example.ru/file.zip');
    $counter->file('/file.zip');

    // Отправка пользовательских параметров - отчёт "Параметры визитов"
    $counter->params(array('level1' => array('level2' => 1)));

    // Не отказ
    $counter->notBounce();
*/
namespace app\models;

use Yii;

class Metrika
{
	const HOST = 'https://mc.yandex.ru';
	const PATH = '/watch/';
	const PORT = 443;

	private $counterId;
	private $counterClass;
	private $encoding;

	public $currentPage;

	function __construct($counterId, $counterClass = 0, $encoding = 'utf-8')
	{
		$this->counterId    = $counterId;
		$this->counterClass = $counterClass;
		$this->encoding     = $encoding;
	}

	// Отправка хита
	public function hit($pageUrl = null, $pageTitle = null, $pageRef = null, $userParams = '', $ut = '')
	{
		$this->currentPage = '/';
		$currentUrl = $this->currentPageUrl();
		$referer    = empty($_SERVER['HTTP_HOST']) ? Yii::$app->params['host'] : $_SERVER['HTTP_HOST'];

		if (is_null($pageUrl)) {
			$pageUrl = $currentUrl;
		}

		if (is_null($pageRef)) {
			$pageRef = $referer;
		}

		$pageUrl = $this->absoluteUrl($pageUrl, $currentUrl);
		$pageRef = $this->absoluteUrl($pageRef, $currentUrl);
		$modes = array('ut' => $ut);
		$this->hitExt($pageUrl, $pageTitle, $pageRef, $userParams, $modes);
	}

	// Достижение цели
	public function reachGoal($target = '', $userParams = null)
	{
		if ($target) {
			$target  = 'goal://'.(empty($_SERVER['HTTP_HOST']) ? Yii::$app->params['host'] : $_SERVER['HTTP_HOST']).'/'.$target;
			$referer = $this->currentPageUrl();
		} else {
			$target  = $this->currentPageUrl();
			$referer = empty($_SERVER['HTTP_HOST']) ? Yii::$app->params['host'] : $_SERVER['HTTP_HOST'];
		}

		$this->hitExt($target, null, $referer, $userParams, null);
	}

	// Внешняя ссылка
	public function extLink($url = '', $title = '')
	{
		if ($url) {
			$modes   = array('ln' => true, 'ut' => 'noindex');
			$referer = $this->currentPageUrl();
			$this->hitExt($url, $title, $referer, null, $modes);
		}
	}

	// Загрузка файла
	public function file($file = '', $title = '')
	{
		if ($file) {
			$currentUrl = $this->currentPageUrl();
			$modes      = array('dl' => true, 'ln' => true);
			$file       = $this->absoluteUrl($file, $currentUrl);
			$this->hitExt($file, $title, $currentUrl, null, $modes);
		}
	}

	// Не отказ
	public function notBounce()
	{
		$modes = array('nb' => true);
		$this->hitExt('', '', '', null, $modes);
	}

	// Параметры визитов
	public function params($data)
	{
		if ($data) {
			$modes = array('pa' => true);
			$this->hitExt('', '', '', $data, $modes);
		}
	}

	// Общий метод для отправки хитов
	private function hitExt($pageUrl = '', $pageTitle = '', $pageRef = '', $userParams = null, $modes = array())
	{
		$postData = array();

		if ($this->counterClass) {
			$postData['cnt-class'] = $this->counterClass;
		}

		if ($pageUrl) {
			$postData['page-url'] = urlencode($pageUrl);
		}
		if ($pageRef) {
			$postData['page-ref'] = urlencode($pageRef);
		}

		if ($modes) {
			$modes['ar'] = true;
		} else {
			$modes = array('ar' => true);
		}

		$browser_info = array();
		if ($modes and count($modes)) {
			foreach ($modes as $key => $value) {
				if ($value and $key != 'ut') {
					if ($value === true) {
						$value = 1;
					}

					$browser_info[] = $key.':'.$value;
				}
			}
		}

		$browser_info[] = 'en:'.$this->encoding;

		if ($pageTitle) {
			$browser_info[] = 't:'.urlencode($pageTitle);
		}

		$postData['browser-info'] = implode(':', $browser_info);
//		$postData['browser-info'] = 'vc:w:s:1920x1080x24:sk:1:l:5.1.20513.0:fpr:59420134601:cn:1:w:1920x494:z:180:i:20150525172834:et:1432564115:en:utf-8:v:631:c:1:la:ru-ru:ls:1304388634489:rqn:89:rn:777010125:hid:848002523:ds:0,0,56,0,45,0,,171,62,,,,378:wn:37618:hl:3:rqnl:1:st:1432564115';
		if ($userParams) {
			$up                    = json_encode($userParams);
			$postData['site-info'] = urlencode($up);
		}

		$postData['callback'] = '_ymjsp'.rand(0, 100000000);


		if (!empty($modes['ut'])) {
			$postData['ut'] = $modes['ut'];
		}

		$getQuery = self::PATH.$this->counterId.'/1?wmode=5';

		$this->postRequest(self::HOST, $getQuery, $this->buildQueryVars($postData));
	}

	// Текущий URL
	private function currentPageUrl()
	{
		$protocol = 'http://';

		if (!empty($_SERVER['HTTPS'])) {
			$protocol = 'https://';
		}
		$pageUrl = $protocol.(empty($_SERVER['HTTP_HOST']) ? Yii::$app->params['host'] : $_SERVER['HTTP_HOST'])
				.($this->currentPage ? $this->currentPage : (empty($_SERVER['REQUEST_URI']) ? $_SERVER['SCRIPT_NAME'] : $_SERVER['REQUEST_URI']));

		return $pageUrl;
	}

	// Преобразование из относительного в абсолютный url
	private function absoluteUrl($url, $baseUrl)
	{
		if (!$url) {
			return '';
		}

		$parseUrl = parse_url($url);
		$base     = parse_url($baseUrl);
		$hostUrl  = $base['scheme']."://".$base['host'];

		if (!empty($parseUrl['scheme'])) {
			$absUrl = $url;
		} elseif (!empty($parseUrl['host'])) {
			$absUrl = "http://".$url;
		} else {
			$absUrl = $hostUrl.$url;
		}

		return $absUrl;
	}

	// Построение переменных в запросе
	private function buildQueryVars($queryVars)
	{
		$queryBits = array();
		while (list($var, $value) = each($queryVars)) {
			$queryBits[] = $var.'='.$value;
		}

		return (implode('&', $queryBits));
	}

	// Отправка POST-запроса
	private function postRequest($host, $path, $dataToSend)
	{
		$curl = curl_init($host.$path.'&'.$dataToSend);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
//		curl_setopt($curl, CURLOPT_POST, true);
//		curl_setopt($curl, CURLOPT_POSTFIELDS, $dataToSend);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(
			"X-Real-IP: ".empty($_SERVER['REMOTE_ADDR']) ? '8.8.8.8' : $_SERVER['REMOTE_ADDR'],
			"Content-type: application/x-www-form-urlencoded"
		));
		curl_exec($curl);
		curl_close($curl);
		return true;
	}
}

// http://docs.php.net/manual/en/function.json-encode.php
if (!function_exists('json_encode')) {
	function json_encode($data)
	{
		if (is_array($data) || is_object($data)) {
			$islist = is_array($data) && (empty($data) || array_keys($data) === range(0, count($data) - 1));

			if ($islist) {
				$json = '['.implode(',', array_map('json_encode', $data)).']';
			} else {
				$items = array();
				foreach ($data as $key => $value) {
					$items[] = json_encode("$key").':'.json_encode($value);
				}
				$json = '{'.implode(',', $items).'}';
			}
		} elseif (is_string($data)) {
			# Escape non-printable or Non-ASCII characters.
			# I also put the \\ character first, as suggested in comments on the 'addclashes' page.
			$string = '"'.addcslashes($data, "\\\"\n\r\t/".chr(8).chr(12)).'"';
			$json   = '';
			$len    = strlen($string);
			# Convert UTF-8 to Hexadecimal Codepoints.
			for ($i = 0; $i < $len; $i++) {

				$char = $string[$i];
				$c1   = ord($char);

				# Single byte;
				if ($c1 < 128) {
					$json .= ($c1 > 31) ? $char : sprintf("\\u%04x", $c1);
					continue;
				}

				# Double byte
				$c2 = ord($string[++$i]);
				if (($c1 & 32) === 0) {
					$json .= sprintf("\\u%04x", ($c1 - 192) * 64 + $c2 - 128);
					continue;
				}

				# Triple
				$c3 = ord($string[++$i]);
				if (($c1 & 16) === 0) {
					$json .= sprintf("\\u%04x", (($c1 - 224) << 12) + (($c2 - 128) << 6) + ($c3 - 128));
					continue;
				}

				# Quadruple
				$c4 = ord($string[++$i]);
				if (($c1 & 8) === 0) {
					$u = (($c1 & 15) << 2) + (($c2 >> 4) & 3) - 1;

					$w1 = (54 << 10) + ($u << 6) + (($c2 & 15) << 2) + (($c3 >> 4) & 3);
					$w2 = (55 << 10) + (($c3 & 15) << 6) + ($c4 - 128);
					$json .= sprintf("\\u%04x\\u%04x", $w1, $w2);
				}
			}
		} else {
			# int, floats, bools, null
			$json = strtolower(var_export($data, true));
		}
		return $json;
	}
}