<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class RegisterForm extends Model
{
	public $mail;
	public $password;
	public $balance;

	/* @var Users $_user */
	private $_user = false;


	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			// username and password are both required
			[['mail', 'password'], 'required'],
			['mail', 'email', 'message' => Yii::t('app', 'Enter correct e-mail.')],
			['mail', 'unique', 'targetClass' => Users::className(), 'targetAttribute' => ['mailLowerCase' => 'lower(mail)'], 'message' => Yii::t('app', 'Entered e-mail already in use.')],
			['balance', 'default', 'value' => Yii::$app->params['default_user_balance']],
			// password is validated by validatePassword()
			// ['repassword', 'compare', 'compareAttribute' => 'password'],
		];
	}

	public function getMailLowerCase()
	{
		return mb_strtolower($this->mail);
	}

	public function attributeLabels()
	{
		return [
			'mail'       => Yii::t('app', 'E-mail'),
			'password'    => Yii::t('app', 'Password'),
		];
	}

	/**
	 *
	 *
	 * @return Users|null
	 */
	public function getUser()
	{
		return $this->_user;
	}

	public function register()
	{
		//save emails, etc
		$this->load(Yii::$app->request->post());

		$passwordOriginal = $this->password;

		if(Users::find()->where(['mail' => $this->mail])->one()){
			$this->addError('login', Yii::t('app', 'User already exists'));
			return false;
		} else {
			if(!empty($this->password)) {
				$this->password = md5($this->password);
			}

			$user = new Users($this->toArray());
			$user->balance = Yii::$app->params['default_user_balance'];
			$user->continent = Account::ipInfo("continent_code");

			if (LANGUAGE == 'rf'){
				$user->is_reg_from_rf = true;
			}

			$newUser = $user->save();

			$bf = new BalanceFlow([
				'user_id' => $user->id,
				'value'=> Yii::$app->params['default_user_balance'],
				'description' => 'Начисление за регистрацию',
				'type' => 'add'
			]);
			$bf->save();

			if ($newUser) {
				Yii::$app->user->login(Users::findByMail($this->mail), 3600 * 24 * 30);

				$caption = Yii::t('mails', 'You’ve successfully signed up');
				$body = [
					Yii::t('mails', 'Congratulations!'),
					Yii::t('mails', 'You successfully sign up at Zengram.net.') . PHP_EOL .
					Yii::t('mails', 'For an authorization at the site use next data:'),
					'',
					Yii::t('mails', 'E-mail: {mail}', ['mail' => $this->mail])  . PHP_EOL .
					Yii::t('mails', 'Password: {password}', ['password' => $passwordOriginal])
				];

				$token = new UserTokens();
				$token->generate(Yii::$app->user->id);
				$token->save();

				try {
					Yii::$app->mailer->compose()
						->setFrom([Yii::$app->params['adminEmail'] => Yii::$app->params['company']])
						->setTo($this->mail)
						->setSubject($caption)
						->setTextBody(
							implode(PHP_EOL, $body)
						)->setHtmlBody(
							Yii::$app->controller->renderPartial('//layouts/mail_'.Yii::$app->language, [
									'caption' => $caption,
									'body' => $body,
									'token' => $token->token
								]
							)
						)->send();
				} catch (\Swift_TransportException $e) {}
			}

			return $newUser;
		}
	}
}
