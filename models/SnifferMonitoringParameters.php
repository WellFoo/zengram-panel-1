<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "sniffer_monitoring_parameters".
 *
 * @property SnifferServers $server
 * @property integer $id
 * @property integer $server_id
 * @property integer $user_id
 * @property string  $parameter
 * @property string  $value
 * @property integer $operator
 * @property integer $notification
 */
class SnifferMonitoringParameters extends ActiveRecord
{

	const OPERATOR_MORE    = 1;
	const OPERATOR_MORE_EQ = 2;
	const OPERATOR_LESS    = 3;
	const OPERATOR_LESS_EQ = 4;
	const OPERATOR_EQ      = 5;

	public $skype;
	public $phone;
	public $email;
	public $push;

	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'sniffer_monitoring_parameters';
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'server_id' => 'Сервер',
			'user_id' => 'ID пользователя',
			'parameter' => 'Параметр',
			'value' => 'Значение',
			'operator' => 'Оператор',
			'notification' => 'Оповещение',
		] + self::getNotificationsList();
	}


	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['server_id', 'user_id', 'operator', 'notification', 'id'], 'integer'],
			[['parameter', 'value'], 'string'],
			[['skype', 'push', 'phone', 'email'], 'safe'],
		];
	}

	public function getServer()
	{
		return SnifferServers::findOne($this->server_id);
	}

	public function compare($value)
	{
		$server_value = round((float) $value, 2);
		$default_value = round((float) $this->value, 2);

		switch ($this->operator) {
			case self::OPERATOR_MORE:
				return $server_value > $default_value;
				break;
			case self::OPERATOR_MORE_EQ:
				return $server_value >= $default_value;
				break;
			case self::OPERATOR_LESS:
				return $server_value < $default_value;
				break;
			case self::OPERATOR_LESS_EQ:
				return $server_value <= $default_value;
				break;
			case self::OPERATOR_EQ:
				return $server_value == $default_value;
				break;
			default:
				return false;
				break;
		}
	}

	public function getOperatorDescription()
	{
		$descriptions = self::getOperatorDescriptions();

		if (!empty($descriptions[$this->operator])) {
			return $descriptions[$this->operator];
		}

		return '';
	}

	public function setDefaultValues($value)
	{
		// TODO: Сделать умную генерацию значений по параметрам
		$this->operator = self::OPERATOR_MORE;
		$this->email = true;
	}

	public static function getOperatorDescriptions()
	{
		return [
			self::OPERATOR_MORE    => 'больше',
			self::OPERATOR_MORE_EQ => 'больше или равен',
			self::OPERATOR_LESS    => 'меньше',
			self::OPERATOR_LESS_EQ => 'меньше или равен',
			self::OPERATOR_EQ      => 'равен',
		];
	}

	public function getParameterDescription()
	{
		$descriptions = self::getParametersList();

		if (!empty($descriptions[$this->parameter])) {
			return $descriptions[$this->parameter];
		}

		return '';
	}

	public static function getParametersList()
	{
		return [
			'processor'  => 'Процессор',
			'disk_drive' => 'Диск',
			'memory'     => 'Память',
			'ping'       => 'Пинг',
		];
	}

	public static function getNotificationsList()
	{
		return [
			'skype' => 'Скайп',
			'phone' => 'Телефон',
			'email' => 'E-mail',
			'push'  => 'Push-уведомления',
		];
	}

	public function setNotification($name, $value)
	{
		if ($value === '1' || $value === 1 || $value === true || $value === 'true') {
			$this->$name = 1;
		} else {
			$this->$name = 0;
		}
	}

	public static function getNotificationsWeight()
	{
		// Веса равны 2 ^ X, для парсинга
		return [
			'skype' => 1,
			'phone' => 2,
			'email' => 4,
			'push'  => 8,
		];
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {

			$this->collapseNotification();

			return true;
		}
		return false;
	}

	private function collapseNotification()
	{
		$notifications = self::getNotificationsWeight();
		$this->notification = 0;

		foreach ($notifications as $notification => $weight) {

			Yii::error($notification);
			Yii::error($this->$notification);
			$this->setNotification($notification, $this->$notification);

			if (!empty($this->$notification)) {
				Yii::error($this->$notification);
				$this->notification += $weight;
			}

		}

		Yii::error($this->notification);
	}

	private function expandNotification()
	{
		$notifications = self::getNotificationsWeight();
		arsort($notifications);

		$value = $this->notification;

		foreach ($notifications as $notification => $weight) {

			if ($value - $weight >= 0) {
				$this->$notification = true;
				$value -= $weight;
			} else {
				$this->$notification = false;
			}
		}
	}

	public function afterSave($insert, $changedAttributes)
	{
		parent::afterSave($insert, $changedAttributes);

		$this->expandNotification();
	}

	public function afterFind()
	{
		$this->expandNotification();
	}
}
