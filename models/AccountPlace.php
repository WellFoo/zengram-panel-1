<?php namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "account_places_new".
 *
 * @property integer $account_id
 * @property integer $place_id
 * @property boolean $circle_enabled
 * @property string  $circle
 *
 * @property FbGeotag[] $geotags
 */
class AccountPlace extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'account_places_new';
	}

	/**
	 * @inheritdoc
	 */
	public static function primaryKey()
	{
		return ['account_id', 'place_id'];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['account_id', 'place_id'], 'integer'],
			[['circle_enabled'], 'boolean'],
			[['circle'], 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'account_id'     => Yii::t('app', 'Account ID'),
			'place_id'       => Yii::t('app', 'Place ID'),
			'circle_enabled' => Yii::t('app', 'Circle Enabled'),
			'circle'         => Yii::t('app', 'Circle'),
		];
	}

	public function getGeotags()
	{
		return $this->hasMany(FbGeotag::className(), ['id' => 'fb_geotag_id']);
	}

	/**
	 * @param $coords array
	 * @param $radius float
	 */
	public function setCircleData($coords, $radius)
	{
		$this->circle = sprintf('%f,%f,%f', $coords['lat'], $coords['lng'], $radius);
	}

	/**
	 * @return array
	 */
	public function getCircleData()
	{
		preg_match_all('/-?\d+(:?\.\d+)?/', $this->circle, $matches);
		list($lat, $lng, $radius) = $matches[0];
		return [
			'center' => [
				'lat' => floatval($lat),
				'lng' => floatval($lng),
			],
			'radius' => floatval($radius),
		];
	}
}
