<?php

namespace app\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * Class AccountSettings
 * @package app\models
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $pause
 * @property string  $setting
 * @property string  $value
 */

class AccountSettings extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return 'account_settings';
	}

	public function rules()
	{
		return [
			[['account_id', 'value', 'setting', 'pause'], 'safe']
		];
	}

	public function attributeLabels()
	{
		return array(
			'setting' => 'Ключ',
			'value' => 'Значение',
		);
	}

}